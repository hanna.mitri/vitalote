diff --git a/js/recaptcha.js b/js/recaptcha.js
new file mode 100644
index 0000000..a9a5912
--- /dev/null
+++ b/js/recaptcha.js
@@ -0,0 +1,34 @@
+/**
+ * @file
+ * Contains the definition of the behaviour recaptcha.
+ */
+
+(function ($, Drupal) {
+  Drupal.behaviors.recaptcha = {
+    attach: function (context) {
+      $('.g-recaptcha', context).each(function () {
+        if (typeof grecaptcha === 'undefined' || typeof grecaptcha.render !== 'function') {
+          return;
+        }
+        if ($(this).closest('body').length > 0) {
+          if ($(this).hasClass('recaptcha-processed')) {
+            grecaptcha.reset();
+          }
+          else {
+            grecaptcha.render(this, $(this).data());
+            $(this).addClass('recaptcha-processed');
+          }
+        }
+      });
+    }
+  };
+
+  window.drupalRecaptchaOnload = function () {
+    $('.g-recaptcha').each(function () {
+      if (!$(this).hasClass('recaptcha-processed')) {
+        grecaptcha.render(this, $(this).data());
+        $(this).addClass('recaptcha-processed');
+      }
+    });
+  };
+})(jQuery, Drupal);
diff --git a/recaptcha.libraries.yml b/recaptcha.libraries.yml
new file mode 100644
index 0000000..7471b37
--- /dev/null
+++ b/recaptcha.libraries.yml
@@ -0,0 +1,6 @@
+recaptcha:
+  js:
+    js/recaptcha.js: {}
+  dependencies:
+    - core/drupal
+    - core/jquery
diff --git a/recaptcha.module b/recaptcha.module
index cc1953e..b14d3fb 100644
--- a/recaptcha.module
+++ b/recaptcha.module
@@ -85,9 +85,10 @@ function recaptcha_captcha($op, $captcha_type = '') {
             '#value' => 'Google no captcha',
           ];
 
-          // As the validate callback does not depend on sid or solution, this
-          // captcha type can be displayed on cached pages.
-          $captcha['cacheable'] = TRUE;
+          // Prevent "CAPTCHA validation error: unknown CAPTCHA session ID"
+          $captcha['cacheable'] = FALSE;
+          $captcha['form']['captcha_response']['#cache'] = ['max-age' => 0];
+          \Drupal::service('page_cache_kill_switch')->trigger();
 
           // Check if reCAPTCHA use globally is enabled.
           $recaptcha_src = 'https://www.google.com/recaptcha/api.js';
@@ -118,6 +119,11 @@ function recaptcha_captcha($op, $captcha_type = '') {
             'data-size' => $config->get('widget.size'),
             'data-tabindex' => $config->get('widget.tabindex'),
           ];
+          $captcha['form']['#attached']['library'] = [
+            'recaptcha/recaptcha',
+            'recaptcha/google.recaptcha_' . \Drupal::service('language_manager')->getCurrentLanguage()->getId(),
+          ];
+
           // Filter out empty tabindex/size.
           $attributes = array_filter($attributes);
 
@@ -130,7 +136,14 @@ function recaptcha_captcha($op, $captcha_type = '') {
                   [
                     '#tag' => 'script',
                     '#attributes' => [
-                      'src' => Url::fromUri($recaptcha_src, ['query' => ['hl' => \Drupal::service('language_manager')->getCurrentLanguage()->getId()], 'absolute' => TRUE])->toString(),
+                      'src' => Url::fromUri($recaptcha_src, [
+                        'query' => [
+                          'hl' => \Drupal::service('language_manager')->getCurrentLanguage()->getId(),
+                          'onload' => 'drupalRecaptchaOnload',
+                          'render' => 'explicit',
+                        ],
+                        'absolute' => TRUE,
+                      ])->toString(),
                       'async' => TRUE,
                       'defer' => TRUE,
                     ],
@@ -219,5 +232,44 @@ function recaptcha_captcha_validation($solution, $response, $element, $form_stat
 function template_preprocess_recaptcha_widget_noscript(&$variables) {
   $variables['sitekey'] = $variables['widget']['sitekey'];
   $variables['language'] = $variables['widget']['language'];
-  $variables['url'] = Url::fromUri($variables['widget']['recaptcha_src_fallback'], ['query' => ['k' => $variables['widget']['sitekey'], 'hl' => $variables['widget']['language']], 'absolute' => TRUE])->toString();
+  $variables['url'] = Url::fromUri($variables['widget']['recaptcha_src_fallback'], [
+    'query' => [
+      'k' => $variables['widget']['sitekey'],
+      'hl' => $variables['widget']['language'],
+    ],
+    'absolute' => TRUE,
+  ])->toString();
 }
+
+/**
+ * Implements hook_library_info_build().
+ */
+function recaptcha_library_info_build() {
+    $libraries = [];
+    $languages = \Drupal::service('language_manager')->getLanguages();
+    foreach ($languages as $key => $language) {
+        $url = Url::fromUri('https://www.google.com/recaptcha/api.js', [
+            'query' => [
+                'hl' => $key,
+                'render' => 'explicit',
+                'onload' => 'drupalRecaptchaOnload',
+              ],
+            'absolute' => TRUE,
+          ])->toString();
+        $libraries["google.recaptcha_$key"] = [
+            'version' => '1.x',
+            'header' => TRUE,
+            'js' => [
+                $url => [
+                    'type' => 'external',
+                    'minified' => TRUE,
+                    'attributes' => [
+                        'async' => TRUE,
+                        'defer' => TRUE,
+                      ],
+                  ],
+              ],
+          ];
+      }
+  return $libraries;
+ }
diff --git a/tests/src/Functional/ReCaptchaBasicTest.php b/tests/src/Functional/ReCaptchaBasicTest.php
index 1189ac2..eb40641 100644
--- a/tests/src/Functional/ReCaptchaBasicTest.php
+++ b/tests/src/Functional/ReCaptchaBasicTest.php
@@ -141,7 +141,15 @@ class ReCaptchaBasicTest extends BrowserTestBase {
     // Check if there is a reCAPTCHA on the login form.
     $this->drupalGet('user/login');
     $this->assertSession()->responseContains($grecaptcha, '[testReCaptchaOnLoginForm]: reCAPTCHA is shown on form.');
-    $this->assertSession()->responseContains('<script src="' . Url::fromUri('https://www.google.com/recaptcha/api.js', ['query' => ['hl' => \Drupal::service('language_manager')->getCurrentLanguage()->getId()], 'absolute' => TRUE])->toString() . '" async defer></script>', '[testReCaptchaOnLoginForm]: reCAPTCHA is shown on form.');
+    $options = [
+      'query' => [
+        'hl' => \Drupal::service('language_manager')->getCurrentLanguage()->getId(),
+        'onload' => 'drupalRecaptchaOnload',
+        'render' => 'explicit',
+      ],
+      'absolute' => TRUE,
+    ];
+    $this->assertSession()->responseContains(Html::escape(Url::fromUri('https://www.google.com/recaptcha/api.js', $options)->toString()), '[testReCaptchaOnLoginForm]: reCAPTCHA is shown on form.');
     $this->assertSession()->responseNotContains($grecaptcha . '<noscript>', '[testReCaptchaOnLoginForm]: NoScript code is not enabled for the reCAPTCHA.');
 
     // Test if the fall back url is properly build and noscript code added.
@@ -161,7 +169,22 @@ class ReCaptchaBasicTest extends BrowserTestBase {
     // Check if there is a reCAPTCHA with global url on the login form.
     $this->config('recaptcha.settings')->set('use_globally', TRUE)->save();
     $this->drupalGet('user/login');
-    $this->assertSession()->responseContains('<script src="' . Url::fromUri('https://www.recaptcha.net/recaptcha/api.js', ['query' => ['hl' => \Drupal::service('language_manager')->getCurrentLanguage()->getId()], 'absolute' => TRUE])->toString() . '" async defer></script>', '[testReCaptchaOnLoginForm]: Global reCAPTCHA is shown on form.');
+    $options = [
+      'query' => [
+        'hl' => \Drupal::service('language_manager')->getCurrentLanguage()->getId(),
+        'onload' => 'drupalRecaptchaOnload',
+        'render' => 'explicit',
+      ],
+      'absolute' => TRUE,
+    ];
+    $this->assertSession()->responseContains(Html::escape(Url::fromUri('https://www.recaptcha.net/recaptcha/api.js', $options)->toString()), '[testReCaptchaOnLoginForm]: Global reCAPTCHA is shown on form.');
+    $options = [
+      'query' => [
+        'k' => $site_key,
+        'hl' => \Drupal::service('language_manager')->getCurrentLanguage()->getId(),
+      ],
+      'absolute' => TRUE,
+    ];
     $this->assertSession()->responseContains(Html::escape(Url::fromUri('https://www.recaptcha.net/recaptcha/api/fallback', $options)->toString()), '[testReCaptchaOnLoginForm]: Global fallback URL with IFRAME has been found.');
 
     // Check that data-size attribute does not exists.
