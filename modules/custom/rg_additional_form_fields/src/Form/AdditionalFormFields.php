<?php

namespace Drupal\rg_additional_form_fields\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\ConfigFormBase;

class AdditionalFormFields extends ConfigFormBase
{

  /**
   * {@inheritdoc}
   */
  public function getFormId()
  {
    return 'additional_form_fields';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state)
  {

    $form = parent::buildForm($form, $form_state);
    $config = $this->config('rg_additional_form_fields.settings');

    // Warranty API site name
    $form['warranty_api'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Site name for warranty API'),
      '#default_value' => $config->get('warranty_api')
    ];

    //brochure page
    $form['brochure'] = array(
      '#type' => 'details',
      '#title' => $this->t('Brochures texts'),
    );

    $form['brochure']['post_code_api_login'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Login to Post Code API'),
      '#default_value' => $config->get('brochure.post_code_api_login'),
      '#required' => true,
    ];

    $form['brochure']['post_code_api_password'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Password to Post Code API'),
      '#default_value' => $config->get('brochure.post_code_api_password'),
      '#required' => true,
    ];

    $form['brochure']['field_header'] = [
      '#type' => 'text_format',
      '#format' => 'full_html',
      '#title' => $this->t('Order Brochure Header'),
      '#default_value' => $config->get('brochure.field_header'),
      '#required' => false,
    ];

    $form['brochure']['field_email_placeholder'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Order brochure email field placeholder'),
      '#default_value' => $config->get('brochure.field_email_placeholder'),
      '#required' => false,
    ];

    $form['brochure']['field_signup_text'] = [
      '#type' => 'text_format',
      '#format' => 'full_html',
      '#title' => $this->t('Order Brochure Sign Up text'),
      '#default_value' => $config->get('brochure.field_signup_text'),
      '#required' => false,
    ];

    $form['brochure']['field_policy'] = [
      '#type' => 'text_format',
      '#format' => 'full_html',
      '#title' => $this->t('Order Brochure Policy'),
      '#default_value' => $config->get('brochure.field_policy'),
      '#required' => false,
    ];
    //product page
    $form['product_page'] = array(
      '#type' => 'details',
      '#title' => $this->t('Product page texts'),
    );
    $form['product_page']['gallery'] = [
      '#type' => 'text_format',
      '#format' => 'raw_html',
      '#title' => $this->t('Gallery title'),
      '#default_value' => $config->get('product_page.gallery'),
    ];

    $form['product_page']['accordion'] = [
      '#type' => 'details',
      '#title' => $this->t('Accordion main title'),
    ];
    $form['product_page']['accordion']['default'] = [
      '#type' => 'text_format',
      '#format' => 'raw_html',
      '#title' => $this->t('Accordion title for cookers'),
      '#default_value' => $config->get('product_page.default'),
    ];
    $form['product_page']['accordion']['builtin'] = [
      '#type' => 'text_format',
      '#format' => 'raw_html',
      '#title' => $this->t('Accordion title for built-in'),
      '#default_value' => $config->get('product_page.builtin'),
    ];
    $form['product_page']['accordion']['fridge'] = [
      '#type' => 'text_format',
      '#format' => 'raw_html',
      '#title' => $this->t('Accordion title for fridge'),
      '#default_value' => $config->get('product_page.fridge'),
    ];
    $form['product_page']['accordion']['hood'] = [
      '#type' => 'text_format',
      '#format' => 'raw_html',
      '#title' => $this->t('Accordion title for hood'),
      '#default_value' => $config->get('product_page.hood'),
    ];
    $form['product_page']['accordion']['sink'] = [
      '#type' => 'text_format',
      '#format' => 'raw_html',
      '#title' => $this->t('Accordion title for sink'),
      '#default_value' => $config->get('product_page.sink'),
    ];
    $form['product_page']['accordion']['tap'] = [
      '#type' => 'text_format',
      '#format' => 'raw_html',
      '#title' => $this->t('Accordion title for tap'),
      '#default_value' => $config->get('product_page.tap'),
    ];
    $form['product_page']['accordion']['cooker_60'] = [
      '#type' => 'text_format',
      '#format' => 'raw_html',
      '#title' => $this->t('Accordion title for cooker 60'),
      '#default_value' => $config->get('product_page.cooker_60'),
    ];
    $form['product_page']['accordion']['splashback'] = [
      '#type' => 'text_format',
      '#format' => 'raw_html',
      '#title' => $this->t('Accordion title for splashback'),
      '#default_value' => $config->get('product_page.splashback'),
    ];
    $form['product_page']['accordion']['disposal'] = [
      '#type' => 'text_format',
      '#format' => 'raw_html',
      '#title' => $this->t('Accordion title for Waste Disposal'),
      '#default_value' => $config->get('product_page.disposal'),
    ];

    $form['product_page']['bottom_block'] = [
      '#type' => 'text_format',
      '#format' => 'raw_html',
      '#title' => $this->t('Bottom block title'),
      '#default_value' => $config->get('product_page.bottom_block'),
    ];

    $form['product_page']['accordion_titles'] = array(
      '#type' => 'details',
      '#title' => $this->t('Accordion titles for rows'),
    );
    $form['product_page']['accordion_titles']['key_features'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Key Features'),
      '#default_value' => $config->get('product_page.accordion_titles.key_features'),
      '#required' => true,
    ];
    $form['product_page']['accordion_titles']['technical_specs'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Technical Specs'),
      '#default_value' => $config->get('product_page.accordion_titles.technical_specs'),
      '#required' => true,
    ];
    $form['product_page']['accordion_titles']['energy_rating'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Energy Rating'),
      '#default_value' => $config->get('product_page.accordion_titles.energy_rating'),
      '#required' => true,
    ];
    $form['product_page']['accordion_titles']['product_fiche'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Product Fiche'),
      '#default_value' => $config->get('product_page.accordion_titles.product_fiche'),
      '#required' => true,
    ];
    $form['product_page']['accordion_titles']['data_sheet'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Data Sheet (for sinks and taps)'),
      '#default_value' => $config->get('product_page.accordion_titles.data_sheet'),
      '#required' => true,
    ];
    $form['product_page']['accordion_titles']['user_guide'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Product User Guide'),
      '#default_value' => $config->get('product_page.accordion_titles.user_guide'),
      '#required' => true,
    ];

    //Warranty registration
    $form['warranty_registration'] = array(
      '#type' => 'details',
      '#title' => $this->t('Warranty registration'),
    );

    $form['warranty_registration']['page_header'] = [
      '#type' => 'text_format',
      '#format' => 'full_html',
      '#title' => $this->t('Warranty registration step one header'),
      '#default_value' => $config->get('warranty_registration.page_header'),
      '#required' => false,
    ];

    $form['warranty_registration']['step_one']['serial_num_empty_message'] = [
      '#type' => 'textfield',
      '#size' => 200,
      '#maxlength' => 255,
      '#title' => $this->t('Serial number does not exist error message'),
      '#default_value' => $config->get('warranty_registration.step_one.serial_num_empty_message'),
      '#required' => false,
    ];

    $form['warranty_registration']['step_one']['serial_num_invalid_message'] = [
      '#type' => 'textfield',
      '#size' => 200,
      '#maxlength' => 255,
      '#title' => $this->t('Invalid serial number error message'),
      '#default_value' => $config->get('warranty_registration.step_one.serial_num_invalid_message'),
      '#required' => false,
    ];

    $form['warranty_registration']['step_one']['banner_colour'] = array(
      '#type' => 'color',
      '#title' => $this->t('Banner background colour'),
      '#default_value' => $config->get('warranty_registration.step_one.banner_colour'),
      '#required' => false,
    );

    $form['warranty_registration']['step_one']['left_banner_text'] = [
      '#type' => 'text_format',
      '#format' => 'full_html',
      '#title' => $this->t('Left banner text'),
      '#default_value' => $config->get('warranty_registration.step_one.left_banner_text'),
      '#required' => false,
    ];

    $form['warranty_registration']['step_one']['right_banner_text'] = [
      '#type' => 'text_format',
      '#format' => 'full_html',
      '#title' => $this->t('Right banner text'),
      '#default_value' => $config->get('warranty_registration.step_one.right_banner_text'),
      '#required' => false,
    ];

    $form['warranty_registration']['step_three']['extended_warr_options'] = [
      '#type' => 'text_format',
      '#format' => 'full_html',
      '#title' => $this->t('Extended warranty options text'),
      '#default_value' => $config->get('warranty_registration.step_three.extended_warr_options'),
      '#required' => false,
    ];

    $form['warranty_registration']['step_three']['extended_warr_options_checkbox'] = [
      '#type' => 'textfield',
      '#size' => 150,
      '#maxlength' => 128,
      '#title' => $this->t('Extended warranty options checkbox text'),
      '#default_value' => $config->get('warranty_registration.step_three.extended_warr_options_checkbox'),
      '#required' => false,
    ];

    $form['warranty_registration']['step_three']['warranty_signup_text'] = [
      '#type' => 'text_format',
      '#format' => 'full_html',
      '#title' => $this->t('Warranty registration Sign Up text'),
      '#default_value' => $config->get('warranty_registration.step_three.warranty_signup_text'),
      '#required' => false,
    ];

    $form['warranty_registration']['step_three']['warranty_signup_checkbox_text_rg_appliances'] = [
      '#type' => 'textfield',
      '#size' => 150,
      '#maxlength' => 128,
      '#title' => $this->t('Warranty registration Sign Up to Rangemaster Appliances checkbox text'),
      '#default_value' => $config->get('warranty_registration.step_three.warranty_signup_checkbox_text_rg_appliances'),
      '#required' => false,
    ];

    $form['warranty_registration']['step_three']['warranty_signup_checkbox_text_rg_sinks_taps'] = [
      '#type' => 'textfield',
      '#size' => 150,
      '#maxlength' => 128,
      '#title' => $this->t('Warranty registration Sign Up to Rangemaster Sinks and Taps checkbox text'),
      '#default_value' => $config->get('warranty_registration.step_three.warranty_signup_checkbox_text_rg_sinks_taps'),
      '#required' => false,
    ];

    $form['warranty_registration']['step_three']['warranty_signup_checkbox_text_rg_cookware'] = [
      '#type' => 'textfield',
      '#size' => 150,
      '#maxlength' => 128,
      '#title' => $this->t('Warranty registration Sign Up to Rangemaster Cookware checkbox text'),
      '#default_value' => $config->get('warranty_registration.step_three.warranty_signup_checkbox_text_rg_cookware'),
      '#required' => false,
    ];

    $form['warranty_registration']['step_three']['warranty_policy'] = [
      '#type' => 'text_format',
      '#format' => 'full_html',
      '#title' => $this->t('Warranty registration Policy'),
      '#default_value' => $config->get('warranty_registration.step_three.warranty_policy'),
      '#required' => false,
    ];

    $form['warranty_registration']['step_three']['warranty_policy_checkbox_text'] = [
      '#type' => 'textfield',
      '#size' => 150,
      '#maxlength' => 128,
      '#title' => $this->t('Warranty registration Policy checkbox text'),
      '#default_value' => $config->get('warranty_registration.step_three.warranty_policy_checkbox_text'),
      '#required' => false,
    ];

    $form['warranty_registration']['step_three']['warranty_policy_error_message'] = [
      '#type' => 'textfield',
      '#size' => 150,
      '#maxlength' => 128,
      '#title' => $this->t('Warranty registration Policy error message'),
      '#default_value' => $config->get('warranty_registration.step_three.warranty_policy_error_message'),
      '#required' => false,
    ];

    $form['warranty_registration']['success_submit_message'] = [
      '#type' => 'text_format',
      '#format' => 'full_html',
      '#title' => $this->t('Message after successfully submit'),
      '#default_value' => $config->get('warranty_registration.success_submit_message'),
      '#required' => false,
    ];

    $form['warranty_registration']['email_letter'] = [
      '#type' => 'textarea',
      '#rows' => 15,
      '#title' => $this->t('Email letter'),
      '#default_value' => $config->get('warranty_registration.email_letter'),
      '#required' => false,
      '#description' => t("
        <p>You may enter data from this view as per the 'Replacement patterns' below:</p>
        <p>[serial_number_field] == Serial number;</p>
        <p>[make] == Make of product;</p>
        <p>[model] == Model of product;</p>
        <p>[fuel] == Fuel of product;</p>
        <p>[purchase_date] == Purchase date of product;</p>
        <p>[converted_to_lpg] == Whether product has been converted to LPG;</p>
        <p>[commercial_purposes] == Whether product is used for commercial purposes;</p>
        <p>[build_date] == Build date of product;</p>
        <p>[sale_date] == Sale date of product;</p>
        <br>
        <p>[title] == Title of person;</p>
        <p>[first_name] == First name of person;</p>
        <p>[surname] == Surname of person;</p>
        <p>[email] == Email of person;</p>
        <p>[telephone_number] == Telephone number of person;</p>
        <p>[mobile_number] == Mobile number of person;</p>
        <p>[postcode_field] == Postcode of person;</p>
        <p>[address_one] == Address one field of person;</p>
        <p>[address_two] == Address two field of person;</p>
        <p>[address_three] == Address three field of person;</p>
        <p>[town] == Town of person;</p>
        <p>[county] == County of person;</p>
      "),
    ];

    //Warranty repair
    $form['warranty_repair'] = array(
      '#type' => 'details',
      '#title' => $this->t('Warranty repair'),
    );

    $form['warranty_repair']['repair_page_header'] = [
      '#type' => 'text_format',
      '#format' => 'full_html',
      '#title' => $this->t('Warranty repair step one header'),
      '#default_value' => $config->get('warranty_repair.page_header'),
      '#required' => false,
    ];

    $form['warranty_repair']['step_one']['repair_subheader_warning'] = [
      '#type' => 'text_format',
      '#format' => 'full_html',
      '#title' => $this->t('Warranty repair step one subheader warning'),
      '#default_value' => $config->get('warranty_repair.step_one.repair_subheader_warning'),
      '#required' => false,
    ];

    $form['warranty_repair']['step_one']['repair_serial_num_empty_message'] = [
      '#type' => 'textfield',
      '#size' => 200,
      '#maxlength' => 255,
      '#title' => $this->t('Serial number does not exist error message'),
      '#default_value' => $config->get('warranty_repair.step_one.serial_num_empty_message'),
      '#required' => false,
    ];

    $form['warranty_repair']['step_one']['repair_serial_num_invalid_message'] = [
      '#type' => 'textfield',
      '#size' => 200,
      '#maxlength' => 255,
      '#title' => $this->t('Invalid serial number error message'),
      '#default_value' => $config->get('warranty_repair.step_one.serial_num_invalid_message'),
      '#required' => false,
    ];

    $form['warranty_repair']['step_three']['repair_extended_warr_options'] = [
      '#type' => 'text_format',
      '#format' => 'full_html',
      '#title' => $this->t('Extended warranty options text'),
      '#default_value' => $config->get('warranty_repair.step_three.extended_warr_options'),
      '#required' => false,
    ];

    $form['warranty_repair']['step_three']['repair_extended_warr_options_checkbox'] = [
      '#type' => 'textfield',
      '#size' => 150,
      '#maxlength' => 128,
      '#title' => $this->t('Extended warranty options checkbox text'),
      '#default_value' => $config->get('warranty_repair.step_three.extended_warr_options_checkbox'),
      '#required' => false,
    ];

    $form['warranty_repair']['step_three']['repair_warranty_signup_text'] = [
      '#type' => 'text_format',
      '#format' => 'full_html',
      '#title' => $this->t('Warranty repair Sign Up text'),
      '#default_value' => $config->get('warranty_repair.step_three.warranty_signup_text'),
      '#required' => false,
    ];

    $form['warranty_repair']['step_three']['repair_warranty_signup_checkbox_text_rg_appliances'] = [
      '#type' => 'textfield',
      '#size' => 150,
      '#maxlength' => 128,
      '#title' => $this->t('Warranty repair Sign Up to Rangemaster Appliances checkbox text'),
      '#default_value' => $config->get('warranty_repair.step_three.warranty_signup_checkbox_text_rg_appliances'),
      '#required' => false,
    ];
    $form['warranty_repair']['step_three']['repair_warranty_signup_checkbox_text_rg_sinks_taps'] = [
      '#type' => 'textfield',
      '#size' => 150,
      '#maxlength' => 128,
      '#title' => $this->t('Warranty repair Sign Up to Rangemaster Sinks and Taps checkbox text'),
      '#default_value' => $config->get('warranty_repair.step_three.warranty_signup_checkbox_text_rg_sinks_taps'),
      '#required' => false,
    ];
    $form['warranty_repair']['step_three']['repair_warranty_signup_checkbox_text_rg_cookware'] = [
      '#type' => 'textfield',
      '#size' => 150,
      '#maxlength' => 128,
      '#title' => $this->t('Warranty repair Sign Up to Rangemaster Cookware checkbox text'),
      '#default_value' => $config->get('warranty_repair.step_three.warranty_signup_checkbox_text_rg_cookware'),
      '#required' => false,
    ];

    $form['warranty_repair']['step_three']['repair_warranty_policy'] = [
      '#type' => 'text_format',
      '#format' => 'full_html',
      '#title' => $this->t('Warranty repair Policy'),
      '#default_value' => $config->get('warranty_repair.step_three.warranty_policy'),
      '#required' => false,
    ];

    $form['warranty_repair']['step_three']['repair_warranty_policy_checkbox_text'] = [
      '#type' => 'textfield',
      '#size' => 150,
      '#maxlength' => 128,
      '#title' => $this->t('Warranty repair Policy checkbox text'),
      '#default_value' => $config->get('warranty_repair.step_three.warranty_policy_checkbox_text'),
      '#required' => false,
    ];

    $form['warranty_repair']['step_three']['repair_warranty_policy_error_message'] = [
      '#type' => 'textfield',
      '#size' => 150,
      '#maxlength' => 128,
      '#title' => $this->t('Warranty repair Policy error message'),
      '#default_value' => $config->get('warranty_repair.step_three.warranty_policy_error_message'),
      '#required' => false,
    ];

    $form['warranty_repair']['repair_success_submit_message'] = [
      '#type' => 'text_format',
      '#format' => 'full_html',
      '#title' => $this->t('Message after successfully submit'),
      '#default_value' => $config->get('warranty_repair.success_submit_message'),
      '#required' => false,
    ];

    $form['warranty_repair']['repair_email_letter'] = [
      '#type' => 'textarea',
      '#rows' => 15,
      '#title' => $this->t('Email letter'),
      '#default_value' => $config->get('warranty_repair.email_letter'),
      '#required' => false,
      '#description' => t("
        <p>You may enter data from this view as per the 'Replacement patterns' below:</p>
        <p>[serial_number_field] == Serial number;</p>
        <p>[make] == Make of product;</p>
        <p>[model] == Model of product;</p>
        <p>[fuel] == Fuel of product;</p>
        <p>[purchase_date] == Purchase date of product;</p>
        <p>[converted_to_lpg] == Whether product has been converted to LPG;</p>
        <p>[commercial_purposes] == Whether product is used for commercial purposes;</p>
        <p>[product_fault] == The fault with product;</p>
        <p>[build_date] == Build date of product;</p>
        <p>[sale_date] == Sale date of product;</p>
        <br>
        <p>[title] == Title of person;</p>
        <p>[first_name] == First name of person;</p>
        <p>[surname] == Surname of person;</p>
        <p>[email] == Email of person;</p>
        <p>[telephone_number] == Telephone number of person;</p>
        <p>[mobile_number] == Mobile number of person;</p>
        <p>[postcode_field] == Postcode of person;</p>
        <p>[address_one] == Address one field of person;</p>
        <p>[address_two] == Address two field of person;</p>
        <p>[address_three] == Address three field of person;</p>
        <p>[town] == Town of person;</p>
        <p>[county] == County of person;</p>
      "),
    ];

    //Store text for Store managers an Sales managers
    $form['store'] = array(
      '#type' => 'details',
      '#title' => $this->t('Store'),
    );

    $form['store']['store_manager_text'] = [
      '#type' => 'text_format',
      '#format' => 'full_html',
      '#title' => $this->t('Store manager initial screen text '),
      '#default_value' => $config->get('store.store_manager_text'),
      '#required' => false,
    ];

    $form['store']['sales_manager_text'] = [
      '#type' => 'text_format',
      '#format' => 'full_html',
      '#title' => $this->t('Sales manager initial screen text '),
      '#default_value' => $config->get('store.sales_manager_text'),
      '#required' => false,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state)
  {

    $config = $this->config('rg_additional_form_fields.settings');

    // Warranty API
    $config->set('warranty_api', $form_state->getValue('warranty_api'));

    //brochure page
    $config->set('brochure.post_code_api_login', $form_state->getValue('post_code_api_login'));
    $config->set('brochure.post_code_api_password', $form_state->getValue('post_code_api_password'));
    $config->set('brochure.field_header', $form_state->getValue('field_header')['value']);
    $config->set('brochure.field_email_placeholder', $form_state->getValue('field_email_placeholder'));
    $config->set('brochure.field_signup_text', $form_state->getValue('field_signup_text')['value']);
    $config->set('brochure.field_policy', $form_state->getValue('field_policy')['value']);

    //product page
    $config->set('product_page.gallery', $form_state->getValue('gallery')['value']);

    $config->set('product_page.default', $form_state->getValue('default')['value']);
    $config->set('product_page.builtin', $form_state->getValue('builtin')['value']);
    $config->set('product_page.fridge', $form_state->getValue('fridge')['value']);
    $config->set('product_page.hood', $form_state->getValue('hood')['value']);
    $config->set('product_page.sink', $form_state->getValue('sink')['value']);
    $config->set('product_page.tap', $form_state->getValue('tap')['value']);
    $config->set('product_page.cooker_60', $form_state->getValue('cooker_60')['value']);
    $config->set('product_page.splashback', $form_state->getValue('splashback')['value']);
    $config->set('product_page.disposal', $form_state->getValue('disposal')['value']);

    $config->set('product_page.bottom_block', $form_state->getValue('bottom_block')['value']);
    $config->set('product_page.accordion_titles.key_features', $form_state->getValue('key_features'));
    $config->set('product_page.accordion_titles.technical_specs', $form_state->getValue('technical_specs'));
    $config->set('product_page.accordion_titles.energy_rating', $form_state->getValue('energy_rating'));
    $config->set('product_page.accordion_titles.product_fiche', $form_state->getValue('product_fiche'));
    $config->set('product_page.accordion_titles.data_sheet', $form_state->getValue('data_sheet'));
    $config->set('product_page.accordion_titles.user_guide', $form_state->getValue('user_guide'));

    //Warranty registration
    $config->set('warranty_registration.page_header', $form_state->getValue('page_header')['value']);
    $config->set('warranty_registration.step_one.serial_num_empty_message', $form_state->getValue('serial_num_empty_message'));
    $config->set('warranty_registration.step_one.serial_num_invalid_message', $form_state->getValue('serial_num_invalid_message'));
    $config->set('warranty_registration.step_one.banner_colour', $form_state->getValue('banner_colour'));
    $config->set('warranty_registration.step_one.left_banner_text', $form_state->getValue('left_banner_text')['value']);
    $config->set('warranty_registration.step_one.right_banner_text', $form_state->getValue('right_banner_text')['value']);
    $config->set('warranty_registration.step_three.extended_warr_options', $form_state->getValue('extended_warr_options')['value']);
    $config->set('warranty_registration.step_three.extended_warr_options_checkbox', $form_state->getValue('extended_warr_options_checkbox'));
    $config->set('warranty_registration.step_three.warranty_signup_text', $form_state->getValue('warranty_signup_text')['value']);
    $config->set('warranty_registration.step_three.warranty_signup_checkbox_text_rg_appliances', $form_state->getValue('warranty_signup_checkbox_text_rg_appliances'));
    $config->set('warranty_registration.step_three.warranty_signup_checkbox_text_rg_sinks_taps', $form_state->getValue('warranty_signup_checkbox_text_rg_sinks_taps'));
    $config->set('warranty_registration.step_three.warranty_signup_checkbox_text_rg_cookware', $form_state->getValue('warranty_signup_checkbox_text_rg_cookware'));
    $config->set('warranty_registration.step_three.warranty_policy', $form_state->getValue('warranty_policy')['value']);
    $config->set('warranty_registration.step_three.warranty_policy_checkbox_text', $form_state->getValue('warranty_policy_checkbox_text'));
    $config->set('warranty_registration.step_three.warranty_policy_error_message', $form_state->getValue('warranty_policy_error_message'));
    $config->set('warranty_registration.success_submit_message', $form_state->getValue('success_submit_message')['value']);
    $config->set('warranty_registration.email_letter', $form_state->getValue('email_letter'));

    //Warranty repair
    $config->set('warranty_repair.page_header', $form_state->getValue('repair_page_header')['value']);
    $config->set('warranty_repair.step_one.repair_subheader_warning', $form_state->getValue('repair_subheader_warning')['value']);
    $config->set('warranty_repair.step_one.serial_num_empty_message', $form_state->getValue('repair_serial_num_empty_message'));
    $config->set('warranty_repair.step_one.serial_num_invalid_message', $form_state->getValue('repair_serial_num_invalid_message'));
    $config->set('warranty_repair.step_three.extended_warr_options', $form_state->getValue('repair_extended_warr_options')['value']);
    $config->set('warranty_repair.step_three.extended_warr_options_checkbox', $form_state->getValue('repair_extended_warr_options_checkbox'));
    $config->set('warranty_repair.step_three.warranty_signup_text', $form_state->getValue('repair_warranty_signup_text')['value']);
    $config->set('warranty_repair.step_three.warranty_signup_checkbox_text_rg_appliances', $form_state->getValue('repair_warranty_signup_checkbox_text_rg_appliances'));
    $config->set('warranty_repair.step_three.warranty_signup_checkbox_text_rg_sinks_taps', $form_state->getValue('repair_warranty_signup_checkbox_text_rg_sinks_taps'));
    $config->set('warranty_repair.step_three.warranty_signup_checkbox_text_rg_cookware', $form_state->getValue('repair_warranty_signup_checkbox_text_rg_cookware'));
    $config->set('warranty_repair.step_three.warranty_policy', $form_state->getValue('repair_warranty_policy')['value']);
    $config->set('warranty_repair.step_three.warranty_policy_checkbox_text', $form_state->getValue('repair_warranty_policy_checkbox_text'));
    $config->set('warranty_repair.step_three.warranty_policy_error_message', $form_state->getValue('repair_warranty_policy_error_message'));
    $config->set('warranty_repair.success_submit_message', $form_state->getValue('repair_success_submit_message')['value']);
    $config->set('warranty_repair.email_letter', $form_state->getValue('repair_email_letter'));

    //Store text for Store managers an Sales managers
    $config->set('store.store_manager_text', $form_state->getValue('store_manager_text')['value']);
    $config->set('store.sales_manager_text', $form_state->getValue('sales_manager_text')['value']);


    $config->save();
    return parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames()
  {
    return [
      'rg_additional_form_fields.settings',
    ];
  }
}
