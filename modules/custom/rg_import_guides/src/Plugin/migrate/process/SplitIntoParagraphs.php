<?php

namespace Drupal\rg_import_guides\Plugin\migrate\process;

use Drupal\file\Entity\File;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;
use Drupal\paragraphs\Entity\Paragraph;

/**
* Split an HTML blob into paragraphs.
 *
 * This version assumes that the HTML is a sequence of <p> elements. If the <p>
 * tag wraps a single <img> element, then create a Media paragraph. Otherwise,
 * create a Text Area paragraph.
 *
 * Return an array of arrays. The inner arrays are keyed by 'target_id' and
 * 'target_revision_id', suitable for passing into a Paragraph field.
 *
 * Example:
 *
 * @code
* process:
 *   field_paragraph:
 *     plugin: split_into_paragraphs
* @endcode
*
 * @MigrateProcessPlugin(
 *   id = "split_into_paragraphs"
  * )
 */
class SplitIntoParagraphs extends ProcessPluginBase {

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    $paragraphs = [];
    $source = $row->getSource();
    $code = $row->getDestinationProperty('field_code');

    ksort($source, SORT_NATURAL);
    foreach ($source as $name => $version) {
      if (strpos($name, "Rev") === 0 && $version) {
        preg_match('/(\d+?)/', $name, $match);
        $i = array_pop($match);
        $date = $source["DOI Issue $i"];
        $paragraphs[] = $this->createParagraph($code, $version, $date);
      }
    }

    return $paragraphs;
  }


  /**
   * Create a Text Area paragraph.
   *
   * @param string $blob
   *   The HTML string to use as the main text field.
   *
   * @return int[]
   *   An array of entity/revision IDs keyed by 'target_id' and
   *   'target_revision_id'.
   */
  protected function createParagraph($code = null, $version, $date = null) {
    $dateTime = \DateTime::createFromFormat('d.m.Y', $date);
    if ($dateTime) {
      $newDateString = $dateTime->format('Y-m-d');
    }

    $version_number = (int) preg_replace('/[^0-9]/', '', $version);
    $version_letter = preg_replace('/[^a-zA-Z]/', '', $version);
    $version_letter = strtoupper($version_letter);

    $paragraph = Paragraph::create([
      'type' => 'user_guide_version',
      'field_date' => $newDateString ?: null,
      'field_version_number' => $version_number,
      'field_version_letter' => $version_letter,
    ]);
    $file = $this->findFile($code, $version);
    if ($file) {
      $paragraph->field_pdf->setValue(['target_id' => $file->id()]);
    }
    $paragraph->save();

    return [
      'target_id' => $paragraph->id(),
      'target_revision_id' => $paragraph->getRevisionId(),
    ];
  }

  /**
   * @param $code
   * @param $version
   * @return \Drupal\Core\Entity\EntityInterface|File|null
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function findFile($code, $version) {
    $file = null;

    $path = \Drupal::service('file_system')->realpath(file_default_scheme() . "://");
    $dir = $path . '/user_guides';
    $files = scandir($dir);

    //to find needed pdf file among existing in directory
    foreach ($files as $filename) {
      $version = str_pad($version, 2, '0', STR_PAD_LEFT);
      $needed_name = "$code-$version";
      $match_name = strpos($filename, $needed_name);

      if ($match_name || $match_name === 0) {
        $source = 'public://user_guides/' . $filename;
        $destination = 'public://user_guides_uploaded/' . $filename;
        $uri = file_unmanaged_copy($source, $destination, FILE_EXISTS_REPLACE);
        if ($uri) {
          //create file entity
          $file = File::Create([
            'uri' => $uri,
          ]);
          $file->save();
          break;
        }
      }
    }

    return $file;
  }
}