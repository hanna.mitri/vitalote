<?php

namespace Drupal\rg_import_guides\Plugin\migrate\process;

use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;

/**
 * Returns value of input array or JSON string using JSONPath.
 *
 * Example of usage:
 * @code
 * process:
 *   title:
 *     -
 *       plugin: trim
 *       source: source_field
 * @endcode
 *
 * @MigrateProcessPlugin(
 *   id = "trim"
 * )
 */
class Trim extends ProcessPluginBase
{
  /**
   * @param $value
   * @param MigrateExecutableInterface $migrate_executable
   * @param Row $row
   * @param $destination_property
   * @return array|int|mixed|string
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property)
  {
    if (is_array($value)) {
      return array_map("trim", $value);
    }
    else {
      return trim($value);
    }
  }
}