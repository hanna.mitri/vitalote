<?php

namespace Drupal\rg_import_guides\Plugin\migrate\process;

use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;

/**
 * Returns value of input array or JSON string using JSONPath.
 *
 * Example of usage:
 * @code
 * process:
 *   title:
 *     -
 *       plugin: build_guide_title
 *       source: source_field
 * @endcode
 *
 * @MigrateProcessPlugin(
 *   id = "build_guide_title"
 * )
 */
class Title extends ProcessPluginBase
{
  /**
   * @param $value
   * @param MigrateExecutableInterface $migrate_executable
   * @param Row $row
   * @param $destination_property
   * @return array|int|mixed|string
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property)
  {
    preg_match('/(?<=,)\d+?(?=\))/', $value[1], $match);
    if (is_numeric($value[1])) {
      $result = (int)$value[1];
    } elseif (!empty($match)) {
      $result = array_pop($match);
    } else  {
      $result = 'No title';
    }
    return $value[0] . $result;
  }
}