<?php

namespace Drupal\rg_import_builtin\Plugin\migrate\process;

use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;

/**
 * Returns value of input array or JSON string using JSONPath.
 *
 * Example of usage:
 * @code
 * process:
 *   title:
 *     -
 *       plugin: build_builtin_title
 *       source: source_field
 * @endcode
 *
 * @MigrateProcessPlugin(
 *   id = "build_builtin_title"
 * )
 */
class BuildBuiltInTitle extends ProcessPluginBase
{
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property)
  {
    $title = [];

    $attributes = $row->getSourceProperty('attr');
    foreach ($attributes as $attr) {
      if ($attr['key'] == 'BIMODEL') {
        $title[2] = $attr['value'];
      }
      if ($attr['key'] == 'RefMODEL') {
        $title[5] = $attr['value'];
      }
    }

    $title[1] = '<div class="first-line">';
    $title[3] = '</div>';
    $title[4] = '<div class="second-line">';
    $title[6] = '</div>';

    ksort($title);
    $title = implode(' ', $title);

    return $title;
  }
}
