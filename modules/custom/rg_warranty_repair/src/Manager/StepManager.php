<?php

namespace Drupal\rg_warranty_repair\Manager;

use Drupal\rg_warranty_repair\Step\StepInterface;
use Drupal\rg_warranty_repair\Step\StepsEnum;

/**
 * Class StepManager.
 *
 * @package Drupal\rg_warranty_repair\Manager
 */
class StepManager {

  /**
   * Multi steps of the form.
   *
   * @var \Drupal\rg_warranty_repair\Step\StepInterface
   */
  protected $steps;

  /**
   * StepManager constructor.
   */
  public function __construct() {
  }

  /**
   * Add a step to the steps property.
   *
   * @param \Drupal\rg_warranty_repair\Step\StepInterface $step
   *   Step of the form.
   */
  public function addStep(StepInterface $step) {
    $this->steps[$step->getStep()] = $step;
  }

  /**
   * Fetches step from steps property, If it doesn't exist, create step object.
   *
   * @param int $step_id
   *   Step ID.
   *
   * @return \Drupal\rg_warranty_repair\Step\StepInterface
   *   Return step object.
   */
  public function getStep($step_id) {
    if (isset($this->steps[$step_id])) {
      // If step was already initialized, use that step.
      // Chance is there are values stored on that step.
      $step = $this->steps[$step_id];
    }
    else {
      // Get class.
      $class = StepsEnum::map($step_id);
      // Init step.
      $step = new $class($this);
    }

    return $step;
  }

  /**
   * Get all steps.
   *
   * @return \Drupal\rg_warranty_repair\Step\StepInterface
   *   Steps.
   */
  public function getAllSteps() {
    return $this->steps;
  }

}
