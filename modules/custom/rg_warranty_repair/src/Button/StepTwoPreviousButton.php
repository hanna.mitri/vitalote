<?php

namespace Drupal\rg_warranty_repair\Button;

use Drupal\rg_warranty_repair\Step\StepsEnum;

/**
 * Class StepTwoPreviousButton.
 *
 * @package Drupal\rg_warranty_repair\Button
 */
class StepTwoPreviousButton extends BaseButton {

  /**
   * {@inheritdoc}
   */
  public function getKey() {
    return 'previous';
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    return [
      '#type' => 'submit',
      '#value' => t('Previous'),
      '#goto_step' => StepsEnum::STEP_ONE,
      '#skip_validation' => TRUE,
    ];
  }

}
