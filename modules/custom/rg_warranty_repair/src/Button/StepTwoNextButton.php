<?php

namespace Drupal\rg_warranty_repair\Button;

use Drupal\rg_warranty_repair\Step\StepsEnum;

/**
 * Class StepTwoNextButton.
 *
 * @package Drupal\rg_warranty_repair\Button
 */
class StepTwoNextButton extends BaseButton {

  /**
   * {@inheritdoc}
   */
  public function getKey() {
    return 'next';
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    return [
      '#type' => 'submit',
      '#value' => t('Next'),
      '#goto_step' => StepsEnum::STEP_THREE,
    ];
  }

}
