<?php

namespace Drupal\rg_warranty_repair\Traits;

trait SubmitForm
{
  /**
   * @param $email
   */
  public function subscribtion($email)
  {
    $subscription_manager = \Drupal::service('simplenews.subscription_manager');
    $langcode = \Drupal::languageManager()->getCurrentLanguage()->getId();
    $newsletter_id = 'warranty_repair_service_request';
    $subscription_manager->subscribe($email, $newsletter_id, FALSE, 'mass subscribe', $langcode);
  }

  /**
   * @param $fields
   * @throws \Exception
   */
  public function addToDatabase($fields)
  {
    foreach($fields as $key => $value){
      if($value === ''){
        unset($fields[$key]);
      }
    }

    \Drupal::database()->insert('rangemaster_warranty_repair')
      -> fields($fields)
      -> execute();
  }

  /**
   * @param $params
   * @param $to
   */
  public function sendEmail($params, $to)
  {
    $newMail = \Drupal::service('plugin.manager.mail');
    $langcode = \Drupal::currentUser()->getPreferredLangcode();
    $params['email_letter'] = $this->config('rg_additional_form_fields.settings')
      ->get('warranty_repair.email_letter');
    $newMail->mail('rg_email_sender', 'warrantyRepairMail', $to, $langcode, $params, $reply = NULL, $send = TRUE);
  }

  public function exportCSV()
  {
    $connection = \Drupal::database();
    $query = $connection->select('rangemaster_warranty_repair', 'rg_w_rep')
      ->fields('rg_w_rep')
      ->orderBy('rg_w_rep.id', 'DESC');
    $result = $query->execute()->fetchAssoc();
    $result["product_fault"] = trim(preg_replace('/\s+/', ' ', $result["product_fault"]));

    if(count($result) > 0){
      $delimiter = "|";
      $filename = "warr_repair_" . $result["id"] . "_" . date('Y-m-d') . ".csv";

      //create a file pointer
      $dir = DRUPAL_ROOT;
      $filename = $dir . '/../Input/Warranty_repair/' . $filename;
      $dirname = dirname($filename);
      if (!is_dir($dirname)) {
        mkdir($dirname, 0755, true);
      }
      $f = fopen($filename, 'w');

      //set column headers
      $fields = array(
        'ID',
        'Serial number',
        'Serial number validated',
        'Make',
        'Model',
        'Fuel',
        'Purchase date',
        'Converted to LPG',
        'Commercial purposes',
        'Product fault',
        'Build date',
        'Sale date',
        'Title',
        'First name',
        'Surname',
        'Email',
        'Telephone number',
        'Mobile number',
        'Postcode',
        'Address one',
        'Address two',
        'Address three',
        'Town',
        'County',
        'Extended warranty options',
        'SignUpRangemasterAppliances',
        'Created at',
        'SignUpRangemasterSinksTaps',
        'SignUpRangemasterCookware',
      );
      fputcsv($f, $fields, $delimiter);

      fputcsv($f, $result, $delimiter);

      //move back to beginning of file
      fseek($f, 0);

      //output all remaining data on a file pointer
      fpassthru($f);
    }
  }
}