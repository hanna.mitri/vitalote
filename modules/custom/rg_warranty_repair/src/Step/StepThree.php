<?php

namespace Drupal\rg_warranty_repair\Step;

use Drupal\Core\Render\Markup;
use Drupal\rg_warranty_repair\Button\StepThreeFinishButton;
use Drupal\rg_warranty_repair\Button\StepThreePreviousButton;
use Drupal\rg_warranty_repair\Validator\ValidatorRequired;

/**
 * Class StepThree.
 *
 * @package Drupal\rg_warranty_repair\Step
 */
class StepThree extends BaseStep {

  /**
   * {@inheritdoc}
   */
  protected function setStep() {
    return StepsEnum::STEP_THREE;
  }

  /**
   * {@inheritdoc}
   */
  public function getButtons() {
    return [
      new StepThreePreviousButton(),
      new StepThreeFinishButton(),
    ];
  }

  /**`
   * {@inheritdoc}
   */
  public function buildStepFormElements() {

    $form['step_three_title'] = [
      '#markup' => Markup::create(t('Terms and Conditions')),
      '#prefix' => '<div id="step_title" class="step-title">',
      '#suffix' => '</div>',
      '#weight' => -99,
    ];

    $form['extended_warranty_options'] = [
      '#markup' => Markup::create($this->config->get('warranty_repair.step_three.extended_warr_options')),
      '#weight' => 1,
    ];

    $form['extended_warranty_options_checkbox'] = [
      '#type' => 'checkbox',
      '#title' =>
        t('@ext_checkbox', ['@ext_checkbox' => $this->config->get('warranty_repair.step_three.extended_warr_options_checkbox')]),
      '#weight' => 2,
      '#prefix' => '<div class="step-three-checkbox-field">',
      '#suffix' => '</div>',
      '#default_value' => isset($this->getValues()['extended_warranty_options_checkbox']) ? $this->getValues()['extended_warranty_options_checkbox'] : 0,
    ];

    $form['signup_text'] = [
      '#markup' => Markup::create($this->config->get('warranty_repair.step_three.warranty_signup_text')),
      '#weight' => 3,
    ];

    $form['signup_wrapper'] = [
      '#prefix' => '<div class="step-three-checkbox-field">',
      '#suffix' => '</div>',
      '#weight' => 4
    ];

    $form['signup_wrapper']['signup_checkbox_rg_appliances'] = [
      '#type' => 'checkbox',
      '#title' =>
        t('@signup_rg_appliances', ['@signup_rg_appliances' => $this->config->get('warranty_repair.step_three.warranty_signup_checkbox_text_rg_appliances')]),
      '#weight' => 4,
      '#default_value' => isset($this->getValues()['signup_checkbox_rg_appliances']) ? $this->getValues()['signup_checkbox_rg_appliances'] : 0,
    ];

    $form['signup_wrapper']['signup_checkbox_rg_sinks_taps'] = [
      '#type' => 'checkbox',
      '#title' =>
        t('@signup_rg_sinks_taps', ['@signup_rg_sinks_taps' => $this->config->get('warranty_repair.step_three.warranty_signup_checkbox_text_rg_sinks_taps')]),
      '#weight' => 4,
      '#default_value' => isset($this->getValues()['signup_checkbox_rg_sinks_taps']) ? $this->getValues()['signup_checkbox_rg_sinks_taps'] : 0,
    ];

    $form['signup_wrapper']['signup_checkbox_rg_cookware'] = [
      '#type' => 'checkbox',
      '#title' =>
        t('@signup_rg_cookware', ['@signup_rg_cookware' => $this->config->get('warranty_repair.step_three.warranty_signup_checkbox_text_rg_cookware')]),
      '#weight' => 4,
      '#default_value' => isset($this->getValues()['signup_checkbox_rg_cookware']) ? $this->getValues()['signup_checkbox_rg_cookware'] : 0,
    ];

    $form['warranty_policy'] = [
      '#markup' => Markup::create($this->config->get('warranty_repair.step_three.warranty_policy')),
      '#weight' => 5,
    ];

    $form['warranty_policy_checkbox'] = [
      '#type' => 'checkbox',
      '#title' =>
        t('@policy', ['@policy' => $this->config->get('warranty_repair.step_three.warranty_policy_checkbox_text')]),
      '#weight' => 6,
      '#required' => true,
      '#validated' => TRUE,
      '#prefix' => '<div class="step-three-checkbox-field">',
      '#suffix' => '</div>',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function getFieldNames() {
    return [
      'extended_warranty_options_checkbox',
      'signup_checkbox_rg_appliances',
      'signup_checkbox_rg_sinks_taps',
      'signup_checkbox_rg_cookware',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFieldsValidators() {
    return [
      'warranty_policy_checkbox' => [
        new ValidatorRequired(t('@policy',
          ['@policy' => $this->config->get('warranty_repair.step_three.warranty_policy_error_message')])),
      ],
    ];
  }

}
