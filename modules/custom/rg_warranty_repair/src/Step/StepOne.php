<?php

namespace Drupal\rg_warranty_repair\Step;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Ajax\InsertCommand;
use Drupal\Core\Ajax\InvokeCommand;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Markup;
use Drupal\rg_warranty_repair\Button\StepOneNextButton;
use Drupal\rg_warranty_repair\Traits\ValidatesInside;
use Drupal\rg_warranty_repair\Validator\ValidatorRequired;
use GuzzleHttp\Exception\ClientException;

/**
 * Class StepOne.
 *
 * @package Drupal\rg_warranty_repair\Step
 */
class StepOne extends BaseStep
{

  use ValidatesInside;

  /**
   * {@inheritdoc}
   */
  protected function setStep()
  {
    return StepsEnum::STEP_ONE;
  }

  /**
   * {@inheritdoc}
   */
  public function getButtons()
  {
    return [
      new StepOneNextButton(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildStepFormElements()
  {
    $form['page_header'] = [
      '#markup' => Markup::create($this->config->get('warranty_repair.page_header')),
      '#prefix' => '<div id="page-header-wrapper" class="page-header">',
      '#suffix' => '</div>',
      '#weight' => -99,
    ];

    $form['subheader_warning'] = [
      '#markup' => Markup::create($this->config->get('warranty_repair.step_one.repair_subheader_warning')),
      '#prefix' => '<div id="page-header-wrapper" class="page-header">',
      '#suffix' => '</div>',
      '#weight' => -99,
    ];

    $form['step_one_title'] = [
      '#markup' => Markup::create(t('Product Details')),
      '#prefix' => '<div id="step_one_title" class="step-title">',
      '#suffix' => '</div>',
      '#weight' => -99,
    ];

    $form['serial_number']['serial_number_field'] = [
      '#type' => 'textfield',
      '#required' => FALSE,
      '#placeholder' => t('Serial number'),
      '#weight' => 10,
      '#attributes' => [
        'id' => 'serial_number_field'
      ],
      '#default_value' => isset($this->getValues()['serial_number_field']) ? $this->getValues()['serial_number_field'] : NULL,
    ];

    if(isset($this->getValues()['serial_number_is_validated']) && $this->getValues()['serial_number_is_validated'] == 1){
      $form['serial_number']['serial_number_field']['#attributes'] += [
        'readonly' => ''
      ];
    }

    $form['serial_number']['serial_number_is_validated'] = [
      '#type' => 'hidden',
      '#weight' => 10,
      '#validated' => TRUE,
      '#prefix' => '<div id="serial_number_is_validated">',
      '#suffix' => '</div>',
      '#default_value' => isset($this->getValues()['serial_number_is_validated']) ? $this->getValues()['serial_number_is_validated'] : 0,
    ];

    $form['serial_number']['dont_have_serial_number'] = [
      '#type' => 'button',
      '#weight' => 11,
      '#value' => t('I do not have a serial number'),
      '#skip_validation' => TRUE,
      '#attributes' => [
        'class' => [
          'dont-have-serial-number',
          'custom-button'
        ]
      ],
      '#ajax' => [
        'callback' => [$this, 'getMakes'],
        'event' => 'click',
        'wrapper' => 'choose_make_field',
        'progress' => ['type' => 'none'],
      ]
    ];

    $form['serial_number']['validate_serial_number'] = [
      '#type' => 'button',
      '#weight' => 12,
      '#value' => t('Validate'),
      '#validate' => ['getInsideValidators'],
      '#attributes' => [
        'class' => [
          'validate-serial-number',
          'custom-button'
        ]
      ],
      '#ajax' => [
        'event' => 'click',
        'callback' => [$this, 'ajaxValidateSerialNumber'],
        'wrapper' => 'choose_model_validated_group_wrapper'
      ],
    ];

    $form['choose_model'] = [
      '#prefix' => '<div id="choose_model_wrapper" class="choose-model-form">',
      '#suffix' => '</div>',
    ];

    $form['choose_model']["validated_group"] = [
      '#prefix' => '<div id="choose_model_validated_group_wrapper" class="choose-model-validated-group">',
      '#suffix' => '</div>',
    ];

    $form['choose_model']['validated_group']['make'] = [
      '#type' => 'select',
      '#options' => [],
      '#empty_option' => isset($this->getValues()['make']) ? [$this->getValues()['make'] => $this->getValues()['make']] : t('Make'),
      '#validated' => TRUE,
      '#required' => true,
      '#weight' => 13,
      '#prefix' => '<div id="choose_make_field" class="col-md-6 make-field without-padding-left">',
      '#suffix' => '</div>',
      '#attributes' => [
        'class' => [
          'make-from-api'
        ],
      ],
      '#ajax' => [
        'event' => 'change',
        'callback' => [$this, 'ajaxGetModel'],
        'wrapper' => 'choose_model_field',
        'progress' => ['type' => 'none'],
      ],
    ];

    $form['choose_model']['validated_group']['model'] = [
      '#type' => 'select',
      '#options' => [],
      '#empty_option' => isset($this->getValues()['model']) ? [$this->getValues()['model'] => $this->getValues()['model']] : t('Model'),
      '#validated' => TRUE,
      '#required' => true,
      '#weight' => 14,
      '#prefix' => '<div id="choose_model_field" class="col-md-6 model-field without-padding-right">',
      '#suffix' => '</div>',
      '#ajax' => [
        'callback' => [$this, 'ajaxGetFuel'],
        'event' => 'change',
        'wrapper' => 'choose_fuel_field',
        'progress' => ['type' => 'none'],
      ],
    ];

    $form['choose_model']['validated_group']['fuel'] = [
      '#type' => 'select',
      '#options' => [],
      '#required' => true,
      '#empty_option' => isset($this->getValues()['fuel']) ? [$this->getValues()['fuel'] => $this->getValues()['fuel']] : t('Fuel'),
      '#validated' => TRUE,
      '#weight' => 15,
      '#prefix' => '<div id="choose_fuel_field" class="col-md-6 fuel-field without-padding-left">',
      '#suffix' => '</div>',
    ];

    $form['choose_model']['purchase_date'] = [
      '#type' => 'date',
      '#weight' => 16,
      '#title' => t('Purchase date:'),
      '#prefix' => '<div class="col-md-6 purchase-date-field without-padding-right">',
      '#suffix' => '</div>',
      '#required' => true,
      '#validated' => TRUE,
      '#default_value' => isset($this->getValues()['purchase_date']) ? $this->getValues()['purchase_date'] : NULL,
    ];

    $form['choose_model']['converted_to_lpg'] = [
      '#type' => 'radios',
      '#title' => t('Has your Range cooker been converted to LPG?'),
      '#default_value' => isset($this->getValues()['converted_to_lpg']) ? $this->getValues()['converted_to_lpg'] : NULL,
      '#options' => [
        'yes' => t('Yes'),
        'no' => t('No'),
        'not_applicable' => t('Not Applicable')
      ],
      '#prefix' => '<div class="col-md-6 without-padding-left checkboxes-field">',
      '#suffix' => '</div>',
      '#required' => true,
      '#weight' => 17,
      '#validated' => TRUE,
    ];

    $form['choose_model']['commercial_purposes'] = [
      '#type' => 'radios',
      '#title' => t('Is your product used for commercial purposes?'),
      '#default_value' => isset($this->getValues()['commercial_purposes']) ? $this->getValues()['commercial_purposes'] : NULL,
      '#options' => [
        'yes' => t('Yes'),
        'no' => t('No'),
      ],
      '#prefix' => '<div class="col-md-6 without-padding-right checkboxes-field">',
      '#suffix' => '</div>',
      '#required' => true,
      '#weight' => 18,
      '#validated' => TRUE,
    ];

    $form['choose_model']['product_fault'] = [
      '#type' => 'textarea',
      '#title' => t('Please describe the fault with your product:'),
      '#default_value' => isset($this->getValues()['product_fault']) ? $this->getValues()['product_fault'] : NULL,
      '#required' => true,
      '#weight' => 19,
      '#validated' => TRUE,
    ];

    $form["choose_model"]["validated_group"]['build_date'] = [
      '#type' => 'hidden',
      '#weight' => 30,
      '#validated' => TRUE,
      '#default_value' => isset($this->getValues()['build_date']) ? $this->getValues()['build_date'] : NULL,
    ];

    $form["choose_model"]["validated_group"]['sale_date'] = [
      '#type' => 'hidden',
      '#weight' => 31,
      '#validated' => TRUE,
      '#default_value' => isset($this->getValues()['sale_date']) ? $this->getValues()['sale_date'] : NULL,
    ];

    return $form;
  }

  /**
   * @param $warrantyCall
   * @return mixed
   */
  public function callToApi($warrantyCall)
  {
    $login = $this->config->get('brochure.post_code_api_login');
    $password = $this->config->get('brochure.post_code_api_password');
    $siteName = $this->config->get('warranty_api');
    $client = \Drupal::httpClient();
    $apiCall = 'https://webapi.agarangemaster.co.uk/AgaRangemasterAPI/api/Warranty/' . $warrantyCall . '/' . $siteName;

    try{
      $request = $client->get($apiCall, ['auth' => [$login, $password]]);
    } catch (ClientException $e) {
      return null;
    }
    /** @noinspection PhpComposerExtensionStubsInspection */
    $response = json_decode($request->getBody());

    return $response;
  }

  public function getMakes($form) : array
  {
    $response = $this->callToApi("GetProductMakes");
    $options = array_combine($response, $response);
    $form["wrapper"]["choose_model"]["validated_group"]["make"]["#options"] += $options;
    return $form["wrapper"]["choose_model"]["validated_group"]["make"];
  }

  public function ajaxValidateSerialNumber($form, FormStateInterface $formState)
  {
    $errors = $this->validateInsideStep($formState);
    $ajax = new AjaxResponse();

    if (empty($errors)) {
      $serialNumber = $formState->getValue("serial_number_field");
      $response = $this->callToApi("ValidateSerialNo/$serialNumber");

      if (isset($response->ValidSerial) && $response->ValidSerial === 'Yes') {
        $form["wrapper"]["choose_model"]["validated_group"]["make"]["#options"] = [$response->Make => $response->Make];
        $form["wrapper"]["choose_model"]["validated_group"]["model"]["#options"] = [$response->Model => $response->Model];
        $form["wrapper"]["choose_model"]["validated_group"]["fuel"]["#options"] = [$response->Fuel => $response->Fuel];
        $form["wrapper"]["choose_model"]["validated_group"]["build_date"]["#value"] = $response->BuildDate;
        $form["wrapper"]["choose_model"]["validated_group"]["sale_date"]["#value"] = $response->SaleDate;
        $chooseModelFields = $form["wrapper"]["choose_model"]["validated_group"];

        $form['wrapper']['serial_number']['serial_number_is_validated']['#value'] = 1;
        $ser_num_is_validated = $form['wrapper']['serial_number']['serial_number_is_validated'];

        $ajax->addCommand(new InsertCommand('#choose_model_validated_group_wrapper', $chooseModelFields));
        $ajax->addCommand(new InsertCommand('#serial_number_is_validated', $ser_num_is_validated));
        $ajax->addCommand(new InvokeCommand('#serial_number_field', 'attr', ['readonly', '']));
        $ajax->addCommand(new InvokeCommand('.choose-model-form', 'show'));
      } else {
        $errorMessage =
          t('@ser_num', ['@ser_num' => $this->config->get('warranty_repair.step_one.serial_num_invalid_message')]);
        $error = $this->wrapTheError($errorMessage);
        $makeForm = $this->getMakes($form);
        $ajax->addCommand(new InsertCommand('#choose_make_field', $makeForm));
        $ajax->addCommand(new HtmlCommand('#messages-wrapper', $error));
        $ajax->addCommand(new InvokeCommand('.choose-model-form', 'show'));
      }
    } else {
      $error = $this->wrapTheError(implode(", \n", $errors));
      $ajax->addCommand(new HtmlCommand('#messages-wrapper', $error));
    }
    return $ajax;
  }

  public function ajaxGetModel($form, FormStateInterface $formState)
  {
    $make = $formState->getValue("make");
    $response = $this->callToApi("GetProductModels/$make");
    $options = array_combine($response, $response);
    $form["wrapper"]["choose_model"]["validated_group"]["model"]["#options"] += $options;

    return $form["wrapper"]["choose_model"]["validated_group"]["model"];
  }

  public function ajaxGetFuel($form, FormStateInterface $formState)
  {
    $model = $formState->getValue("model");
    $response = $this->callToApi("GetProductFuels/$model");
    $options = array_combine($response, $response);
    $form["wrapper"]['choose_model']["validated_group"]['fuel']["#options"] += $options;

    return $form["wrapper"]['choose_model']["validated_group"]['fuel'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFieldNames()
  {
    return [
      'serial_number_field',
      'serial_number_is_validated',
      'make',
      'model',
      'fuel',
      'purchase_date',
      'converted_to_lpg',
      'commercial_purposes',
      'product_fault',
      'build_date',
      'sale_date'
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFieldsValidators()
  {
    return [
      'make' => [
        new ValidatorRequired(t("Please fill out the  'Make' field")),
      ],
      'model' => [
        new ValidatorRequired(t("Please fill out the  'Model' field")),
      ],
      'fuel' => [
        new ValidatorRequired(t("Please fill out the  'Fuel' field")),
      ],
      'purchase_date' => [
        new ValidatorRequired(t("Please fill out the  'Purchase Date' field")),
      ],
      'converted_to_lpg' => [
        new ValidatorRequired(t("Please fill out the  'Has your Range cooker been converted to LPG?' field")),
      ],
      'commercial_purposes' => [
        new ValidatorRequired(t("Please fill out the  'Is your product used for commercial purposes?' field")),
      ],
      'product_fault' => [
        new ValidatorRequired(t("Please fill out the  'Fault Description' field")),
      ],
    ];
  }

  public function getInsideValidators(): array
  {
    return [
      'serial_number_field' => [
        new ValidatorRequired(t('@ser_num_empty',
          ['@ser_num_empty' => $this->config->get('warranty_repair.step_one.serial_num_empty_message')])),
      ],
    ];
  }

}
