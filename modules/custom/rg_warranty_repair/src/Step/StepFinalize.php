<?php

namespace Drupal\rg_warranty_repair\Step;

use Drupal\Core\Render\Markup;
use Drupal\Core\Url;

/**
 * Class StepFinalize.
 *
 * @package Drupal\rg_warranty_repair\Step
 */
class StepFinalize extends BaseStep {

  /**
   * {@inheritdoc}
   */
  protected function setStep() {
    return StepsEnum::STEP_FINALIZE;
  }

  /**
   * {@inheritdoc}
   */
  public function getButtons() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function buildStepFormElements() {

    $form['step_finalize_title'] = [
      '#markup' => t('Sucessfully Submitted'),
      '#weight' => 1,
      '#prefix' => '<div id="step_title" class="step-title">',
      '#suffix' => '</div>',
    ];

    $form['success_submit_message'] = [
      '#markup' =>
        Markup::create($this->config->get('warranty_repair.success_submit_message')),
      '#weight' => 2,
    ];

    $form['go_back_link'] = [
      '#type' => 'link',
      '#title' => t('Go back to homepage'),
      '#url' => Url::fromRoute('rg_warranty_repair.page'),
      '#weight' => 3,
    ];

    return $form;
  }

}
