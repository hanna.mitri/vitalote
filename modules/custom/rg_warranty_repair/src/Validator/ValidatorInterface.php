<?php

namespace Drupal\rg_warranty_repair\Validator;

/**
 * Interface ValidatorInterface.
 *
 * @package Drupal\rg_warranty_repair\Validator
 */
interface ValidatorInterface {

  /**
   * Returns bool indicating if validation is ok.
   */
  public function validates($value);

  /**
   * Returns error message.
   */
  public function getErrorMessage();

}
