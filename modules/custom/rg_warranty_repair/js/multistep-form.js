jQuery(document).ready(function ($) {
      //hide-show choose model form
      var dontHaveSerialNumber = $('.dont-have-serial-number');
      var chooseModelForm = $('.choose-model-form');

      chooseModelForm.once().hide();

      dontHaveSerialNumber.once().click(function () {
        chooseModelForm.show('slow');
      });

      //for scrolling to top after submit (next-previous)
      $.fn.customScrollTop = function() {
            $("html, body").animate({ scrollTop: 0 }, 5);
      };

});