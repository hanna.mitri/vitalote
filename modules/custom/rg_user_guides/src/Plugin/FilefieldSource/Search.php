<?php

/**
 * @file
 * Contains \Drupal\rg_user_guides\Plugin\FilefieldSource\Search.
 */

namespace Drupal\rg_user_guides\Plugin\FilefieldSource;

use Drupal\Core\File\FileSystem;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\filefield_sources\FilefieldSourceInterface;
use Drupal\Core\Field\WidgetInterface;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Site\Settings;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Route;

/**
 * A FileField source plugin to allow use of files within a server directory.
 *
 * @FilefieldSource(
 *   id = "search",
 *   name = @Translation("File search from server directory"),
 *   label = @Translation("File search"),
 *   description = @Translation("Select a file from a directory on the server."),
 *   weight = 3
 * )
 */
class Search implements FilefieldSourceInterface {

  /**
   * {@inheritdoc}
   */
  public static function value(array &$element, &$input, FormStateInterface $form_state) {
    if (!empty($input['filefield_search']['filename'])) {
      $fileStorage = \Drupal::entityTypeManager()->getStorage('field_config');
      $fileSystem = \Drupal::service('file_system');
      $instance = $fileStorage->load($element['#entity_type'] . '.' . $element['#bundle'] . '.' . $element['#field_name']);
      $filepath = $input['filefield_search']['filename'];

      // Check that the destination is writable.
      $directory = $element['#upload_location'];
      $mode = Settings::get('file_chmod_directory', FileSystem::CHMOD_DIRECTORY);
      // This first chmod check is for other systems such as S3, which don't
      // work with file_prepare_directory().
      if (!$fileSystem->chmod($directory, $mode) && !$fileSystem->prepareDirectory($directory, FileSystemInterface::CREATE_DIRECTORY)) {
        \Drupal::logger('filefield_sources')->log(E_NOTICE, 'File %file could not be copied, because the destination directory %destination is not configured correctly.', array(
          '%file' => $filepath,
          '%destination' => $fileSystem->realpath($directory),
        ));
        MessengerInterface::addMessage(t('The specified file %file could not be copied, because the destination directory is not properly configured. This may be caused by a problem with file or directory permissions. More information is available in the system log.', array('%file' => $filepath)), 'error');
        return;
      }

      // Clean up the file name extensions and transliterate.
      $original_filepath = $filepath;
      $new_filepath = filefield_sources_clean_filename($filepath, $instance->getSetting('file_extensions'));
      if (file_exists($filepath) && $filepath != $new_filepath) {
        rename($filepath, $new_filepath);
      }
      $filepath = $new_filepath;

      // Run all the normal validations, minus file size restrictions.
      $validators = $element['#upload_validators'];
      if (isset($validators['file_validate_size'])) {
        unset($validators['file_validate_size']);
      }

      // Save the file to the new location.
      if ($file = filefield_sources_save_file($filepath, $validators, $directory)) {
        if (!in_array($file->id(), $input['fids'])) {
          $input['fids'][] = $file->id();
        }

        // Delete the original file if "moving" the file instead of copying.
        if ($element['#filefield_sources_settings']['source_search']['search_mode'] !== FILEFIELD_SOURCE_SEARCH_MODE_COPY) {
          @unlink($filepath);
        }
      }

      // Restore the original file name if the file still exists.
      if (file_exists($filepath) && $filepath != $original_filepath) {
        rename($filepath, $original_filepath);
      }

      $input['filefield_search']['filename'] = '';
    }
  }


  /**
   * {@inheritdoc}
   */
  public static function process(array &$element, FormStateInterface $form_state, array &$complete_form) {

    $element['filefield_search'] = array(
      '#weight' => 100.5,
      '#theme' => 'filefield_sources_element',
      '#source_id' => 'search',
      // Required for proper theming.
      '#filefield_source' => TRUE,
      '#filefield_sources_hint_text' => FILEFIELD_SOURCE_SEARCH_HINT_TEXT,
    );

    $settings = $element['#filefield_sources_settings']['source_search'];
    $autocomplete_route_parameters = array(
      'entity_type' => $element['#entity_type'],
      'bundle_name' => $element['#bundle'],
      'field_name' => $element['#field_name'],
      'path' => $settings['path'],
      'absolute' => $settings['absolute'],
    );

    $element['filefield_search']['filename'] = array(
      '#type' => 'textfield',
      '#autocomplete_route_name' => 'rg_user_guides_sources.autocomplete',
      '#autocomplete_route_parameters' => $autocomplete_route_parameters,
      '#description' => filefield_sources_element_validation_help($element['#upload_validators']),
    );

    $class = '\Drupal\file\Element\ManagedFile';
    $ajax_settings = [
      'callback' => [$class, 'uploadAjaxCallback'],
      'options' => [
        'query' => [
          'element_parents' => implode('/', $element['#array_parents']),
        ],
      ],
      'wrapper' => $element['upload_button']['#ajax']['wrapper'],
      'effect' => 'fade',
    ];

    $element['filefield_search']['select'] = [
      '#name' => implode('_', $element['#parents']) . '_autocomplete_select',
      '#type' => 'submit',
      '#value' => t('Upload!'),
      '#validate' => [],
      '#submit' => ['filefield_sources_field_submit'],
      '#limit_validation_errors' => [$element['#parents']],
      '#ajax' => $ajax_settings,
    ];

    return $element;
  }

  /**
   * Theme the output of the search element.
   */
  public static function element($variables) {
    $element = $variables['element'];

    $element['filename']['#field_suffix'] = \Drupal::service('renderer')->render($element['select']);
    return '<div class="filefield-source filefield-source-search clear-block">' . \Drupal::service('renderer')->render($element['filename']) . '</div>';
  }


  /**
   * Menu callback; autocomplete.js callback to return a list of files.
   */
  public static function autocomplete(Request $request, $entity_type, $bundle_name, $field_name, $path, $absolute)
  {
    $matches = [];
    $string = strtolower($request->query->get('q'));

    if (isset($string)) {

      $path = static::getDirectory($path, $absolute);

      if (\Drupal::service('file_system')->prepareDirectory($path, FileSystemInterface::CREATE_DIRECTORY)) {


        $pattern = !empty($extensions) ? '/\.(' . strtr($extensions, ' ', '|') . ')$/' : '/.*/';
        $files = file_scan_directory($path, $pattern);

        if (count($files)) {
          foreach ($files as $file) {
            $filename = strtolower($file->name);
            if (strpos($filename, $string) !== false) {
              $key = str_replace(' ', '%20', $file->uri);
              $label = str_replace($path . '/', '', $file->uri);
              $matches[] = array('value' => $key, 'label' => $label);
            }
          }
        }
      }
    }
    return new JsonResponse($matches);
  }


  /**
   * Get search options.
   *
   * @param string $path
   *   Path to scan files.
   * @param string $extensions
   *   Path to scan files.
   *
   * @return array
   *   List of options.
   */
  protected static function getSearchOptions($path, $extensions = FALSE) {
    if (!\Drupal::service('file_system')->prepareDirectory($path, FileSystemInterface::CREATE_DIRECTORY)) {
      MessengerInterface::addMessage(t('Specified file search path %path must exist or be writable.', array('%path' => $path)), 'error');
      return FALSE;
    }

    $options = array();
    $pattern = !empty($extensions) ? '/\.(' . strtr($extensions, ' ', '|') . ')$/' : '/.*/';
    $files = file_scan_directory($path, $pattern);

    if (count($files)) {
      $options = array('' => t('-- Select file --'));
      foreach ($files as $file) {
        $options[str_replace(' ', '%20', $file->uri)] = str_replace($path . '/', '', $file->uri);
      }
      natcasesort($options);
    }

    return $options;
  }

  /**
   * Define routes for Reference source.
   *
   * @return array
   *   Array of routes.
   */
  public static function routes() {
    $routes = array();

    $routes['rg_user_guides_sources.autocomplete'] = new Route(
      '/file/search-in-folder/{entity_type}/{bundle_name}/{field_name}/{path}/{absolute}',
      array(
        '_controller' => get_called_class() . '::autocomplete',
      ),
      array(
        '_access_filefield_sources_field' => 'TRUE',
      )
    );

    return $routes;
  }

  /**
   * Implements hook_filefield_source_settings().
   */
  public static function settings(WidgetInterface $plugin)
  {
    $settings = $plugin->getThirdPartySetting('filefield_sources', 'filefield_sources', array(
      'source_search' => [
        'autocomplete' => FILEFIELD_SOURCE_SEARCH_MATCH_STARTS_WITH,
        'path' => FILEFIELD_SOURCE_SEARCH_DEFAULT_PATH,
        'absolute' => FILEFIELD_SOURCE_SEARCH_RELATIVE,
        'search_mode' => FILEFIELD_SOURCE_SEARCH_MODE_MOVE,
      ],
    ));

    $return['source_search'] = array(
      '#title' => t('File search settings'),
      '#type' => 'details',
      '#description' => t('File search allows for selecting a file from a directory on the server, commonly used in combination with FTP.') . ' <strong>' . t('This file source will ignore file size checking when used.') . '</strong>',
      '#element_validate' => array(array(get_called_class(), 'filePathValidate')),
      '#weight' => 3,
    );

    $return['source_search']['path'] = array(
      '#type' => 'textfield',
      '#title' => t('File search path'),
      '#default_value' => $settings['source_search']['path'],
      '#size' => 60,
      '#maxlength' => 128,
      '#description' => t('The directory within the <em>File search location</em> that will contain searchable files.'),
    );
    if (\Drupal::moduleHandler()->moduleExists('token')) {
      $return['source_search']['tokens'] = array(
        '#theme' => 'token_tree',
        '#token_types' => array('user'),
      );
    }
    $return['source_search']['absolute'] = array(
      '#type' => 'radios',
      '#title' => t('File search location'),
      '#options' => array(
        FILEFIELD_SOURCE_SEARCH_RELATIVE => t('Within the files directory'),
        FILEFIELD_SOURCE_SEARCH_ABSOLUTE => t('Absolute server path'),
      ),
      '#default_value' => $settings['source_search']['absolute'],
      '#description' => t('The <em>File search path</em> may be with the files directory (%file_directory) or from the root of your server. If an absolute path is used and it does not start with a "/" your path will be relative to your site directory: %realpath.', array('%file_directory' => drupal_realpath(file_default_scheme() . '://'), '%realpath' => realpath('./'))),
    );
    $return['source_search']['search_mode'] = array(
      '#type' => 'radios',
      '#title' => t('Search method'),
      '#options' => array(
        FILEFIELD_SOURCE_SEARCH_MODE_MOVE => t('Move the file directly to the final location'),
        FILEFIELD_SOURCE_SEARCH_MODE_COPY => t('Leave a copy of the file in the search directory'),
      ),
      '#default_value' => isset($settings['source_search']['search_mode']) ? $settings['source_search']['search_mode'] : 'copy',
    );

    $return['source_search']['autocomplete'] = array(
      '#title' => t('Match file name'),
      '#options' => array(
        FILEFIELD_SOURCE_SEARCH_MATCH_STARTS_WITH => t('Starts with'),
        FILEFIELD_SOURCE_SEARCH_MATCH_CONTAINS => t('Contains'),
      ),
      '#type' => 'radios',
      '#default_value' => isset($settings['source_search']['autocomplete']) ? $settings['source_search']['autocomplete'] : FILEFIELD_SOURCE_SEARCH_MATCH_STARTS_WITH,
    );

    return $return;
  }




  /**
   * Get directory from settings.
   *
   * @param array $settings
   *   Search source's settings.
   * @param object $account
   *   User to replace token.
   *
   * @return string
   *   Path that contains files to search.
   */
  protected static function getDirectory($path, $absolute, $account = NULL) {
    $account = isset($account) ? $account : \Drupal::currentUser();
    $absolute = !empty($settings['absolute']);

    // Replace user level tokens.
    // Node level tokens require a lot of complexity like temporary storage
    // locations when values don't exist. See the filefield_paths module.
    if (\Drupal::moduleHandler()->moduleExists('token')) {
      $token = \Drupal::token();
      $path = $token->replace($path, array('user' => $account));
    }

    return $absolute ? $path : file_default_scheme() . '://' . $path;
  }


  /**
   * Validate file path.
   *
   * @param array $element
   *   Form element.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state.
   * @param array $complete_form
   *   Complete form.
   */
  public static function filePathValidate(array &$element, FormStateInterface $form_state, array &$complete_form) {
    $parents = $element['#parents'];
    array_pop($parents);
    $input_exists = FALSE;

    // Get input of the whole parent element.
    $input = NestedArray::getValue($form_state->getValues(), $parents, $input_exists);
    if ($input_exists) {
      // Only validate if this source is enabled.
      if (!$input['sources']['search']) {
        return;
      }

      // Strip slashes from the end of the file path.
      $filepath = rtrim($element['path']['#value'], '\\/');
      $form_state->setValueForElement($element['path'], $filepath);
      $filepath = static::getDirectory(
        $input['source_search']['path'],
        $input["source_search"]["absolute"]
      );
      // Check that the directory exists and is writable.
      if (!\Drupal::service('file_system')->prepareDirectory($filepath, FileSystemInterface::CREATE_DIRECTORY)) {
        $form_state->setError($element['path'], t('Specified file search path %path must exist or be writable.', array('%path' => $filepath)));
      }
    }
  }

}
