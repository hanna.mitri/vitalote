<?php
/**
 * @file
 * Definition of Drupal\rg_store\Plugin\views\filter\RelatedProducts.
 */
namespace Drupal\rg_user_guides\Plugin\views\filter;

use Drupal\Core\Database\Database;
use Drupal\rg_store\StoreHelper;
use Drupal\views\Plugin\views\display\DisplayPluginBase;
use Drupal\views\Plugin\views\filter\FilterPluginBase;
use Drupal\views\Plugin\views\filter\InOperator;
use Drupal\views\ViewExecutable;
use Drupal\views\Views;

/**
 * Filters by given list of related content title options.
 *
 * @ingroup views_filter_handlers
 *
 * @ViewsFilter("rg_user_guides_not_empty")
 */
class ExistingGuides extends FilterPluginBase {


  /**
   * {@inheritdoc}
   */
  public function init(ViewExecutable $view, DisplayPluginBase $display, array &$options = NULL) {
    parent::init($view, $display, $options);
    $this->valueTitle = t('Node Domain filter');
  }


  public function query() {
    //Get the current domain.

   $configuration = [
      'table' => 'node__field_files',
      'field' => 'entity_id',
      'left_table' => 'node_field_data',
      'left_field' => 'nid',
      'operator' => '='
    ];
    $join = Views::pluginManager('join')->createInstance('standard', $configuration);
    $this->query->addRelationship('node__field_files', $join, 'node_field_data');


   $configuration = [
      'table' => 'paragraph__field_pdf',
      'field' => 'entity_id',
      'left_table' => 'node__field_files',
      'left_field' => 'field_files_target_id',
      'operator' => '='
    ];
    $join = Views::pluginManager('join')->createInstance('standard', $configuration);
    $this->query->addRelationship('paragraph__field_pdf', $join, 'node__field_files');


    $this->query->addWhere('AND', 'paragraph__field_pdf.deleted', 0);
  }

}