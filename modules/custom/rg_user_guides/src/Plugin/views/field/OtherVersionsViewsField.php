<?php

namespace Drupal\rg_user_guides\Plugin\views\field;

use Drupal\Core\Form\FormStateInterface;
use Drupal\paragraphs\Entity\Paragraph;
use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;

/**
 * A handler to provide a field that is completely custom by the administrator.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("other_versions_views_field")
 */
class OtherVersionsViewsField extends FieldPluginBase {

  /**
   * {@inheritdoc}
   */
  public function usesGroupBy() {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    // Do nothing -- to override the parent query.
  }

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();

    $options['hide_alter_empty'] = ['default' => FALSE];
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $values) {

    // built array with available versions
    $paragraphs = $values->_entity->field_files->getValue();
    foreach ($paragraphs as $version_id) {
      $paragraph = Paragraph::load($version_id['target_id']);

      if (!$paragraph->get('field_pdf')->isEmpty()) {
        $version_number = $paragraph->field_version_number->value;
        $version_letter = $paragraph->field_version_letter->value;

        //date of first version
        if (empty($version_letter) || empty($data[$version_number]) ||
          (string) $version_letter < (string) $data[$version_number]['version_letter_value']) {
          $data[$version_number]['date'] = $paragraph->field_date->view([
              'type' => 'datetime_custom',
              'settings' => ['date_format' => '\D\O\I: d/m/Y '],
              'label' => 'hidden'
            ]
          );
        }
        //other data of last version
        if (empty($data[$version_number]['version_number']) || !empty($version_letter) ||
          (string) $version_letter > (string) $data[$version_number]['version_letter_value']) {
          $data[$version_number]['version_number'] = $paragraph->field_version_number->view(['label' => 'inline']);
          $data[$version_number]['version_letter'] = $paragraph->field_version_letter->view(['label' => 'hidden']);
          $data[$version_number]['pdf'] = $paragraph->field_pdf->view(['label' => 'hidden']);
          $data[$version_number]['version_letter_value'] = $version_letter;
        }
      }
    }
    krsort($data);
    array_shift($data);

    $output = [
      '#theme' => 'user_guide_list',
      '#guides' => $data
    ];
    return render($output);
  }

}
