<?php

namespace Drupal\rg_user_guides\Plugin\views\field;

use Drupal\Core\Form\FormStateInterface;
use Drupal\paragraphs\Entity\Paragraph;
use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;

/**
 * A handler to provide a field that is completely custom by the administrator.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("last_version_views_field")
 */
class LastVersionViewsField extends FieldPluginBase {

  /**
   * {@inheritdoc}
   */
  public function usesGroupBy() {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    // Do nothing -- to override the parent query.
  }

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();

    $options['hide_alter_empty'] = ['default' => FALSE];
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $values) {

    // built array with available versions
    $versions = [];
    $paragraphs = $values->_entity->field_files->getValue();
    foreach ($paragraphs as $version_id) {
      $paragraph = Paragraph::load($version_id['target_id']);
      if (!$paragraph->get('field_pdf')->isEmpty()) {
        $version_number = $paragraph->field_version_number->value;
        $version_letter = $paragraph->field_version_letter->value;
        $versions[$paragraph->id()] = $version_number . ' ' . $version_letter;
      }
    }
    if (empty($versions)) {
      return null;
    }
    // sort versions
    $sorted_versions = $versions;
    sort($sorted_versions, SORT_NATURAL);

    //get last version
    $last_version = array_pop($sorted_versions);
    $last_version = Paragraph::load(array_search($last_version, $versions));
    $last_version_number = $last_version->field_version_number->value;

    //filter to get all versions with the same number
    $filtered_versions = array_filter($versions, function ($var) use ($last_version_number)  {
      return strpos($var, $last_version_number) === 0;
    });
    //get first version with same number
    $first_version = array_shift($filtered_versions);
    $first_version = Paragraph::load(array_search($first_version, $versions));


    //date of first version
    $data[0]['date'] = $first_version->field_date->view([
        'type' => 'datetime_custom',
        'settings' => ['date_format' => '\D\O\I: d/m/Y '],
        'label' => 'hidden'
      ]
    );
    //other data of last version
    $data[0]['version_number'] = $last_version->field_version_number->view(['label' => 'inline']);
    $data[0]['version_letter'] = $last_version->field_version_letter->view(['label' => 'hidden']);
    $data[0]['pdf'] = $last_version->field_pdf->view(['label' => 'hidden']);

    $output = [
      '#theme' => 'user_guide_list',
      '#guides' => $data
    ];
    return render($output);
  }

}
