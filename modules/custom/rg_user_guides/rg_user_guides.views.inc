<?php

/**
 * @file
 * Contains rg_user_guides\rg_user_guides.views.inc..
 * Provide a custom views field data that isn't tied to any other module. */


/**
* Implements hook_views_data().
*/
function rg_user_guides_views_data() {

    $data['node']['last_version_views_field'] = [
        'title' => t('Last guide version'),
        'help' => t('Last guide version'),
        'field' => [
            'id' => 'last_version_views_field',
        ],
    ];

    $data['node']['other_versions_views_field'] = [
        'title' => t('Other guide versions'),
        'help' => t('Other guide versions'),
        'field' => [
            'id' => 'other_versions_views_field',
        ],
    ];

    return $data;
}