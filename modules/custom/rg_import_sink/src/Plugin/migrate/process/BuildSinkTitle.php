<?php

namespace Drupal\rg_import_sink\Plugin\migrate\process;

use Drupal\commerce_product\Entity\ProductAttributeValue;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;
use Drupal\taxonomy\Entity\Term;

/**
 * Returns value of input array or JSON string using JSONPath.
 *
 * Example of usage:
 * @code
 * process:
 *   title:
 *     -
 *       plugin: build_sink_title
 *       source: source_field
 * @endcode
 *
 * @MigrateProcessPlugin(
 *   id = "build_sink_title"
 * )
 */
class BuildSinkTitle extends ProcessPluginBase
{
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property)
  {
    $title = [];

    $attributes = $row->getSourceProperty('attr');
    foreach ($attributes as $attr) {
      if ($attr['key'] == 'ProductFamily') {
        $title[1] = $attr['value'];
        continue;
      }
      if ($attr['key'] == 'SinMatTyp') {
        $title[2] = $attr['value'];
        continue;
      }
      if ($attr['key'] == 'ProductType') {
        $title[3] = $attr['value'] ;
        continue;
      }
      if ($attr['key'] == 'SinNoOfBow') {
        $title[4] = $attr['value'] ;
        continue;
      }
      if ($attr['key'] == 'colour') {
        $title[5] = $attr['value'];
        continue;
      }

    }

    $title[1] = '<div class="first-line">' . $title[1];
    $title[3] .= ' Sink</div>';
    $title[4] = '<div class="second-line">' . $title[4];
    $title[6] = '</div>';

    ksort($title);
    $title = implode(' ', $title);

    return $title;
  }
}