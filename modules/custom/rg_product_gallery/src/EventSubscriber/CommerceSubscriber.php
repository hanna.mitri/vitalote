<?php

namespace Drupal\rg_product_gallery\EventSubscriber;

use Drupal\commerce_product\Event\ProductEvents;
use Drupal\commerce_product\Event\ProductVariationAjaxChangeEvent;
use Drupal\Core\Ajax\HtmlCommand;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Drupal\rg_product\ProductHelper;

/**
 * Class CommerceAjaxSubscriber.
 */
class CommerceSubscriber implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  static function getSubscribedEvents() {
    return [
      ProductEvents::PRODUCT_VARIATION_AJAX_CHANGE => ['onResponse', 51],
    ];
  }

  /**
   * Respond to AJAX variation update.
   */
  public function onResponse(ProductVariationAjaxChangeEvent $event) {
    $product_variation = $event->getProductVariation();
    $response = $event->getResponse();
    $gallery = ProductHelper::getProductGallery($product_variation);
    $gallery = \Drupal::service('renderer')->render($gallery);

    $response->addCommand(new HtmlCommand(".gallery-section-wrapper", $gallery));
  }
}

