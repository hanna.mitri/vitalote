jQuery(document).ready(function ($) {
      //hide-show choose model form
      var dontHaveSerialNumber = $('.dont-have-serial-number');
      var chooseModelForm = $('.choose-model-form');

      chooseModelForm.once().hide();

      dontHaveSerialNumber.once().click(function () {
        chooseModelForm.show('slow');
      });

      //change banner background colour
      var bgColourValue = $('#step_one_banner_colour').val();
      var banner = $('.step-one-banner');

      banner.css('background-color', bgColourValue);

      //for scrolling to top after submit (next-previous)
      $.fn.customScrollTop = function() {
            $("html, body").animate({ scrollTop: 0 }, 5);
      };

});