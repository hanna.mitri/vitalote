(function ($, Drupal, drupalSettings) {
  Drupal.behaviors.multistep = {
    attach: function (context, settings) {

      //change banner background colour
      var bgColourValue = $('#step_one_banner_colour').val();
      var banner = $('.step-one-banner');

      banner.css('background-color', bgColourValue);
    }
  };
})(jQuery, Drupal, drupalSettings);