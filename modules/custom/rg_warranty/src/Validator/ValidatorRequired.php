<?php

namespace Drupal\rg_warranty\Validator;

/**
 * Class ValidatorRequired.
 *
 * @package Drupal\rg_warranty\Validator
 */
class ValidatorRequired extends BaseValidator {

  /**
   * {@inheritdoc}
   */
  public function validates($value) {
    $value = strip_tags(trim($value));
    return is_array($value) ? !empty(array_filter($value)) : !empty($value);
  }

}
