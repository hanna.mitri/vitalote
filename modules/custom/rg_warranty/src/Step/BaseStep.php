<?php

namespace Drupal\rg_warranty\Step;

/**
 * Class BaseStep.
 *
 * @package Drupal\rg_warranty\Step
 */
abstract class BaseStep implements StepInterface {

  /**
   * Multi steps of the form.
   *
   * @var StepInterface
   */
  protected $step;

  /**
   * Values of element.
   *
   * @var array
   */
  protected $values;

  /**
   * \Drupal::config(). From additional form fields.
   *
   * @var array
   */
  protected $config;

  /**
   * BaseStep constructor.
   */
  public function __construct() {
    $this->step = $this->setStep();
    $this->config = \Drupal::config('rg_additional_form_fields.settings');
  }

  /**
   * {@inheritdoc}
   */
  public function getStep() {
    return $this->step;
  }

  /**
   * {@inheritdoc}
   */
  public function isLastStep() {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function setValues($values) {
    $this->values = $values;
  }

  /**
   * {@inheritdoc}
   */
  public function getValues() {
    return $this->values;
  }

  /**
   * {@inheritdoc}
   */
  public function getFieldNames() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getFieldsValidators() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  abstract protected function setStep();

}
