<?php

namespace Drupal\rg_warranty\Step;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\AlertCommand;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Ajax\InvokeCommand;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Markup;
use Drupal\rg_warranty\Button\StepTwoNextButton;
use Drupal\rg_warranty\Button\StepTwoPreviousButton;
use Drupal\rg_warranty\Validator\ValidatorRegex;
use Drupal\rg_warranty\Validator\ValidatorRequired;

/**
 * Class StepTwo.
 *
 * @package Drupal\rg_warranty\Step
 */
class StepTwo extends BaseStep {

  /**
   * {@inheritdoc}
   */
  protected function setStep() {
    return StepsEnum::STEP_TWO;
  }

  /**
   * {@inheritdoc}
   */
  public function getButtons() {
    return [
      new StepTwoPreviousButton(),
      new StepTwoNextButton(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildStepFormElements() {

    $form['step_two_title'] = [
      '#markup' => Markup::create(t('My Details')),
      '#prefix' => '<div id="step_title" class="step-title">',
      '#suffix' => '</div>',
      '#weight' => -99,
    ];

    $form['left_column'] = [
      '#prefix' => '<div id="left_column" class="col-md-6 without-padding-left left-column">',
      '#suffix' => '</div>',
    ];

    $form['left_column']['title'] = [
      '#type' => 'select',
      '#required' => true,
      '#validated' => TRUE,
      '#weight' => 10,
      '#options' => ['Mr' => 'Mr', 'Mrs' => 'Mrs', 'Miss' => 'Miss', 'Dr' => 'Dr'],
      '#empty_option' => t('Title'),
      '#default_value' => isset($this->getValues()['title']) ? $this->getValues()['title'] : [],
    ];

    $form['left_column']['first_name'] = [
      '#type' => 'textfield',
      '#required' => true,
      '#validated' => TRUE,
      '#placeholder' => t('First Name (required)'),
      '#weight' => 11,
      '#default_value' => isset($this->getValues()['first_name']) ? $this->getValues()['first_name'] : NULL,
    ];

    $form['left_column']['surname'] = [
      '#type' => 'textfield',
      '#required' => true,
      '#validated' => TRUE,
      '#placeholder' => t('Surname (required)'),
      '#weight' => 12,
      '#default_value' => isset($this->getValues()['surname']) ? $this->getValues()['surname'] : NULL,
    ];

    $form['left_column']['email'] = [
      '#type' => 'email',
      '#required' => true,
      '#validated' => TRUE,
      '#placeholder' => t('Email Address (required)'),
      '#weight' => 13,
      '#default_value' => isset($this->getValues()['email']) ? $this->getValues()['email'] : NULL,
    ];

    $form['left_column']['telephone_number'] = [
      '#type' => 'tel',
      '#required' => true,
      '#validated' => TRUE,
      '#placeholder' => t('Telephone Number (required)'),
      '#weight' => 14,
      '#default_value' => isset($this->getValues()['telephone_number']) ? $this->getValues()['telephone_number'] : NULL,
    ];

    $form['left_column']['mobile_number'] = [
      '#type' => 'tel',
      '#required' => true,
      '#validated' => TRUE,
      '#placeholder' => t('Mobile number (required)'),
      '#weight' => 15,
      '#default_value' => isset($this->getValues()['mobile_number']) ? $this->getValues()['mobile_number'] : NULL,
    ];

    $form['right_column'] = [
      '#prefix' => '<div id="right_column" class="col-md-6 without-padding-right right-column">',
      '#suffix' => '</div>',
    ];

    $form['right_column']['postcode_field'] = [
      '#type' => 'textfield',
      '#required' => true,
      '#validated' => TRUE,
      '#prefix' => '<div class="col-md-9 without-padding-left postcode-field">',
      '#suffix' => '</div>',
      '#placeholder' => t('Postcode (required)'),
      '#weight' => 16,
      '#default_value' => isset($this->getValues()['postcode_field']) ? $this->getValues()['postcode_field'] : NULL,
    ];

    $form['right_column']['postcode_button'] = [
      '#type' => 'button',
      '#value' => t('Find'),
      '#prefix' => '<div id="postcode_button_wrapper" class="postcode-button-wrapper">',
      '#suffix' => '</div>',
      '#attributes' => [
        'class' => [
          'use-ajax',
          'postcode-button',
          'col-md-3',
          'col-sm-12',
          'col-xs-12'
        ],
      ],
      '#weight' => 17,
      '#ajax' => [
        'callback' => [$this, 'getAddressesFromPostalCode'],
        'event' => 'click',
        'wrapper' => 'addresses_from_api_wrapper',
        'progress' => ['type' => 'none'],
      ],
      '#limit_validation_errors' => [],
    ];

    $form['right_column']['addresses_from_api_wrapper'] = [
      '#prefix' => '<div id="addresses_from_api_wrapper" class="form-wrapper close-popup-form">',
      '#suffix' => '</div>',
      '#weight' => 18,
    ];

    $form['right_column']['addresses_from_api_wrapper']['addresses_from_api'] = [
      '#type' => 'select',
      '#default_value' => t('Select address'),
      '#options' => [0 => t('Select address')],
      '#validated' => TRUE,
      '#weight' => 18,
      '#attributes' => [
        'class' => [
          'use-ajax',
          'addresses_from_api_select_list'
        ],
      ],
      '#ajax' => [
        'callback' => [$this, 'autocompleteFormFromAddress'],
        'event' => 'change',
        'wrapper' => 'autocomplete_fields'
      ],
    ];

    $form['right_column']['autocomplete_fields'] = [
      '#prefix' => '<div id="autocomplete_fields_wrapper">',
      '#suffix' => '</div>',
      '#weight' => 19,
    ];

    $form['right_column']['autocomplete_fields']['address_one'] = [
      '#type' => 'textfield',
      '#required' => true,
      '#validated' => TRUE,
      '#placeholder' => t('Address Line 1 (required)'),
      '#weight' => 19,
      '#default_value' => isset($this->getValues()['address_one']) ? $this->getValues()['address_one'] : NULL,
    ];

    $form['right_column']['autocomplete_fields']['address_two'] = [
      '#type' => 'textfield',
      '#placeholder' => t('Address Line 2'),
      '#weight' => 20,
      '#default_value' => isset($this->getValues()['address_two']) ? $this->getValues()['address_two'] : NULL,
    ];

    $form['right_column']['autocomplete_fields']['address_three'] = [
      '#type' => 'textfield',
      '#placeholder' => t('Address Line 3'),
      '#weight' => 21,
      '#default_value' => isset($this->getValues()['address_three']) ? $this->getValues()['address_three'] : NULL,
    ];

    $form['right_column']['autocomplete_fields']['town'] = [
      '#type' => 'textfield',
      '#placeholder' => t('Town'),
      '#weight' => 22,
      '#default_value' => isset($this->getValues()['town']) ? $this->getValues()['town'] : NULL,
    ];

    $form['right_column']['autocomplete_fields']['county'] = [
      '#type' => 'textfield',
      '#placeholder' => t('County'),
      '#weight' => 23,
      '#default_value' => isset($this->getValues()['county']) ? $this->getValues()['county'] : NULL,
    ];

    return $form;
  }

  public function call_to_api($addressLookupCall)
  {
    $login = $this->config->get('brochure.post_code_api_login');
    $password = $this->config->get('brochure.post_code_api_password');
    $client = \Drupal::httpClient();
    $apiCall = 'https://webapi.agarangemaster.co.uk/AgaRangemasterAPI/api/AddressLookup/' . $addressLookupCall;

    try{
      $request = $client->get($apiCall, ['auth' => [$login, $password]]);
    } catch (\Exception $e) {
      return null;
    }
    /** @noinspection PhpComposerExtensionStubsInspection */
    $response = json_decode($request->getBody());

    return $response;
  }

  /**
   * for autocomplete 'address 1', 'address 2', 'address 3', 'county' and 'province' fields
   * @param $form
   * @param FormStateInterface $form_state
   * @return AjaxResponse
   */
  public function autocompleteFormFromAddress($form, FormStateInterface $form_state)
  {
    $ajax = new AjaxResponse();
    $address_id = $form_state->getValue("addresses_from_api");

    if (!empty($address_id)) {

      $response = $this->call_to_api('RetrieveAddress/' . $address_id);

      $form["wrapper"]["right_column"]["autocomplete_fields"]["address_one"]["#value"] = $response[0]->Line1;
      $form["wrapper"]["right_column"]["autocomplete_fields"]["address_two"]["#value"] = $response[0]->Line2;
      $form["wrapper"]["right_column"]["autocomplete_fields"]["address_three"]["#value"] = $response[0]->Line3;
      $form["wrapper"]["right_column"]["autocomplete_fields"]["town"]["#value"] = $response[0]->City;
      $form["wrapper"]["right_column"]["autocomplete_fields"]["county"]["#value"] = $response[0]->Province;

      $autocompleteAddressField = $form["wrapper"]["right_column"]["autocomplete_fields"];
      $ajax->addCommand(new HtmlCommand('#autocomplete_fields_wrapper', $autocompleteAddressField));

      $ajax->addCommand(new InvokeCommand('#addresses_from_api_wrapper', 'addClass', ['close-popup-form']));
    }
      return $ajax;
  }

  /**
   * for call to api and get list of addresses
   * @param $form
   * @param FormStateInterface $form_state
   * @return AjaxResponse
   */
  public function getAddressesFromPostalCode($form, FormStateInterface $form_state)
  {
    $ajax = new AjaxResponse();
    $post_code = $form_state->getValue('postcode_field');

    if (!empty($post_code)) {
      $response = $this->call_to_api('FindAddress/' . $post_code);
      $options = [];

      foreach ($response as $value) {
        $options[$value->Id] = $value->Address;
      }

      $form["wrapper"]["right_column"]['addresses_from_api_wrapper']["addresses_from_api"]["#options"] += $options;
      $addressesFromApiField = $form["wrapper"]["right_column"]['addresses_from_api_wrapper']["addresses_from_api"];

      $ajax->addCommand(new HtmlCommand('#addresses_from_api_wrapper', $addressesFromApiField));
      $ajax->addCommand(new InvokeCommand('#addresses_from_api_wrapper', 'removeClass', ['close-popup-form']));

      return $ajax;
    } else {
      $ajax->addCommand(new AlertCommand(t('Failed to lookup postcode data')));
      return $ajax;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getFieldNames() {
    return [
      'title',
      'first_name',
      'surname',
      'email',
      'telephone_number',
      'mobile_number',
      'postcode_field',
      'address_one',
      'address_two',
      'address_three',
      'town',
      'county',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFieldsValidators() {
    return [
      'title' => [
        new ValidatorRequired(t("Please fill out the  'Title' field")),
      ],
      'first_name' => [
        new ValidatorRequired(t("Please fill out the  'First Name' field")),
      ],
      'surname' => [
        new ValidatorRequired(t("Please fill out the  'Surname' field")),
      ],
      'email' => [
        new ValidatorRequired(t("Please fill out the  'Email' field")),
        new ValidatorRegex(t("Invalid Email address "), '/^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,}$/'),

      ],
      'telephone_number' => [
        new ValidatorRequired(t("Please fill out the  'Telephone Number' field")),
        new ValidatorRegex(t("Invalid Telephone number (number shouldn't contains spaces)"), '/^[0-9]+$/'),
        new ValidatorRegex(t("Invalid Telephone number (need at least 10 and not more 15 digits)"), '/^[0-9]{10,15}$/'),
      ],
      'mobile_number' => [
        new ValidatorRequired(t("Please fill out the  'Mobile Number' field")),
        new ValidatorRegex(t("Invalid Mobile number (number shouldn't contains spaces)"), '/^[0-9]+$/'),
        new ValidatorRegex(t("Invalid Mobile number (need at least 10 and not more 15 digits)"), '/^[0-9]{10,15}$/'),
      ],
      'postcode_field' => [
        new ValidatorRequired(t("Please fill out the  'Postcode' field")),
        new ValidatorRegex(t("Invalid Postcode (need not more 40 characters)"), '/^[\w\s\W]{1,40}$/'),
      ],
      'address_one' => [
        new ValidatorRequired(t("Please fill out the  'Address One' field")),
      ],

    ];
  }

}
