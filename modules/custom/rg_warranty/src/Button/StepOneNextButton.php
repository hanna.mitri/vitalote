<?php

namespace Drupal\rg_warranty\Button;

use Drupal\rg_warranty\Step\StepsEnum;

/**
 * Class StepOneNextButton.
 *
 * @package Drupal\rg_warranty\Button
 */
class StepOneNextButton extends BaseButton {

  /**
   * {@inheritdoc}
   */
  public function getKey() {
    return 'next';
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    return [
      '#type' => 'submit',
      '#value' => t('Next'),
      '#goto_step' => StepsEnum::STEP_TWO,
      '#prefix' => '<div id="choose_model_next_button" class="choose-model-form hidden-form">',
      '#suffix' => '</div>',
    ];
  }

}
