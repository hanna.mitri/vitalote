<?php

namespace Drupal\rg_warranty\Button;

/**
 * Class BaseButton.
 *
 * @package Drupal\rg_warranty\Button
 */
abstract class BaseButton implements ButtonInterface {

  /**
   * {@inheritdoc}
   */
  public function ajaxify() {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function getSubmitHandler() {
    return FALSE;
  }

}
