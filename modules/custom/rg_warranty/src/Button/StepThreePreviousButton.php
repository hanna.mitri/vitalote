<?php

namespace Drupal\rg_warranty\Button;

use Drupal\rg_warranty\Step\StepsEnum;

/**
 * Class StepThreePreviousButton.
 *
 * @package Drupal\rg_warranty\Button
 */
class StepThreePreviousButton extends BaseButton {

  /**
   * {@inheritdoc}
   */
  public function getKey() {
    return 'previous';
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    return [
      '#type' => 'submit',
      '#value' => t('Previous'),
      '#goto_step' => StepsEnum::STEP_TWO,
      '#skip_validation' => TRUE,
    ];
  }

}
