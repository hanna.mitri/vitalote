<?php
/**
 * Created by PhpStorm.
 * User: roman
 * Date: 06.03.19
 * Time: 19:23
 */

namespace Drupal\rg_warranty\Traits;

use Drupal\Core\Form\FormStateInterface;
use Drupal\rg_warranty\Validator\ValidatorInterface;

trait ValidatesInside
{
  public function validateInsideStep(FormStateInterface $state) : array
  {
    $errors = [];
    if ($fields_validators = $this->getInsideValidators()) {
      // Validate fields.
      foreach ($fields_validators as $field => $validators) {
        // Validate all validators for field.
        $field_value = $state->getValue($field);
        foreach ($validators as $validator) {
          /* @var ValidatorInterface $validator */
          if (!$validator->validates($field_value)) {
            $errors[] = $validator->getErrorMessage();
          }
        }
      }
    }
    return $errors;
  }

  public function wrapTheError($errorMessage): string
  {
    $prefix = trim('
        <div role="contentinfo" aria-label="Error message" class="messages messages--error" style="">
            <div role="alert">
                <h2 class="visually-hidden">Error message</h2>
                ');
    $suffix = '</div></div>';

    return "$prefix$errorMessage$suffix";
  }

  public function getInsideValidators() : array
  {
    return [];
  }
}