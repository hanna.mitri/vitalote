<?php

namespace Drupal\rg_product_select\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * Checks that the submitted value is a unique integer.
 *
 * @Constraint(
 *   id = "UniqueProduct",
 *   label = @Translation("Unique Integer", context = "Validation"),
 *   type = "string"
 * )
 */
class UniqueProduct extends Constraint {

  // The message that will be shown if the value is not unique.
  public $notUnique = 'Product Variation "%value" are already chosen in other user guide';

}
