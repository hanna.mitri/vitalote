<?php

namespace Drupal\rg_product_select\Plugin\Validation\Constraint;

use Drupal\commerce_product\Entity\ProductVariation;
use Drupal\Core\Database\Database;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Validates the Unique Product constraint.
 */
class UniqueProductValidator extends ConstraintValidator
{

  /**
   * {@inheritdoc}
   */
  public function validate($items, Constraint $constraint)
  {
    foreach ($items as $item) {
      $value = $item->getValue()["target_id"];

      $current_node = $this->context->getRoot()->getValue();
      $current_node_id = $current_node->id();
      // Next check if the value is unique.
      if (!$this->isUnique($value, $current_node_id)) {
        $variation = ProductVariation::load($value);
        $this->context->addViolation($constraint->notUnique, ['%value' => $variation->getTitle()]);
      }
    }
  }

  /**
   * @param $variation_id
   * @return bool
   */
  private function isUnique($variation_id, $current_node_id)
  {

    $connection = Database::getConnection();
    $query = $connection->select('node__field_products', 'gp');
    $query->fields('gp', ['entity_id']);
    $query->condition('gp.field_products_target_id', $variation_id);
    $query->condition('gp.bundle', 'user_guide');
    $query->condition('gp.entity_id', $current_node_id, '<>');
    $data = $query->distinct()->execute();

    $results = $data->fetchCol();
    return !$results;

  }
}