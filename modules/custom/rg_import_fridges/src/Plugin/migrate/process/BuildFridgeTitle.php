<?php

namespace Drupal\rg_import_fridges\Plugin\migrate\process;

use Drupal\commerce_product\Entity\ProductAttributeValue;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;
use Drupal\taxonomy\Entity\Term;

/**
 * Returns value of input array or JSON string using JSONPath.
 *
 * Example of usage:
 * @code
 * process:
 *   title:
 *     -
 *       plugin: build_fridge_title
 *       source: source_field
 * @endcode
 *
 * @MigrateProcessPlugin(
 *   id = "build_fridge_title"
 * )
 */
class BuildFridgeTitle extends ProcessPluginBase
{
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property)
  {
    $title = [];

    $attributes = $row->getSourceProperty('attr');
    foreach ($attributes as $attr) {
      if ($attr['key'] == 'ProductFamily') {
        $title[1] = $attr['value'];
        continue;
      }
      if ($attr['key'] == 'ProductType2') {
        $title[2] = $attr['value'];
        continue;
      }
      if ($attr['key'] == 'colour') {
        $title[4] = $attr['value'];
        continue;
      }
      if ($attr['key'] == 'PFINISH') {
        $title[5] = 'with ' . $attr['value'] . ' trim';
        continue;
      }
    }

    $title[1] = '<div class="first-line">' . $title[1];
    $title[3] = '</div>';
    $title[4] = '<div class="second-line">' . $title[4];
    $title[6] = '</div>';

    ksort($title);
    $title = implode(' ', $title);

    return $title;
  }
}