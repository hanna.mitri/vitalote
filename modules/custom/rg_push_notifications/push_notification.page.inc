<?php

/**
 * @file
 * Contains push_notification.page.inc.
 *
 * Page callback for Push notification entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Push notification templates.
 *
 * Default template: push_notification.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_push_notification(array &$variables) {
  // Fetch PushNotificationEntity Entity Object.
  $push_notification = $variables['elements']['#push_notification'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
