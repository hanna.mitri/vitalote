<?php

namespace Drupal\rg_push_notifications;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Link;

/**
 * Defines a class to build a listing of Push notification entities.
 *
 * @ingroup rg_push_notifications
 */
class PushNotificationEntityListBuilder extends EntityListBuilder {


  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('Push notification ID');
    $header['name'] = $this->t('Name');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var $entity \Drupal\rg_push_notifications\Entity\PushNotificationEntity */
    $row['id'] = $entity->id();
    $row['name'] = Link::createFromRoute(
      $entity->label(),
      'entity.push_notification.edit_form',
      ['push_notification' => $entity->id()]
    );
    return $row + parent::buildRow($entity);
  }

}
