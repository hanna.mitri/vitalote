<?php

namespace Drupal\rg_push_notifications\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class PushNotificationEntityTypeForm.
 */
class PushNotificationEntityTypeForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $push_notification_type = $this->entity;
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $push_notification_type->label(),
      '#description' => $this->t("Label for the Push notification type."),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $push_notification_type->id(),
      '#machine_name' => [
        'exists' => '\Drupal\rg_push_notifications\Entity\PushNotificationEntityType::load',
      ],
      '#disabled' => !$push_notification_type->isNew(),
    ];

    /* You will need additional form elements for your custom properties. */

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $push_notification_type = $this->entity;
    $status = $push_notification_type->save();

    switch ($status) {
      case SAVED_NEW:
        $this->messenger()->addMessage($this->t('Created the %label Push notification type.', [
          '%label' => $push_notification_type->label(),
        ]));
        break;

      default:
        $this->messenger()->addMessage($this->t('Saved the %label Push notification type.', [
          '%label' => $push_notification_type->label(),
        ]));
    }
    $form_state->setRedirectUrl($push_notification_type->toUrl('collection'));
  }

}
