<?php

namespace Drupal\rg_push_notifications\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Push notification entities.
 *
 * @ingroup rg_push_notifications
 */
class PushNotificationEntityDeleteForm extends ContentEntityDeleteForm {


}
