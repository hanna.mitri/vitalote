<?php

namespace Drupal\rg_push_notifications\Form;

use Drupal\Core\Database\Database;
use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Form controller for Push notification edit forms.
 *
 * @ingroup rg_push_notifications
 */
class PushNotificationEntityForm extends ContentEntityForm
{

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state)
  {
    /* @var $entity \Drupal\rg_push_notifications\Entity\PushNotificationEntity */
    $form = parent::buildForm($form, $form_state);

    $entity = $this->entity;

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state)
  {
    $entity = $this->entity;

    $status = parent::save($form, $form_state);

    switch ($status) {
      case SAVED_NEW:

        //get data for notification
        $data['title'] = $entity->getTitle();
        $data['body'] = $entity->field_body->getString();
        if ($entity->field_link[0]) {
          $link = Url::fromUri($entity->field_link[0]->uri);
          $link = $link->setAbsolute();
          $data['link'] = $link->toString();
        }
        $data['type'] = $entity->bundle();
        $data['date'] = $entity->get('created')->value;


        foreach (ALLOWED_TOKEN_TYPES as $type) {

          if ($count = $this->getTotalNumber($type)) {
            $step = 500;
            $from = 0;
            $to = $step;
            do {
              $tokens = $this->getTokens($type, $from, $to);
              //send notifications
              $function = 'rg_push_notifications_send_' . $type;
              $function($tokens, $data);

              $from = $to;
              $to = $to + $step;
            } while ($to <= $count);
          }

        }

        $this->messenger()->addMessage(
          $this->t('Created the %label Push notification.', [
            '%label' => $entity->label(),
          ]));
        break;

      default:
        $this->messenger()->addMessage(
          $this->t('Saved the %label Push notification.', [
            '%label' => $entity->label(),
          ]));
    }
    $form_state->setRedirect('entity.push_notification.canonical', ['push_notification' => $entity->id()]);
  }


  /**
   * return the list of firebase tokens for specific type
   * @param $type
   * @return mixed
   */
  protected function getTokens($type, $offset, $limit)
  {
    $connection = Database::getConnection();
    $result = $connection->select('firebase_tokens', 'firebase')
      ->fields('firebase', ['token'])
      ->condition('status', ACTIVE_DEVICE_TOKEN)
      ->condition('type', $type)
      ->execute()
      ->fetchCol();

    return $result;
  }

  /**
   * @param $type
   * @return mixed
   */
  protected function getTotalNumber($type)
  {
    $connection = Database::getConnection();
    $result = $connection->select('firebase_tokens', 'firebase')
      ->fields('firebase', ['token'])
      ->condition('status', ACTIVE_DEVICE_TOKEN)
      ->condition('type', $type)
      ->countQuery()
      ->execute()
      ->fetch();

    return $result->expression;
  }

}
