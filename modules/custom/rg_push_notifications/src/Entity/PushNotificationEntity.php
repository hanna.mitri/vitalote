<?php

namespace Drupal\rg_push_notifications\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\user\UserInterface;

/**
 * Defines the Push notification entity.
 *
 * @ingroup rg_push_notifications
 *
 * @ContentEntityType(
 *   id = "push_notification",
 *   label = @Translation("Push notification"),
 *   bundle_label = @Translation("Push notification type"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\rg_push_notifications\PushNotificationEntityListBuilder",
 *     "views_data" = "Drupal\rg_push_notifications\Entity\PushNotificationEntityViewsData",
 *     "translation" = "Drupal\rg_push_notifications\PushNotificationEntityTranslationHandler",
 *
 *     "form" = {
 *       "default" = "Drupal\rg_push_notifications\Form\PushNotificationEntityForm",
 *       "add" = "Drupal\rg_push_notifications\Form\PushNotificationEntityForm",
 *       "edit" = "Drupal\rg_push_notifications\Form\PushNotificationEntityForm",
 *       "delete" = "Drupal\rg_push_notifications\Form\PushNotificationEntityDeleteForm",
 *     },
 *     "access" = "Drupal\rg_push_notifications\PushNotificationEntityAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\rg_push_notifications\PushNotificationEntityHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "push_notification",
 *   data_table = "push_notification_field_data",
 *   translatable = TRUE,
 *   admin_permission = "administer push notification entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "bundle" = "type",
 *     "label" = "title",
 *     "uuid" = "uuid",
 *     "langcode" = "langcode",
 *     "status" = "status",
 *   },
 *   links = {
 *     "canonical" = "/admin/content/push_notification/{push_notification}",
 *     "add-page" = "/admin/content/push_notification/add",
 *     "add-form" = "/admin/content/push_notification/add/{push_notification_type}",
 *     "edit-form" = "/admin/content/push_notification/{push_notification}/edit",
 *     "delete-form" = "/admin/content/push_notification/{push_notification}/delete",
 *     "collection" = "/admin/content/push_notification",
 *   },
 *   bundle_entity_type = "push_notification_type",
 *   field_ui_base_route = "entity.push_notification_type.edit_form"
 * )
 */
class PushNotificationEntity extends ContentEntityBase implements PushNotificationEntityInterface {

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   */
  public function getTitle() {
    return $this->get('title')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setTitle($title) {
    $this->set('title', $title);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isPublished() {
    return (bool) $this->getEntityKey('status');
  }

  /**
   * {@inheritdoc}
   */
  public function setPublished($published) {
    $this->set('status', $published ? TRUE : FALSE);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['title'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Title'))
      ->setDescription(t('The title of the Push notification entity.'))
      ->setSettings([
        'max_length' => 50,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['status'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Publishing status'))
      ->setDescription(t('A boolean indicating whether the Push notification is ready to send.'))
      ->setDefaultValue(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'weight' => 50,
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    return $fields;
  }

}
