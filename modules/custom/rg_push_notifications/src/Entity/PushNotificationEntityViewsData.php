<?php

namespace Drupal\rg_push_notifications\Entity;

use Drupal\views\EntityViewsData;

/**
 * Provides Views data for Push notification entities.
 */
class PushNotificationEntityViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    // Additional information for Views integration, such as table joins, can be
    // put here.

    return $data;
  }

}
