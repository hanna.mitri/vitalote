<?php

namespace Drupal\rg_push_notifications\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Defines the Push notification type entity.
 *
 * @ConfigEntityType(
 *   id = "push_notification_type",
 *   label = @Translation("Push notification types"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\rg_push_notifications\PushNotificationEntityTypeListBuilder",
 *     "form" = {
 *       "add" = "Drupal\rg_push_notifications\Form\PushNotificationEntityTypeForm",
 *       "edit" = "Drupal\rg_push_notifications\Form\PushNotificationEntityTypeForm",
 *       "delete" = "Drupal\rg_push_notifications\Form\PushNotificationEntityTypeDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\rg_push_notifications\PushNotificationEntityTypeHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "push_notification_type",
 *   admin_permission = "administer site configuration",
 *   bundle_of = "push_notification",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/push_notification_type/{push_notification_type}",
 *     "add-form" = "/admin/structure/push_notification_type/add",
 *     "edit-form" = "/admin/structure/push_notification_type/{push_notification_type}/edit",
 *     "delete-form" = "/admin/structure/push_notification_type/{push_notification_type}/delete",
 *     "collection" = "/admin/structure/push_notification_type"
 *   }
 * )
 */
class PushNotificationEntityType extends ConfigEntityBundleBase implements PushNotificationEntityTypeInterface {

  /**
   * The Push notification type ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Push notification type label.
   *
   * @var string
   */
  protected $label;

}
