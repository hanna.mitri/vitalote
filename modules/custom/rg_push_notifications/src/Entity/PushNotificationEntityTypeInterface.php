<?php

namespace Drupal\rg_push_notifications\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Push notification type entities.
 */
interface PushNotificationEntityTypeInterface extends ConfigEntityInterface {

  // Add get/set methods for your configuration properties here.
}
