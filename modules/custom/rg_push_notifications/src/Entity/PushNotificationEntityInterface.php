<?php

namespace Drupal\rg_push_notifications\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Push notification entities.
 *
 * @ingroup rg_push_notifications
 */
interface PushNotificationEntityInterface extends ContentEntityInterface, EntityChangedInterface {

  // Add get/set methods for your configuration properties here.

  /**
   * Gets the Push notification name.
   *
   * @return string
   *   Name of the Push notification.
   */
  public function getTitle();

  /**
   * Sets the Push notification name.
   *
   * @param string $name
   *   The Push notification name.
   *
   * @return \Drupal\rg_push_notifications\Entity\PushNotificationEntityInterface
   *   The called Push notification entity.
   */
  public function setTitle($name);

  /**
   * Gets the Push notification creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Push notification.
   */
  public function getCreatedTime();

  /**
   * Sets the Push notification creation timestamp.
   *
   * @param int $timestamp
   *   The Push notification creation timestamp.
   *
   * @return \Drupal\rg_push_notifications\Entity\PushNotificationEntityInterface
   *   The called Push notification entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the Push notification published status indicator.
   *
   * Unpublished Push notification are only visible to restricted users.
   *
   * @return bool
   *   TRUE if the Push notification is published.
   */
  public function isPublished();

  /**
   * Sets the published status of a Push notification.
   *
   * @param bool $published
   *   TRUE to set this Push notification to published, FALSE to set it to unpublished.
   *
   * @return \Drupal\rg_push_notifications\Entity\PushNotificationEntityInterface
   *   The called Push notification entity.
   */
  public function setPublished($published);

}
