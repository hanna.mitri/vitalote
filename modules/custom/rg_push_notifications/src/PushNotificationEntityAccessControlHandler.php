<?php

namespace Drupal\rg_push_notifications;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Push notification entity.
 *
 * @see \Drupal\rg_push_notifications\Entity\PushNotificationEntity.
 */
class PushNotificationEntityAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\rg_push_notifications\Entity\PushNotificationEntityInterface $entity */
    switch ($operation) {
      case 'view':
        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished push notification entities');
        }
        return AccessResult::allowedIfHasPermission($account, 'view published push notification entities');

      case 'update':
        return AccessResult::allowedIfHasPermission($account, 'edit push notification entities');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete push notification entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add push notification entities');
  }

}
