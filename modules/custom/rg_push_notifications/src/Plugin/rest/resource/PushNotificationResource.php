<?php

namespace Drupal\rg_push_notifications\Plugin\rest\resource;

use Drupal\Core\Database\Database;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\rest\ResourceResponse;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * Provides a resource to get walkthrough nodes.
 *
 * @RestResource(
 *   id = "push_notifications_resource",
 *   label = @Translation("Notification tokens"),
 *   uri_paths = {
 *     "canonical" = "/api/v1/notification-token/{device_id}",
 *     "https://www.drupal.org/link-relations/create" = "/api/v1/notification-token"
 *   }
 * )
 */
class PushNotificationResource extends ResourceBase
{
  /**
   * A current user instance.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * Constructs a new CreateArticleResource object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param array $serializer_formats
   *   The available serialization formats.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   A current user instance.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    array $serializer_formats,
    LoggerInterface $logger,
    AccountProxyInterface $current_user) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);

    $this->currentUser = $current_user;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->getParameter('serializer.formats'),
      $container->get('logger.factory')->get('dummy'),
      $container->get('current_user')
    );
  }

  /**
   * Responds to POST requests.
   */
   function post($request)
  {
    if (!$this->currentUser->hasPermission('manage firebase token')) {
      throw new AccessDeniedHttpException();
    }

    $error_response = [
      'status' => false,
      'message' => 'Something went wrong during token registration. Check your data.',
      'data' => [],
    ];

    $build = [
      '#cache' => [
        'max-age' => 0,
      ],
    ];

    if (!$request['device_id'] || !$request['token'] || !$request['type']
    || !in_array($request['type'], ALLOWED_TOKEN_TYPES)) {
      return new ResourceResponse($error_response, 400);
    }

    try {

      $connection = Database::getConnection();
      $connection->merge('firebase_tokens')
        ->key(['device_id' => $request['device_id']])
        ->insertFields([
          'device_id' => $request['device_id'],
          'token' => $request['token'],
          'type' => $request['type'],
          'status' => $request['status'],
        ])
        ->updateFields([
          'token' => $request['token'],
          'status' => $request['status'],
        ])
        ->execute();

      $result = $connection->select('firebase_tokens', 'ft')
        ->fields('ft', ['device_id', 'status', 'token', 'type'])
        ->condition('ft.device_id', $request['device_id'])
        ->execute();
      $result = $result->fetchAssoc();

      if (!$result) {
        $no_result_response = [
          'status' => false,
          'message' => 'Device id isn\'t registered.',
          'data' => [],
        ];
        return new ResourceResponse($no_result_response, 404);
      }

      $succes_response = [
        'status' => true,
        'message' => 'Successfully created',
        'data' => [$result],
      ];
      return (new ResourceResponse($succes_response, 201))->addCacheableDependency($build);
    } catch (\Exception $e) {
      $error_response['message'] = $e->getMessage();
      return (new ResourceResponse($error_response, 400))->addCacheableDependency($build);
    }
  }
  /**
   * Responds to PATCH requests.
   */
   function patch($device_id, $request)
  {

    $error_response = [
      'status' => false,
      'message' => 'Something went wrong during updating token. Check your data.',
      'data' => [],
    ];

    $build = [
      '#cache' => [
        'max-age' => 0,
      ],
    ];

    if (!$this->currentUser->hasPermission('manage firebase token')) {
      throw new AccessDeniedHttpException();
    }

    try {
      $connection = Database::getConnection();
      $connection->update('firebase_tokens')
        ->fields(['status' => $request['status'], 'token' => $request['token']])
        ->condition('device_id', $device_id)
        ->execute();

      $result = $connection->select('firebase_tokens', 'ft')
        ->fields('ft', ['device_id', 'status', 'token', 'type'])
        ->condition('ft.device_id', $device_id)
        ->execute();
      $result = $result->fetchAssoc();

      if (!$result) {
        $no_result_response = [
          'status' => false,
          'message' => 'Device id isn\'t registered.',
          'data' => [],
        ];
        return new ResourceResponse($no_result_response, 404);
      }

      $succes_response = [
        'status' => true,
        'message' => 'Successfully updated!',
        'data' => [$result],
      ];


      return (new ResourceResponse($succes_response, 200))->addCacheableDependency($build);
    } catch (\Exception $e) {
      $error_response['message'] = $e->getMessage();
      return (new ResourceResponse($error_response, 400))->addCacheableDependency($build);
    }
  }

  /**
   * Responds to GET requests.
   */
   function get($device_id, $request)
  {

    $error_response = [
      'status' => false,
      'data' => [],
    ];

    $build = [
      '#cache' => [
        'max-age' => 0,
      ],
    ];

    if (!$this->currentUser->hasPermission('manage firebase token')) {
      throw new AccessDeniedHttpException();
    }

    try {
      $connection = Database::getConnection();
      $result = $connection->select('firebase_tokens', 'ft')
        ->fields('ft', ['device_id', 'status', 'token', 'type'])
        ->condition('ft.device_id', $device_id)
        ->execute();
      $result = $result->fetchAssoc();

      if (!$result) {
        $no_result_response = [
          'status' => false,
          'message' => 'Device id isn\'t registered.',
          'data' => [],
        ];
        return new ResourceResponse($no_result_response, 404);
      }

      $succes_response = [
        'status' => true,
        'message' => 'Success',
        'data' => $result,
      ];
      return (new ResourceResponse($succes_response, 200))->addCacheableDependency($build);
    } catch (\Exception $e) {
      $error_response['message'] = $e->getMessage();
      return (new ResourceResponse($error_response, 400))->addCacheableDependency($build);
    }
  }
}