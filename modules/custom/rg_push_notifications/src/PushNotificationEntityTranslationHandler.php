<?php

namespace Drupal\rg_push_notifications;

use Drupal\content_translation\ContentTranslationHandler;

/**
 * Defines the translation handler for push_notification.
 */
class PushNotificationEntityTranslationHandler extends ContentTranslationHandler {

  // Override here the needed methods from ContentTranslationHandler.

}
