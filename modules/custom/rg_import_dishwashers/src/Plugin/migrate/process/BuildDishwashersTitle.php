<?php

namespace Drupal\rg_import_dishwashers\Plugin\migrate\process;

use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;

/**
 * Returns value of input array or JSON string using JSONPath.
 *
 * Example of usage:
 * @code
 * process:
 *   title:
 *     -
 *       plugin: build_dishwashers_title
 *       source: source_field
 * @endcode
 *
 * @MigrateProcessPlugin(
 *   id = "build_dishwashers_title"
 * )
 */
class BuildDishwashersTitle extends ProcessPluginBase
{
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property)
  {
    $title = [];

    $attributes = $row->getSourceProperty('attr');
    foreach ($attributes as $attr) {
      if ($attr['key'] == 'title') {
        $title[2] = $attr['value'];
        continue;
      }
      if ($attr['key'] == 'frebuiin') {
        $title[3] = $attr['value'];
        continue;
      }
      if ($attr['key'] == 'ProductFamily') {
        $title[4] = $attr['value'];
        continue;
      }
      if ($attr['key'] == 'size') {
        $title[7] = $attr['value'] . 'cm ';
        continue;
      }
      if ($attr['key'] == 'Placesettings') {
        $title[8] = 'with ' . $attr['value'] . ' place settings';
        continue;
      }
    }

    $title[1] = '<div class="first-line">';
    $title[5] = '</div>';
    $title[6] = '<div class="second-line">';
    $title[9] = '</div>';

    ksort($title);
    $title = implode(' ', $title);

    return $title;
  }
}