<?php

namespace Drupal\rg_brochure;

use Drupal;
use Drupal\contact\Entity\Message;
use Drupal\node\Entity\Node;

class BrochureHelper
{
  public static function message()
  {
    $message = [
      '#theme' => 'status_messages',
      '#message_list' => Drupal::messenger()->all(),
      '#status_headings' => [
        'status' => t('Status message'),
        'error' => t('Error message'),
        'warning' => t('Warning message'),
      ],
    ];

    return $message;
  }

  public static function subscription($form_state)
  {
    $subscription_manager = Drupal::service('simplenews.subscription_manager');
    $email = trim($form_state->getValue('mail'));
    $langcode = $form_state->getValue('language');
    $newsletter_id = 'order_brochure';
    $subscription_manager->subscribe($email, $newsletter_id, FALSE, 'mass subscribe', $langcode);
  }

  public static function exportCSV()
  {
    $data = Message::loadMultiple();
    $datum = end($data);

    $result = [];
    foreach ($datum as $key => $item) {
      $result[$key] = $item->value;
    }

    $ids = $datum->get("field_brochure")->getValue();
    $id = [];
    foreach ($ids as $value) {
      $id[] = $value['target_id'];
    }

    $result['created'] = gmdate('d/m/Y', $result['created']);

    self::exportCustomerCSV($result);
    self::exportBrochureCSV($result, $id);
  }

  private static function exportCustomerCSV($data)
  {
    if(count($data) > 0){
      $fields = [
        'field_message_id',
        'mail',
        'created',
        'field_address_1',
        'field_address_2',
        'field_address_3',
        'field_country',
        'field_county',
        'field_first_name',
        'field_last_name',
        'field_mobile_number',
        'field_postcode',
        'field_salutation',
        'field_telephone_number',
        'field_town',
        'field_signup_checkbox',
        'field_signup_rg_sinks_taps',
        'field_signup_rg_cookware',
        'field_signup_rg_extended_warrant',
        'field_signup_aga_appliances',
        'field_signup_aga_cookshop',
        'field_signup_aga_stoves',
        'field_signup_rayburn',
      ];
      $fields = array_fill_keys($fields, '');
      $data = array_intersect_key($data, $fields);
      $data += [
        'type' => 'Brochure',
        'show_code' => '',
        'consent_form_reference' => '',
      ];
      $delimiter = "|";
      $filename = "customer_" . $data["field_message_id"] . "_" . date('Y-m-d') . ".csv";

      //create a file pointer
      $dir = DRUPAL_ROOT;
      $filename = $dir . '/../Input/Order_brochure/' . $filename;
      $dirname = dirname($filename);
      if (!is_dir($dirname)) {
        mkdir($dirname, 0755, true);
      }
      $f = fopen($filename, 'w');

      //set column headers
      $titles = array(
        'Email',
        'Created at',
        'Address one',
        'Address two',
        'Address three',
        'Country',
        'County',
        'First name',
        'Last name',
        'Message ID',
        'Mobile number',
        'Postcode',
        'Title',
        'SignUpAGAAppliances',
        'SignUpAGACookshop',
        'SignUpAGAStoves',
        'SignUpRangemasterAppliances',
        'SignUpRayburn',
        'SignUpRangemasterCookware',
        'SignUpRangemasterExtendedWarranty',
        'SignUpRangemasterSinksTaps',
        'Telephone number',
        'Town',
        'Type',
        'ShowCode',
        'ConsentFormReference',
      );
      fputcsv($f, $titles, $delimiter);

      fputcsv($f, $data, $delimiter);
      //move back to beginning of file
      fseek($f, 0);
      //output all remaining data on a file pointer
      fpassthru($f);
    }
  }

  private static function exportBrochureCSV($data, $ids)
  {
    if(count($data) > 0){
      $fields = [
        'field_message_id' => '',
        'created' => '',
      ];
      $data = array_intersect_key($data, $fields);
      $vid = 'brands';
      $query = Drupal::entityQuery('taxonomy_term')
        ->condition('vid', $vid)
        ->condition('status', 1)
        ->condition('field_main_brand', 1);

      $term_id = $query->execute();
      $term = Drupal::entityTypeManager()->getStorage('taxonomy_term')->load(array_shift($term_id));
      $term = !empty($term) ? $term->getName() : '';
      $data += [
        'brochure_code' => '',
        'brochure_description' => '',
        'quantity' => 1,
        'brand' => $term
      ];
      $delimiter = "|";
      $filename = "brochure_" . $data["field_message_id"] . "_" . date('Y-m-d') . ".csv";

      //create a file pointer
      $dir = DRUPAL_ROOT;
      $filename = $dir . '/../Input/Order_brochure/' . $filename;
      $dirname = dirname($filename);
      if (!is_dir($dirname)) {
        mkdir($dirname, 0755, true);
      }
      $f = fopen($filename, 'w');

      //set column headers
      $fields = array(
        'Created at',
        'Message ID',
        'Brochure code',
        'Brochure Description',
        'Quantity',
        'Brand',
      );
      fputcsv($f, $fields, $delimiter);

      $brochures = Node::loadMultiple($ids);
      foreach ($brochures as $key => $brochure){
        $data["brochure_code"] = $brochure->get('field_brochure_code')->value;
        $data["brochure_description"] = trim(strip_tags($brochure->getTitle()));
        fputcsv($f, $data, $delimiter);
      }

      //move back to beginning of file
      fseek($f, 0);
      //output all remaining data on a file pointer
      fpassthru($f);
    }
  }
}
