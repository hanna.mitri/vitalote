<?php

/**
 * @file
 * Contains rg_seo.module.
 */

use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\rg_product\ProductHelper;
use Drupal\Core\Form\FormStateInterface;

/**
 * for creating custom tokens
 *
 * Implements hook_token_info().
 */
function rg_seo_token_info()
{
  $info = [];
  $info['types']['product_variation_group'] = [
    'name' => t('Product Variation Group'),
    'description' => t('Product variation Group')
  ];
  $info['types']['product_group'] = [
    'name' => t('Product Group'),
    'description' => t('Product Group')
  ];

  $info['tokens']['product_variation_group']['variation_field:?_']['name'] = 'Variation with parameters';
  $info['tokens']['product_variation_group']['variation_field:?_']['description'] = 'To add parameter for token: you have to add name of the field (you want to add) after underscore "?_" (e.g.: [product_variation_group:variation_field:?_title] -- to add variation title)';

  $info['tokens']['product_group']['product_title']['name'] = 'Product title';
  $info['tokens']['product_group']['product_title']['description'] = 'A token to extract title from Products';
  $info['tokens']['product_group']['product_description']['name'] = 'Product description';
  $info['tokens']['product_group']['product_description']['description'] = 'A token to extract description from Products';
  $info['tokens']['product_group']['product_last_url']['name'] = 'Product last URL item';
  $info['tokens']['product_group']['product_last_url']['description'] = 'A token to extract last item from Products catalog URL (e.g.: on the page "/products/built-in" returns "built-in")';


  return $info;
}

/**
 * for creating custom tokens
 *
 * Implements hook_tokens().
 * @param $type
 * @param $tokens
 * @param array $data
 * @return array
 * @throws InvalidPluginDefinitionException
 * @throws PluginNotFoundException
 */
function rg_seo_tokens($type, $tokens, array $data, array $options, \Drupal\Core\Render\BubbleableMetadata $bubbleable_metadata)
{
  $replacements = [];

  switch ($type) {
    case 'product_variation_group':

      $product_variation = ProductHelper::getDefaultVariation();
      if(isset($product_variation) && isset($data['commerce_product'])){
        foreach ($tokens as $name => $original) {
          // Find the desired token by name.
          if (strpos($name, '?_') !== false) {
            $title = explode('?_', $name);
            $title = end($title);
            $replacements[$original] =  $product_variation->get($title)->value;
            break;
          }
        }
      }
      break;
    case 'product_group':

      if(isset($data["view"])) {
        $current_uri = Drupal::request()->getRequestUri();
        $current_uri = strtok($current_uri, '?');
        $uri_arr = explode('/', $current_uri);
        $last_url = end($uri_arr);
        $title = implode(' ', explode('-', $last_url));
        $title = ucfirst($title);

        foreach ($tokens as $name => $original) {
          switch ($name) {
            case 'product_title':
              $replacements[$original] = $title;
              break;
            case 'product_last_url':
              $replacements[$original] = $last_url;
          }
        }
      }
      break;

  }
  return $replacements;
}

/**
 * Implements hook_simple_sitemap_links_alter().
 * @param array $links
 * @throws InvalidPluginDefinitionException
 * @throws PluginNotFoundException
 * @throws Drupal\Core\Entity\EntityMalformedException
 * @throws Exception
 */
function rg_seo_simple_sitemap_links_alter(array &$links)
{
  foreach ($links as $key => $link) {
    if(stripos($link['url'], '%2A') !== false){
      unset($links[$key]);
    }
  }

  $variations = Drupal::entityTypeManager()
    ->getStorage('commerce_product_variation')
    ->loadMultiple();

  /** @var \Drupal\commerce_product\Entity\ProductVariation $variation */
  foreach ($variations as $variation) {
    if ($variation->get('status')->value === '0') {
      continue;
    }
    $changed = $variation->get('changed')->value;
    $datetime = new DateTime("@$changed");
    $changed = $datetime->format('c');
    $url = $variation->toUrl('canonical', ['absolute' => TRUE]);
    $url = $url->toString();
    $sitemap_variation = [
      'lastmod' => $changed,
      'priority' => '0.5',
      'changefreq' => 'always',
      'url' => $url,
    ];
    array_push($links, $sitemap_variation);
  }
}

/**
 * Declaring the custom voice search icon image alt text.
 */

function rg_seo_form_alter(&$form, FormStateInterface $form_state, $form_id) {
  $config = \Drupal::config('custom_voice_search.googlevoicesearch');
  $form['#attached']['library'][] = 'custom_voice_search/custom_voice_search';
  if ($config->get('form_ids') !== NULL) {
    foreach ($config->get('form_ids') as $value) {
      // Change form id here.
      if (strstr($form_id, $value['id'])) {
        $form[$value['input_machine_name']]['#field_suffix'] = '<div class="voice-search-block"><img alt="Voice search" src="/' . drupal_get_path('module', 'custom_voice_search') . '/images/mic.png" /></div>';
      }
      $form['#attached']['drupalSettings']['custom_voice_search']['custom_voice_search']['ids'][] = [
        'id' => $value['id'],
        'input_id' => $value['input_id'],
      ];
    }
  }
}
