<?php

namespace Drupal\rg_user\Controller;

use Drupal\Core\Url;
use Drupal\user\Controller\UserAuthenticationController;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

/**
 * Class UserController.
 */
class UserController extends UserAuthenticationController {

  /**
   * Reset password.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   A response which contains the ID and CSRF token.
   */
  public function resendInvitation($user) {

    $account = null;
    // Load by name if provided.
    if (isset($user)) {
      /** @var \Drupal\Core\Session\AccountInterface $account */
      $account = $this->userStorage->load($user);
    }

    if ($account && $account->id()) {
      if ($this->userIsBlocked($account->getAccountName())) {
        throw new BadRequestHttpException('The user has not been activated or is blocked.');
      }

      // Send the password reset email.
      $mail = _user_mail_notify('password_reset', $account, $account->getPreferredLangcode());
      if (empty($mail)) {
        throw new BadRequestHttpException('Unable to send email. Contact the site administrator if the problem persists.');
      }
      else {
        $this->logger->notice('Password reset instructions mailed to %name at %email.', ['%name' => $account->getAccountName(), '%email' => $account->getEmail()]);
        \Drupal::messenger()->addMessage(t('An invitation was sent.'));

        $destination = Url::fromUserInput(\Drupal::destination()->get());
        if ($destination->isRouted()) {
          // Valid internal path.
          return $this->redirect($destination->getRouteName());
        }
      }
    }

    // Error if no users found with provided name or mail.
    throw new BadRequestHttpException('Unrecognized username or email address.');
  }

}
