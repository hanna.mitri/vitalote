Drupal.behaviors.faqsNodeEdit = {
  attach: function (context, settings) {
    var paragraphs = jQuery('.paragraphs-subform');

    paragraphs.each(function (key) {

      jQuery(".form-item-field-question-"+key+"-subform-all")
        .prependTo('[data-drupal-selector="edit-field-question-'+key+'-subform-field-category-wrapper"] .fieldset-wrapper');


      var allBox = jQuery('[data-drupal-selector="edit-field-question-'+key+'-subform-all"]');
      var checkboxes = jQuery('[data-drupal-selector="edit-field-question-'+key+'-subform-field-category-wrapper"] :checkbox:not(#edit-field-question-'+key+'-subform-field-category-0)');

      checkboxes.change(function () {
        if (allBox.get(0).checked && !this.checked) {
          allBox.get(0).checked = false;
        }
      });

      allBox.change(function () {
        if (this.checked) {
          checkboxes.each(function () {
            this.checked = true;
          });
        } else {
          checkboxes.each(function () {
            this.checked = false;
          });
        }
      });



    });

  }
};