<?php

namespace Drupal\rangemaster\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Defines a route controller for entity autocomplete form elements.
 */
class AutocompleteController extends ControllerBase {

  /**
   * Handler for autocomplete request.
   */
  public function handleAutocomplete(Request $request, $field_name, $count) {
    $results = [];

    // Get the typed string from the URL, if it exists.
    if ($input = $request->query->get('q')) {

      $query = \Drupal::database()->select('paragraph_revision__field_mt_vgs_slide_title', 'p');
      $query->join('node__field_slide', 'n', 'n.field_slide_target_id = p.entity_id');
      $query_result  = $query
        ->fields('p', array('field_mt_vgs_slide_title_value', 'entity_id'))
        ->fields('n', array('entity_id'))
        ->condition('p.field_mt_vgs_slide_title_value', $input . '%', 'LIKE')
        ->execute()
        ->fetchAll();
    }
    foreach ($query_result as $row) {
        $results[] = [
          'value' => $row->field_mt_vgs_slide_title_value . ' (' . $row->entity_id . ' in ' . $row->n_entity_id . ')',
          'label' => $row->field_mt_vgs_slide_title_value,
        ];
    }
    return new JsonResponse($results);
  }

}