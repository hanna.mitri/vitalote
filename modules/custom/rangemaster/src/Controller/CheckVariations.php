<?php


namespace Drupal\rangemaster\Controller;


use Drupal\commerce_price\Price;
use Drupal\commerce_product\Entity\Product;
use Drupal\Core\Controller\ControllerBase;
use Drupal\commerce_product\Entity\ProductVariation;
use Drupal\Core\Url;
use Drupal\Core\Link;

/**
 * Provides route responses for the Example module.
 */
class CheckVariations extends ControllerBase {

  /**
   * Returns a simple page.
   *
   * @return array
   *   A simple renderable array.
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function Page(){
    $rows = [];
    $message = '';
    $query = \Drupal::database()->select('commerce_product__variations', 'cpv');
    $query->addField('cpv', 'variations_target_id');
    $query->addField('cpv', 'entity_id');
    $results = $query->execute();
    $headers = [
      'product_id' => t('Product id'),
      'variation_id' => t('Variation id'),
      'action' => t('Action'),
    ];
    foreach ($results as $result) {
      $variation = ProductVariation::load($result->variations_target_id);
      $product_id = $result->entity_id ? $result->entity_id : '';
      if(!is_object($variation) && isset($product_id)) {
        $product = Product::load($product_id);
        if (!$product) {
          continue;
        }
        $existing_variations = $product->getVariations();
        if (empty($existing_variations)) {
          $default_variation = ProductVariation::create([
            'type' => 'default',
            'sku' => 'qqq',
            'title' => 'my title',
            'price' => new Price('111111', 'GBP')
          ]);
          $default_variation->save();
          $product->setVariations([$default_variation]);
          $product->save();
          $product->delete();
          $product_name = $product->getTitle();
          $message .= "<li>Product $product_name with id=$product_id was deleted</li>";
        } else {
          $url = Url::fromUri("internal:/product/{$product_id}/variations");
          $link = Link::fromTextAndUrl(t('Click save button on this page'), $url);
          $rows[] = [
            'product_id' => $product_id,
            'variation_id' => $result->variations_target_id,
            'action' => $link,
          ];
        }
      }
    }
    if(count($rows) > 0) {
      $this->removeProductMessage($message);
      return array(
        '#theme' => 'table',
        '#header' => $headers,
        '#rows' => $rows,
      );
    } else {
      if (empty($message)) {
        return array(
          '#markup' => t("We haven't bugs with product variations on the site"),
        );
      }
      $this->removeProductMessage($message);
      return [];
    }
  }

  protected function removeProductMessage($message) {
    if (!empty($message)) {
      \Drupal::logger('custom')->info('<ul>' . $message . '</ul>');
      $status_message = $this->t(
        'Some products were deleted. You can see more information on <a href="@log-page">Recent log page</a>', [
        '@log-page' => '/admin/reports/dblog',
      ]);
      \Drupal::messenger()->addStatus($status_message);
    }
  }

}
