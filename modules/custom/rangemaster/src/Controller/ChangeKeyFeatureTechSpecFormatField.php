<?php

namespace Drupal\rangemaster\Controller;

use Drupal;
use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityStorageException;

/**
 * Change Format value in view display mode for Key Feature and Technical Spec
 */
class ChangeKeyFeatureTechSpecFormatField extends ControllerBase
{
  /** Change Key Feature and Tech Spec "view display" format value (from 'number_decimal' to 'number_unformatted')
   *
   * @return array
   * @throws InvalidPluginDefinitionException
   * @throws PluginNotFoundException
   * @throws EntityStorageException
   */
  public function Page()
  {
    $bundles = Drupal::entityTypeManager()->getStorage('commerce_product_variation_type')->loadMultiple();

    $entityType = 'commerce_product_variation';
    $viewModes = ['key_features', 'technical_specification'];

    foreach ($bundles as $bundle) {
      foreach ($viewModes as $viewMode) {
        $display = Drupal::entityTypeManager()
          ->getStorage('entity_view_display')
          ->load($entityType . '.' . $bundle->id() . '.' . $viewMode);
        if (!$display) {
          continue;
        }
        $displayFields = $display->getComponents();

        foreach ($displayFields as $machineName => $field) {
          if ($field['type'] === 'number_decimal') {
            $fieldValues = [
              'type' => 'number_unformatted',
              'weight' => $field['weight'],
              'region' => $field['region'],
              'label' => $field['label'],
              'settings' => $field['settings'],
              'third_party_settings' => $field['third_party_settings'],
            ];
            $display->setComponent($machineName, $fieldValues);
          }
        }
        $display->save();
      }
    }
    return [];
  }
}
