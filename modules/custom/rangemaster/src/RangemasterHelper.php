<?php

namespace Drupal\rangemaster;


use Drupal\views\Views;

class RangemasterHelper
{
  /**
   * @param $viewId
   * @param $displayId
   * @param array $arguments
   * @return bool
   *
   * Return rendered view
   */
  public static function getView($viewId, $displayId, array $arguments)
  {
    $result = false;
    $view = Views::getView($viewId);

    if (is_object($view)) {
      $view->setDisplay($displayId);
      $view->setArguments($arguments);
      $view->execute();

      // Render the view
      $renderable = $view->render();
      $result = \Drupal::service('renderer')->render($renderable);
    }

    return $result;
  }

}