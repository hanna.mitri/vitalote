<?php

namespace Drupal\rangemaster\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\user\Entity\User;


class AddBloggerForm extends FormBase
{

  public function getFormId() {
    return 'add_blogger_form';
  }

  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['name'] = array(
      '#type' => 'textfield',
      '#title' => t('Blogger name'),
      '#required' => TRUE,
    );
    $form['picture'] = array(
        '#type' => 'managed_file',
        '#title' => t('Profile Picture'),
        '#upload_validators' => array(
          'file_validate_extensions' => array('gif png jpg jpeg'),
          'file_validate_size' => array(25600000),
        ),
       '#theme' => 'image_widget',
      '#preview_image_style' => 'medium',
       '#upload_location' => 'public://profile-pictures',
       '#required' => TRUE,
    );

    $form['description'] = array(
      '#type' => 'textarea',
      '#title' => t('Description'),
      '#required' => TRUE,
    );
    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Submit'),
    );
    return $form;
  }

  public function validateForm(array &$form, FormStateInterface $form_state) {
    // Validate video URL.
    $name = $form_state->getValue('name');
    if (user_load_by_name($name)) {
      $form_state->setErrorByName('name', $this->t("The user '%name' already exists", array('%name' => $form_state->getValue('name'))));
    }
  }

  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Display result.
    $user = User::create();

    //Mandatory settings
    $user->setPassword('<password>');
    $user->enforceIsNew();
    $user->setEmail('<email>');

    //This username must be unique and accept only a-Z,0-9, - _ @ .
    $user->setUsername($form_state->getValue('name'));
    $user->addRole('blogger');

    $fid = $form_state->getValue('picture');
    $user->set('user_picture', $fid[0]);
    $user->set('field_specialization', $form_state->getValue('description'));


    //Optional settings
    $user->activate();
    //Save user
    $user->save();
    $id = $user->id();
    $dest_url = "/user/$id";
    $url = Url::fromUri('internal:' . $dest_url);
    $form_state->setRedirectUrl($url);
    drupal_set_message("User with uid " . $user->id() . " saved!\n");

  }

}