<?php

namespace Drupal\rangemaster\Plugin\Block;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Session\AccountInterface;

/**
 * Provides a block with a simple text.
 *
 * @Block(
 *   id = "related_videos",
 *   admin_label = @Translation("Related videos"),
 * )
 */
class VideoRelatedLinks extends BlockBase
{
  /**
   * {@inheritdoc}
   */
  public function build()
  {
    $emptyText = $this->t('No related videos!');

    $build = [
      '#markup' => '<div class="related-links-wrapper">
        <div class="empty-text">' . $emptyText . '</div>
        <div class="content"></div>
      </div>',
    ];

    $build['#cache']['max-age'] = 0;

    return $build;
  }

  /**
   * {@inheritdoc}
   */
  protected function blockAccess(AccountInterface $account)
  {
    return AccessResult::allowed();
  }

}