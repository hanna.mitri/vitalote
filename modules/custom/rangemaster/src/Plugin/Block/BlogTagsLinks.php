<?php

namespace Drupal\rangemaster\Plugin\Block;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\user\Entity\User;

/**
 * Provides a block with a simple text.
 *
 * @Block(
 *   id = "blog_tags_links_block",
 *   admin_label = @Translation("Blog tags links block"),
 * )
 */
class BlogTagsLinks extends BlockBase
{
  /**
   * {@inheritdoc}
   */
  public function build()
  {
    $build = [];

    $path = \Drupal::request()->getpathInfo();
    $path = explode('/', $path);

    $users = User::loadMultiple();

    foreach ($users as $user) {
      $user_name = $user->get('name')->value;
      $user_name = str_replace(' ', '-', strtolower($user_name));
      if (isset($path[3]) && $path[3] == $user_name) {
        $url = $path[3];
        break;
      } else {
        $url = 'all';
      }
    }

    $term = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree('tags');
    foreach ($term as $key => $value) {
      $term_name = str_replace(' ', '-', strtolower($value->name));

      $build['content'][$key] = [
        '#type' => 'markup',
        '#markup' => '<div class="tag-links"><a href="/blog/filter/' . $url . '/' . $term_name . '">' . $value->name . '</a></div>'
      ];
    }

    $build['#cache']['max-age'] = 0;

    return $build;
  }

  /**
   * {@inheritdoc}
   */
  protected function blockAccess(AccountInterface $account)
  {
    return AccessResult::allowedIfHasPermission($account, 'access content');
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state)
  {
    $config = $this->getConfiguration();

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state)
  {
    $this->configuration['blog_tags_links_block_settings'] = $form_state->getValue('blog_tags_links_block_settings');
  }
}