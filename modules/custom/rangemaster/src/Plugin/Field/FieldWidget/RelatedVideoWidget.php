<?php
/**
 * Created by PhpStorm.
 * User: ginger
 * Date: 18.01.19
 * Time: 9:56
 */

namespace Drupal\rangemaster\Plugin\Field\FieldWidget;


use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\link\Plugin\Field\FieldWidget\LinkWidget;

/**
 * Plugin implementation of the 'link' widget.
 *
 * @FieldWidget(
 *   id = "related_video",
 *   label = @Translation("Related Video"),
 *   field_types = {
 *     "string"
 *   }
 * )
 */
class RelatedVideoWidget extends WidgetBase
{
  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state)
  {
    $value = isset($items[$delta]->value) ? $items[$delta]->value : '';
    $element += [
      '#type' => 'textfield',
      '#default_value' => $value,
      '#autocomplete_route_name' => 'rangemaster.autocomplete',
      '#autocomplete_route_parameters' => [
        'field_name' => 'field_link',
        'count' => 10
      ],
      '#element_validate' => [
        [static::class, 'validate'],
      ],
    ];
    return ['value' => $element];
  }

  /**
   * Validate the color text field.
   */
  public static function validate($element, FormStateInterface $form_state)
  {
    $value = $element['#value'];
    if (strlen($value) == 0) {
      $form_state->setValueForElement($element, '');
      return;
    }
  }
}