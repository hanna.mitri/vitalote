<?php
/**
 * Created by PhpStorm.
 * User: ginger
 * Date: 14.03.19
 * Time: 16:20
 */

namespace Drupal\rg_mobile\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Class MobileHomeController
 * @package Drupal\rg_mobile\Controller
 */
class MobileHomeController extends ControllerBase {

  /**
   * Returns a simple page.
   *
   * @return array
   *   A simple renderable array.
   */
  public function page() {
    $element = array(
      '#markup' => '',
    );
    return $element;
  }

}