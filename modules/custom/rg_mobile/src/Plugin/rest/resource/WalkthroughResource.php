<?php

namespace Drupal\rg_mobile\Plugin\rest\resource;

use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;

/**
 * Provides a resource to get walkthrough nodes.
 *
 * @RestResource(
 *   id = "walkthrough_resource",
 *   label = @Translation("Walkthrough on load resource"),
 *   uri_paths = {
 *     "canonical" = "/api/v1/walkthrough"
 *   }
 * )
 */
class WalkthroughResource extends ResourceBase
{

  /**
   * Responds to GET requests.
   */
  public function get()
  {
    $data = [];
    // Find categories.
    $nids = \Drupal::entityQuery('node')
      ->condition('status', 1)
      ->condition('type', 'walkthrough')
      ->accessCheck(false)
      ->execute();
    $nodes = \Drupal\node\Entity\Node::loadMultiple($nids);
    foreach ($nodes as $node) {
      //image
      if ($uri = $node->get('field_background_image')->entity) {
        $uri = $uri->uri->value;
        $image = file_create_url($uri);
      } else {
        $image = null;
      }
      $data[] = [
        'id' => $node->id(),
        'name' => $node->getTitle(),
        'body' => $node->field_body_plain->value,
        'image' => $image,
      ];
    }
    $build = [
      '#cache' => [
        'max-age' => 0,
      ],
    ];
    $response = [
      'status' => true,
      'message' => t('Success'),
      'type' => 'Walkthrough',
      'data' => $data,
    ];
    return (new ResourceResponse($response, 200))->addCacheableDependency($build);
  }

}
