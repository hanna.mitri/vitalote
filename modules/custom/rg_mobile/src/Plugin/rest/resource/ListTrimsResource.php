<?php

namespace Drupal\rg_mobile\Plugin\rest\resource;

use Drupal\commerce_product\Entity\ProductAttributeValue;
use Drupal\commerce_product\Entity\ProductVariation;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;

/**
 * Provides a resource to get articles.
 *
 * @RestResource(
 *   id = "trims_resource",
 *   label = @Translation("List trims resource"),
 *   uri_paths = {
 *     "canonical" = "/api/v1/get-trims/{category_id}/{collection_id}"
 *   }
 * )
 */
class ListTrimsResource extends ResourceBase {

  /**
   * Responds to GET requests.
   */
  public function get($category_id = null, $collection_id = null) {
    $data = [];
    $status = 200;
    $category_id =  (int) $category_id;
    $collection_id =  (int) $collection_id;


    if (is_int($category_id) && is_int($collection_id)) {
      $query = \Drupal::entityQuery('commerce_product_variation')
        ->condition('field_category', $category_id)
        ->condition('field_range_cooker_collection', $collection_id)
        ->condition('status', 1)
        ->accessCheck(false);
      if ($query_trim = \Drupal::request()->query->get('trim')) {
        $query->condition('attribute_trim', $query_trim, '=');
      }
      $vids = $query->execute();
      $variations = ProductVariation::loadMultiple($vids);

      foreach ($variations as $variation) {
        //prepare data
        $trim = $variation->getAttributeValue('attribute_trim');

        $data[$trim->id()] =  [
            'id' => $trim->id(),
            'name' => $trim->name->value,
            'hex_code' => $trim->field_hex_color_code->value,
        ];
      }
      $data = array_values($data);

      $response = [
        'status' => true,
        'type' => 'Attribute trim',
        'message' => t('Success'),
        'data' => $data,
      ];
    }
    if (empty($data)) {
      $response = [
        'status' => false,
        'type' => 'Attribute trim',
        'message' => t('Nothing here'),
        'data' => $data,
      ];
      $status = 404;
    }


    $build = [
      '#cache' => [
        'max-age' => 0,
      ],
    ];

    return (new ResourceResponse($response, $status))->addCacheableDependency($build);
  }

}
