<?php

namespace Drupal\rg_mobile\Plugin\rest\resource;

use Drupal\Core\Url;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Drupal\block\Entity\Block;

/**
 * Provides a resource to get explore nodes.
 *
 * @RestResource(
 *   id = "explore_resource",
 *   label = @Translation("Explore on load resource"),
 *   uri_paths = {
 *     "canonical" = "/api/v1/explore"
 *   }
 * )
 */
class ExploreResource extends ResourceBase
{

  /**
   * Responds to GET requests.
   */
  public function get()
  {
    $data = [];
    $block = Block::load('mobilenavigation');
    $uuid = $block->getPluginId();
    $uuid = str_replace('block_content:', '', $uuid);
    $block_content = \Drupal::service('entity.repository')->loadEntityByUuid('block_content', $uuid);
    if ($block_content) {
      $items = $block_content->field_image_link->referencedEntities();
      foreach ($items as $item) {
        //image
        if ($uri = $item->get('field_mt_if_image')->entity) {
          $uri = $uri->uri->value;
          $image = file_create_url($uri);
        } else {
          $image = null;
        }

        //title
        if ($title = $item->get('field_mt_fltt_body')) {
          $title = strip_tags($title->value);
        } else {
          $title = null;
        }
        //link
        if ($link = $item->field_mt_if_link) {
          $link = Url::fromUri($link->uri);
          $link->setAbsolute();
          $path = $link->toString(true);
          $url_string = $path->getGeneratedUrl();
        } else {
          $path = null;
        }
        $data[] = [
          'title' => $title,
          'url' => $url_string,
          'image' => $image,
        ];
      }
    }
    $response = [
      'status' => true,
      'message' => t('Success'),
      'type' => 'Explore',
      'data' => $data,
    ];
    $response = new ResourceResponse($response, 200);
    $response->addCacheableDependency($path);
    return $response;
  }

}