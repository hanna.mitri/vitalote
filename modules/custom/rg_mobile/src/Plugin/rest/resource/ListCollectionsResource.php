<?php

namespace Drupal\rg_mobile\Plugin\rest\resource;

use Drupal\commerce_product\Entity\ProductVariation;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;

/**
 * Provides a resource to get articles.
 *
 * @RestResource(
 *   id = "collections_resource",
 *   label = @Translation("List collections resource"),
 *   uri_paths = {
 *     "canonical" = "/api/v1/category/{id}"
 *   }
 * )
 */
class ListCollectionsResource extends ResourceBase {

  /**
   * Responds to GET requests.
   */
  public function get($id = null) {
    $data = [];
    $status = 200;
    $id =  (int) $id;

    if (is_int($id)) {
      $query = \Drupal::entityQuery('commerce_product_variation')
        ->condition('field_category', $id)
        ->condition('status', 1)
        ->accessCheck(false);
      $pids = $query->execute();
      $variations = ProductVariation::loadMultiple($pids);

      $tids = [];
      foreach ($variations as $variation) {
        $tids[] = $variation->field_range_cooker_collection->getValue()[0]["target_id"];
      }
      $tids = array_unique($tids);
      $collections = \Drupal::entityTypeManager()
        ->getStorage('taxonomy_term')
        ->loadMultiple($tids);
      foreach ($collections as $term) {
        //image
        if ($uri = $term->get('field_category_image')->entity) {
          $uri = $uri->uri->value;
          $image = file_create_url($uri);
        } else {
          $image = null;
        }

        $data[] = [
          'id' => $term->id(),
          'name' => $term->getName(),
          'image' => $image,
        ];
      }

      $response = [
        'status' => true,
        'type' => 'Collection',
        'message' => t('Success'),
        'data' => $data,
      ];
    }

    if (empty($data)) {
      $response = [
        'status' => false,
        'type' => 'Collection',
        'message' => t('Nothing here'),
        'data' => $data,
      ];
      $status = 404;
    }



    $build = [
      '#cache' => [
        'max-age' => 0,
      ],
    ];

    return (new ResourceResponse($response, $status))->addCacheableDependency($build);
  }

}
