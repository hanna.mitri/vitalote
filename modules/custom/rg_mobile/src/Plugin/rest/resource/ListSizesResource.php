<?php

namespace Drupal\rg_mobile\Plugin\rest\resource;

use Drupal\commerce_product\Entity\ProductVariation;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;

/**
 * Provides a resource to get articles.
 *
 * @RestResource(
 *   id = "sizes_resource",
 *   label = @Translation("List sizes resource"),
 *   uri_paths = {
 *     "canonical" = "/api/v1/get-sizes/{category_id}/{collection_id}"
 *   }
 * )
 */
class ListSizesResource extends ResourceBase {

  /**
   * Responds to GET requests.
   */
  public function get($category_id = null, $collection_id = null) {
    $data = [];
    $status = 200;
    $category_id =  (int) $category_id;
    $collection_id =  (int) $collection_id;


    if (is_int($category_id) && is_int($collection_id)) {
      $query = \Drupal::entityQuery('commerce_product_variation')
        ->condition('field_category', $category_id)
        ->condition('field_range_cooker_collection', $collection_id)
        ->condition('status', 1)
        ->accessCheck(false);
      if ($query_size = \Drupal::request()->query->get('size')) {
        $query->condition('attribute_size', $query_size, '=');
      }
      $vids = $query->execute();
      $variations = ProductVariation::loadMultiple($vids);

      foreach ($variations as $variation) {
        //prepare data
        $sizes = $variation->getAttributeValue('attribute_size');

        $data[$sizes->id()] =  [
            'id' => $sizes->id(),
            'name' => $sizes->name->value,
        ];
      }
      usort($data, function($a, $b) {
        return $a['name'] <=> $b['name'];
      });

      $response = [
        'status' => true,
        'type' => 'Attribute size',
        'message' => t('Success'),
        'data' => $data,
      ];
    }
    if (empty($data)) {
      $response = [
        'status' => false,
        'type' => 'Attribute size',
        'message' => t('Nothing here'),
        'data' => $data,
      ];
      $status = 404;
    }

    $build = [
      '#cache' => [
        'max-age' => 0,
      ],
    ];

    return (new ResourceResponse($response, $status))->addCacheableDependency($build);
  }

}
