<?php

namespace Drupal\rg_mobile\Plugin\rest\resource;

use Drupal\commerce_product\Entity\ProductVariation;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;

/**
 * Provides a resource to get articles.
 *
 * @RestResource(
 *   id = "variations_resource",
 *   label = @Translation("List variations resource"),
 *   uri_paths = {
 *     "canonical" = "/api/v1/category/{category_id}/{collection_id}"
 *   }
 * )
 */
class ListVariationsResource extends ResourceBase {

  /**
   * Responds to GET requests.
   */
  public function get($category_id = null, $collection_id = null) {
    $data = [];
    $filters = [
      'colour' => false,
      'fuel' => false,
      'size' => false,
      'trim' => false,
      'no_of_bowls' => false,
    ];
    $status = 200;
    $category_id =  (int) $category_id;
    $collection_id =  (int) $collection_id;


    if (is_int($category_id) && is_int($collection_id)) {
      $query = \Drupal::entityQuery('commerce_product_variation')
        ->condition('field_category', $category_id)
        ->condition('field_range_cooker_collection', $collection_id)
        ->condition('status', 1)
        ->accessCheck(false);
      if ($query_colour = \Drupal::request()->query->get('colour')) {
        $query->condition('attribute_color', $query_colour, '=');
      }
      if ($query_trim = \Drupal::request()->query->get('trim')) {
        $query->condition('attribute_trim', $query_trim, '=');
      }
      if ($query_size = \Drupal::request()->query->get('size')) {
        $query->condition('attribute_size', $query_size, '=');
      }
      if ($query_fuel = \Drupal::request()->query->get('fuel')) {
        $query->condition('attribute_fuel', $query_fuel, '=');
      }
      if ($query_no_of_bowls = \Drupal::request()->query->get('no_of_bowls')) {
        $query->condition('attribute_no_of_bowls', $query_no_of_bowls, '=');
      }
      $vids = $query->execute();
      $variations = ProductVariation::loadMultiple($vids);

      /**
       * @var ProductVariation $variation
       */
      foreach ($variations as $key => $variation) {
        //prepare data
        global $base_url;
        $product_id = $variation->product_id->getValue()[0]["target_id"];
        $variation_id = $variation->id();

        $data[$key] = [
          'id' => $variation_id,
          'sku' => $variation->getSku(),
          'path' => "$base_url/product/$product_id?v=$variation_id",
          'title' => $variation->getTitle(),
          'image' => $variation->field_dam_thumb->value,
          'collection' => $variation->field_range_cooker_collection->getValue()[0]['target_id'],
          'category' => $variation->field_category->getValue()[0]['target_id'],
          'sticky_status' => $variation->field_sticky_status->value,
          'created' => $variation->getCreatedTime(),
        ];

        //attributes
        //color
        if ($variation->hasField('attribute_color') &&
          $color = $variation->getAttributeValue('attribute_color')) {
          $filters['colour'] = true;
          $data[$key]['colour'] = [
            'id' => $color->id(),
            'name' => $color->name->value,
            'hex_code' => $color->field_hex_color_code->value,
          ];
        }
        //trim
        if ($variation->hasField('attribute_trim') &&
          $trim = $variation->getAttributeValue('attribute_trim')) {
          $filters['trim'] = true;
          $data[$key]['trim'] = [
            'id' => $trim->id(),
            'name' => $trim->name->value,
            'hex_code' => $trim->field_hex_color_code->value,
          ];
        }
        //fuel
        if ($variation->hasField('attribute_fuel') &&
          $fuel = $variation->getAttributeValue('attribute_fuel')) {
          $filters['fuel'] = true;
          $uri = $fuel->get('field_fuel_type_icon')->entity->uri->value;
          $data[$key]['fuel'] = [
            'id' => $fuel->id(),
            'name' => $fuel->name->value,
            'icon' => $uri ? file_create_url($uri) : null,
          ];
        }
        //size
        if ($variation->hasField('attribute_size') &&
          $size = $variation->getAttributeValue('attribute_size')) {
          $filters['size'] = true;
          $data[$key]['size'] = [
            'id' => $size->id(),
            'name' => $size->name->value,
          ];
        }
        //no. of bowls
        if ($variation->hasField('attribute_no_of_bowls') &&
          $size = $variation->getAttributeValue('attribute_no_of_bowls')) {
          $filters['no_of_bowls'] = true;
          $data[$key]['no_of_bowls'] = [
            'id' => $size->id(),
            'name' => $size->name->value,
          ];
        }
      }

      usort($data, function($a, $b) {
        return [$b['sticky_status'], $b['created'], $a['title']] <=> [$a['sticky_status'], $a['created'], $b['title']];
      });

      $response = [
        'status' => true,
        'type' => 'Product variation',
        'message' => t('Success'),
        'filters' => $filters,
        'data' => array_values($data),
      ];
    }
    if (empty($data)) {
      $response = [
        'status' => false,
        'type' => 'Product variation',
        'message' => t('Nothing here'),
        'filters' => $filters,
        'data' => $data,
      ];
      $status = 404;
    }


    $build = [
      '#cache' => [
        'max-age' => 0,
      ],
    ];

    return (new ResourceResponse($response, $status))->addCacheableDependency($build);
  }

}
