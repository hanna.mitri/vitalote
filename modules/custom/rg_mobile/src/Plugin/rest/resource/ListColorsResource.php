<?php

namespace Drupal\rg_mobile\Plugin\rest\resource;

use Drupal\commerce_product\Entity\ProductAttributeValue;
use Drupal\commerce_product\Entity\ProductVariation;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;

/**
 * Provides a resource to get articles.
 *
 * @RestResource(
 *   id = "colors_resource",
 *   label = @Translation("List colors resource"),
 *   uri_paths = {
 *     "canonical" = "/api/v1/get-colors/{category_id}/{collection_id}"
 *   }
 * )
 */
class ListColorsResource extends ResourceBase {

  /**
   * Responds to GET requests.
   */
  public function get($category_id = null, $collection_id = null) {
    $data = [];
    $status = 200;
    $category_id =  (int) $category_id;
    $collection_id =  (int) $collection_id;


    if (is_int($category_id) && is_int($collection_id)) {
      $query = \Drupal::entityQuery('commerce_product_variation')
        ->condition('field_category', $category_id)
        ->condition('field_range_cooker_collection', $collection_id)
        ->condition('status', 1)
        ->accessCheck(false);
      if ($query_colour = \Drupal::request()->query->get('colour')) {
        $query->condition('attribute_color', $query_colour, '=');
      }
      $vids = $query->execute();
      $variations = ProductVariation::loadMultiple($vids);

      foreach ($variations as $variation) {
        //prepare data
        $color = $variation->getAttributeValue('attribute_color');

        $data[$color->id()] =  [
            'id' => $color->id(),
            'name' => $color->name->value,
            'hex_code' => $color->field_hex_color_code->value,
        ];
      }
      $data = array_values($data);

      $response = [
        'status' => true,
        'type' => 'Attribute color',
        'message' => t('Success'),
        'data' => $data,
      ];
    }
    if (empty($data)) {
      $response = [
        'status' => false,
        'type' => 'Attribute color',
        'message' => t('Nothing here'),
        'data' => $data,
      ];
      $status = 404;
    }


    $build = [
      '#cache' => [
        'max-age' => 0,
      ],
    ];

    return (new ResourceResponse($response, $status))->addCacheableDependency($build);
  }

}
