<?php

namespace Drupal\rg_mobile\Plugin\rest\resource;

use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;

/**
 * Provides a resource to get articles.
 *
 * @RestResource(
 *   id = "categories_resource",
 *   label = @Translation("List categories resource"),
 *   uri_paths = {
 *     "canonical" = "/api/v1/category"
 *   }
 * )
 */
class ListCategoriesResource extends ResourceBase
{

  /**
   * Responds to GET requests.
   */
  public function get()
  {
    $data = [];
    // Find categories.
    $query = \Drupal::entityQuery('taxonomy_term')
      ->condition('vid', 'mt_product_tags')
      ->condition('field_on_mobile', true)
      ->accessCheck(false);
    $tids = $query->execute();
    $categories = \Drupal::entityTypeManager()
      ->getStorage('taxonomy_term')
      ->loadMultiple($tids);
    foreach ($categories as $term) {
      if ($uri = $term->get('field_category_image')->entity) {
        $uri = $uri->uri->value;
        $image = file_create_url($uri);
      } else {
        $image = null;
      }
      $data[] = [
        'id' => $term->id(),
        'name' => $term->getName(),
        'image' => $image,
      ];
    }
    $build = [
      '#cache' => [
        'max-age' => 0,
      ],
    ];
    $response = [
      'status' => true,
      'message' => t('Success'),
      'type' => 'Category',
      'data' => $data,
    ];
    return (new ResourceResponse($response, 200))->addCacheableDependency($build);
  }

}
