<?php

namespace Drupal\rg_mobile\Plugin\rest\resource;

use Drupal\commerce_product\Entity\ProductAttributeValue;
use Drupal\commerce_product\Entity\ProductVariation;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;

/**
 * Provides a resource to get articles.
 *
 * @RestResource(
 *   id = "no_of_bowls_resource",
 *   label = @Translation("List no. of bowls resource"),
 *   uri_paths = {
 *     "canonical" = "/api/v1/get-no_of_bowls/{category_id}/{collection_id}"
 *   }
 * )
 */
class ListNoofBowlsResource extends ResourceBase {

  /**
   * Responds to GET requests.
   */
  public function get($category_id = null, $collection_id = null) {
    $data = [];
    $status = 200;
    $category_id =  (int) $category_id;
    $collection_id =  (int) $collection_id;


    if (is_int($category_id) && is_int($collection_id)) {
      $query = \Drupal::entityQuery('commerce_product_variation')
        ->condition('field_category', $category_id)
        ->condition('field_range_cooker_collection', $collection_id)
        ->condition('status', 1)
        ->accessCheck(false);
      if ($query_no_of_bowls = \Drupal::request()->query->get('no_of_bowls')) {
        $query->condition('attribute_no_of_bowls', $query_no_of_bowls, '=');
      }
      $vids = $query->execute();
      $variations = ProductVariation::loadMultiple($vids);

      foreach ($variations as $variation) {
        //prepare data
        $no_of_bowls = $variation->getAttributeValue('attribute_no_of_bowls');

        $data[$no_of_bowls->id()] =  [
            'id' => $no_of_bowls->id(),
            'name' => $no_of_bowls->name->value,
        ];
      }
      $data = array_values($data);

      $response = [
        'status' => true,
        'type' => 'Attribute no_of_bowls',
        'message' => t('Success'),
        'data' => $data,
      ];
    }
    if (empty($data)) {
      $response = [
        'status' => false,
        'type' => 'Attribute no_of_bowls',
        'message' => t('Nothing here'),
        'data' => $data,
      ];
      $status = 404;
    }


    $build = [
      '#cache' => [
        'max-age' => 0,
      ],
    ];

    return (new ResourceResponse($response, $status))->addCacheableDependency($build);
  }

}
