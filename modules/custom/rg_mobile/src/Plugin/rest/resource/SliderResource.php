<?php

namespace Drupal\rg_mobile\Plugin\rest\resource;

use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;

/**
 * Provides a resource to get slider nodes.
 *
 * @RestResource(
 *   id = "slider_resource",
 *   label = @Translation("Slider ofn explore"),
 *   uri_paths = {
 *     "canonical" = "/api/v1/slider"
 *   }
 * )
 */
class SliderResource extends ResourceBase
{

  /**
   * Responds to GET requests.
   */
  public function get()
  {
    $data = [];
    // Find categories.
    $nids = \Drupal::entityQuery('node')
      ->condition('status', 1)
      ->condition('field_display_on_mobile', 1)
      ->condition('type', 'mt_slideshow_entry')
      ->accessCheck(false)
      ->execute();
    $nodes = \Drupal\node\Entity\Node::loadMultiple($nids);
    foreach ($nodes as $node) {
      //image
      if ($uri = $node->get('field_mt_slideshow_image')->entity) {
        $uri = $uri->uri->value;
        $image = file_create_url($uri);
      } else {
        continue;
      }

      $data[] = [
        'id' => $node->id(),
        'name' => strip_tags($node->field_title->value),
        'path' => $node->field_mt_slideshow_path->value,
        'image' => $image,
      ];
    }
    $build = [
      '#cache' => [
        'max-age' => 0,
      ],
    ];
    $response = [
      'status' => true,
      'message' => t('Success'),
      'type' => 'Slider',
      'data' => $data,
    ];
    return (new ResourceResponse($response, 200))->addCacheableDependency($build);
  }

}
