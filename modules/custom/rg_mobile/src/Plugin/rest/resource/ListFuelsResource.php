<?php

namespace Drupal\rg_mobile\Plugin\rest\resource;

use Drupal\commerce_product\Entity\ProductAttributeValue;
use Drupal\commerce_product\Entity\ProductVariation;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;

/**
 * Provides a resource to get articles.
 *
 * @RestResource(
 *   id = "fuels_resource",
 *   label = @Translation("List fuels resource"),
 *   uri_paths = {
 *     "canonical" = "/api/v1/get-fuels/{category_id}/{collection_id}"
 *   }
 * )
 */
class ListFuelsResource extends ResourceBase {

  /**
   * Responds to GET requests.
   */
  public function get($category_id = null, $collection_id = null) {
    $data = [];
    $status = 200;
    $category_id =  (int) $category_id;
    $collection_id =  (int) $collection_id;


    if (is_int($category_id) && is_int($collection_id)) {
      $query = \Drupal::entityQuery('commerce_product_variation')
        ->condition('field_category', $category_id)
        ->condition('field_range_cooker_collection', $collection_id)
        ->condition('status', 1)
        ->accessCheck(false);
      if ($query_fuel = \Drupal::request()->query->get('fuel')) {
        $query->condition('attribute_fuel', $query_fuel, '=');
      }
      $vids = $query->execute();
      $variations = ProductVariation::loadMultiple($vids);

      foreach ($variations as $variation) {
        //prepare data
        $fuel = $variation->getAttributeValue('attribute_fuel');
        //image
        if ($uri = $fuel->get('field_fuel_type_icon')->entity) {
          $uri = $uri->uri->value;
          $image = file_create_url($uri);
        } else {
          $image = null;
        }

        $data[$fuel->id()] =  [
            'id' => $fuel->id(),
            'name' => $fuel->name->value,
            'icon' => $image,
        ];
      }
      $data = array_values($data);

      $response = [
        'status' => true,
        'type' => 'Attribute fuel',
        'message' => t('Success'),
        'data' => $data,
      ];
    }
    if (empty($data)) {
      $response = [
        'status' => false,
        'type' => 'Attribute fuel',
        'message' => t('Nothing here'),
        'data' => $data,
      ];
      $status = 404;
    }


    $build = [
      '#cache' => [
        'max-age' => 0,
      ],
    ];

    return (new ResourceResponse($response, $status))->addCacheableDependency($build);
  }

}
