<?php

namespace Drupal\rg_store\Controller;

use Drupal\commerce_store\Entity\StoreInterface;
use Drupal\Core\Controller\ControllerBase;

/**
 * Class StoreModerationController.
 */
class StoreModerationController extends ControllerBase {


  /**
   * @param StoreInterface $commerce_store
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   * @throws \Drupal\Core\Entity\EntityMalformedException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function approveChanges(StoreInterface $commerce_store) {

    $description = $commerce_store->field_fake_description->value;
    $commerce_store->set('field_store_description', [
      'value' => $description,
      'format' => 'full_html'
    ]);
    $commerce_store->save();

    return $this->redirect('entity.commerce_store.canonical', ['commerce_store' => $commerce_store->id()]);
  }


  /**
   * @param StoreInterface $commerce_store
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   * @throws \Drupal\Core\Entity\EntityMalformedException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function denyChanges(StoreInterface $commerce_store) {

    $description = $commerce_store->field_store_description->value;
    $commerce_store->set('field_fake_description', [
      'value' => $description,
      'format' => 'full_html'
    ]);
    $commerce_store->save();

    return $this->redirect('entity.commerce_store.canonical', ['commerce_store' => $commerce_store->id()]);
  }

}
