<?php
/**
 * Created by PhpStorm.
 * User: ginger
 * Date: 07.11.18
 * Time: 16:59
 */

namespace Drupal\rg_store\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Drupal\commerce_store\Entity\StoreInterface;
use Drupal\commerce_product\Entity\ProductInterface;


/**
 * Provides route responses for the Example module.
 */
class StoreLocatorController extends ControllerBase
{

  /**
   * @param StoreInterface $commerce_store
   * @param ProductInterface $commerce_product
   * @return RedirectResponse
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function removeFromStore(StoreInterface $commerce_store, ProductInterface $commerce_product)
  {
    $storeId = $commerce_store->id();
    $ids = $commerce_product->getStoreIds();
    array_splice($ids, array_search($storeId, $ids), 1);

    $commerce_product->setStoreIds($ids);
    $commerce_product->save();

    return new RedirectResponse($_SERVER["HTTP_REFERER"]);
  }

  /**
   * @param \Drupal\commerce_store\Entity\StoreInterface $commerce_store
   * @param \Drupal\commerce_product\Entity\ProductInterface $commerce_product
   * @param \Drupal\Core\Session\AccountInterface $account
   *   Run access checks for this account.
   *
   * @return AccessResult
   */
  public function access(StoreInterface $commerce_store, ProductInterface $commerce_product, AccountInterface $account)
  {
    $uid = $account->id();
    $owner_id = $commerce_store->getOwnerId();
    return AccessResult::allowedIf($uid == $owner_id || $uid == 1);
  }

  /**
   * @param StoreInterface $commerce_store
   * @param ProductInterface $commerce_product
   * @return RedirectResponse
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function AddToStore(StoreInterface $commerce_store, ProductInterface $commerce_product)
  {
    $storeId = $commerce_store->id();
    $ids = $commerce_product->getStoreIds();
    if (!in_array($storeId, $ids)) {
      $ids[] = $storeId;
      $commerce_product->setStoreIds($ids);
      $commerce_product->save();
    }
    return new RedirectResponse($_SERVER["HTTP_REFERER"]);
  }

  /**
   * Return stores of specific user
   * @param $user
   * @return \Drupal\Core\Entity\EntityInterface[]
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public static function getUserStores($user)
  {
    return \Drupal::entityTypeManager()->getStorage('commerce_store')->loadByProperties(['uid' => $user->id()]);
  }
}