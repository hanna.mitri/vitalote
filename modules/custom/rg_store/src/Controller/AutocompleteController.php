<?php

namespace Drupal\rg_store\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Database\Database;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\rg_store\StoreHelper;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Defines a route controller for entity autocomplete form elements.
 */
class AutocompleteController extends ControllerBase {

  /**
   * Handler for autocomplete request.
   */
  public function handleAutocomplete(Request $request) {

    $results = [];
    if ($input = $request->query->get('q')) {
      $connection = Database::getConnection();
      $query = $connection->select('commerce_store__field_products', 'rp');
      $query->join('commerce_store_field_data', 's', 's.store_id = rp.entity_id');
      $query->join('commerce_product_variation_field_data', 'v', 'v.variation_id = rp.field_products_target_id');
      $query->fields('rp', ['field_products_target_id']);
      $query->fields('v', ['title']);
      $query->condition('v.title', '%' .$input . '%', 'LIKE');
      $session = \Drupal::request()->getSession();
      $sids = $session->get('store_locator_sids');
      if ($sids) {
        $sids = json_decode($sids);
        $query->condition('s.store_id', $sids, 'IN');
      }
      $query->range(0, 10);
      $data = $query->distinct()->execute();

      $query_result = $data->fetchAllKeyed();

      foreach ($query_result as $key => $row) {
        $results[] = [
          'value' => $row . ' (' . $key . ')',
          'label' => $row,
        ];
      }
    }
    return new JsonResponse($results);
  }

}