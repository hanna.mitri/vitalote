<?php

namespace Drupal\rg_store\Form;

use Drupal\commerce_product\Entity\ProductAttributeValue;
use Drupal\commerce_product\Entity\ProductVariation;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Ajax\InvokeCommand;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Database\Database;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\taxonomy\Entity\Term;

/**
 * Class DependentSelect.
 */
class DependentSelect extends FormBase
{

  /**
   * {@inheritdoc}
   */
  public function getFormId()
  {
    return 'dependent_select';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state)
  {
    $session = $this->getRequest()->getSession();
    if (!\Drupal::request()->query->get('pid')) {
      $session->set('store_locator_category', null);
      $session->set('store_locator_collection', null);
      $session->set('store_locator_size', null);
      $session->set('store_locator_fuel', null);
      $session->set('store_locator_color', null);
      $session->set('store_locator_chosen', null);
    }

    $form['group'] = [
      '#type' => 'container',
      '#title' => t('Product search'),
      '#attributes' => ['id' => ['dependent-select-wrapper']],
      '#prefix' => '
        <div class="product_search_summary"><i class="fa fa-angle-right"></i><span>Product search</span></div>
        ',
    ];
    if (empty(\Drupal::request()->query->all())) {
      $form['group']['#attributes'] = ['class' => ['hidden']];
    }

    $category = $session->get('store_locator_category');
    $form['group']['category'] = [
      '#type' => 'select',
      '#options' => $this->getCategoryOptions(),
      '#empty_option' => t('-Select by Category-'),
      '#value' => $category,
      '#ajax' => [
        'event' => 'change',
        'callback' => '::ajaxFamilyCallback',
        'wrapper' => 'dropdown_second_replace',
      ],
    ];

    $pids = $this->getAvailableVariations($category);

    $collection = $session->get('store_locator_collection');
    $collection_options = $this->getCollectionOptions($pids);
    $form['group']['collection'] = [
      '#type' => 'select',
      '#options' => $collection_options,
      '#empty_option' => t('-Product Family-'),
      '#value' => $collection,
      '#ajax' => [
        'event' => 'change',
        'callback' => '::ajaxAttributeCallback',
        'wrapper' => 'dropdown_third_replace',
      ],
      '#prefix' => '<div id="dropdown_second_replace">',
      '#suffix' => '</div>',
      '#validated' => true,
    ];


    if ($category || $collection) {
      $pids = $this->getAvailableVariations($category, $collection);
      $size_options = $this->getAttributeOptions($pids, 'attribute_size');
      $fuel_options = $this->getAttributeOptions($pids, 'attribute_fuel');
      $color_options = $this->getAttributeOptions($pids, 'attribute_color');
    }
    else {
      $size_options = [];
      $fuel_options = [];
      $color_options = [];
    }

    $size = $session->get('store_locator_size');
    $form['group']['attributes']['size'] = [
      '#type' => 'select',
      '#options' => $size_options,
      '#empty_option' => t('-Size-'),
      '#value' => $size,
      '#ajax' => [
        'event' => 'change',
        'callback' => '::ajaxSetIdCallback',
        'wrapper' => 'dropdown_third_replace',
      ],
      '#prefix' => '<div id="dropdown_third_replace">',
      '#validated' => true,
    ];

    $form['group']['attributes']['fuel'] = [
      '#type' => 'select',
      '#options' => $fuel_options,
      '#empty_option' => t('-Fuel Type-'),
      '#value' => $session->get('store_locator_fuel'),
      '#ajax' => [
        'event' => 'change',
        'callback' => '::ajaxSetIdCallback',
        'wrapper' => 'dropdown_third_replace',
      ],
      '#validated' => true,
    ];

    $form['group']['attributes']['color'] = [
      '#type' => 'select',
      '#options' => $color_options,
      '#empty_option' => t('-Colour-'),
      '#value' => $session->get('store_locator_color'),
      '#ajax' => [
        'event' => 'change',
        'callback' => '::ajaxSetIdCallback',
        'wrapper' => 'dropdown_third_replace',
      ],
      '#validated' => true,
      '#suffix' => '</div>',
    ];


    $message = '';
    $form['group']['description'] = [
      '#markup' => '<div class="description-message">' . $message . '</div>',
      '#allowed_tags' => ['div'],
    ];
    $form['group']['chosen'] = [
      '#markup' => '<div class="chosen-value">' .
        $session->get('store_locator_chosen') . '</div>',
      '#allowed_tags' => ['div'],
    ];
    $form['#cache'] = ['max-age' => 0];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state)
  {
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state)
  {
  }

  public function ajaxSetIdCallback($form, $form_state)
  {
    $input = $form_state->getUserInput();
    $input['collection'] = isset($input['collection']) ? $input['collection'] : '';
    $input['size'] = isset($input['size']) ? $input['size'] : '';
    $input['fuel'] = isset($input['fuel']) ? $input['fuel'] : '';
    $input['color'] = isset($input['color']) ? $input['color'] : '';

    $session = $this->getRequest()->getSession();
    $session->set('store_locator_category', $input['category']);
    $session->set('store_locator_collection', $input['collection']);
    $session->set('store_locator_size', $input['size']);
    $session->set('store_locator_fuel', $input['fuel']);
    $session->set('store_locator_color', $input['color']);

    $response = new AjaxResponse();

    $pids = $this->getAvailableVariations(
      $input['category'],
      $input['collection'],
      $input['size'],
      $input['fuel'],
      $input['color']
    );

    if ($pids) {
      $chosen = '';
      if ($category = Term::load($input['category']) ){
        $chosen .= ' ';
        $chosen .= $category->get('name')->value;
      }
      if ($collection = Term::load($input['collection']) ){
        $chosen .= ' ';
        $chosen .= $collection->get('name')->value;
      }
      if ($size = ProductAttributeValue::load($input['size'])) {
        $chosen .= ' ';
        $chosen .= $size->get('name')->value;
      }
      if ($fuel = ProductAttributeValue::load($input['fuel'])) {
        $chosen .= ' ';
        $chosen .= $fuel->get('name')->value;
      }
      if ($color = ProductAttributeValue::load($input['color'])) {
        $chosen .= ' ';
        $chosen .= $color->get('name')->value;
      }
    }
    else {
      $chosen = $this->t('No products found');
      $pids = [];
    }

    $session->set('store_locator_chosen', $chosen);

    $response->addCommand(new InvokeCommand(
        NULL,
        'changeInputValueAjaxCallback',
        [
          'product-id-wrapper',
          json_encode($pids),
          (string)$chosen
        ])
    );

    // Return ajax response.
    return $response;
  }

  /**
   * @param $form
   * @param $form_state
   * @return mixed
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function ajaxFamilyCallback($form, $form_state)
  {
    $input = $form_state->getUserInput();
    unset($input['collection']);
    unset($input['size']);
    unset($input['fuel']);
    unset($input['color']);
    $form_state->setUserInput($input);

    $pids = $this->getAvailableVariations($input['category']);

    $response = $this->ajaxSetIdCallback($form, $form_state);

    //change collection
    $null_value = [null => t('-Product Family-')];
    $options = $null_value + $this->getCollectionOptions($pids);
    $renderedField = '';
    foreach ($options as $key => $value)
    {
      $renderedField .= "<option value='" . $key . "'>" . $value . "</option>";
    }
    $response->addCommand(new HtmlCommand('#dropdown_second_replace select', $renderedField));

    //change attributes
    $attributes = [
      'size',
      'fuel',
      'color',
    ];
    foreach ($attributes as $attribute) {
      $attributeOptions = $form['group']['attributes'][$attribute]['#options'] +
        $this->getAttributeOptions($pids, 'attribute_' . $attribute);
      $rendered = '';
      foreach ($attributeOptions as $key => $value)
      {
        $rendered .= "<option value='" . $key . "'>" . $value . "</option>";
      }
      $response->addCommand(new HtmlCommand('#edit-' . $attribute, $rendered));
    }

    return $response;

  }


  /**
   * @param $form
   * @param $form_state
   * @return mixed
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function ajaxAttributeCallback($form, $form_state)
  {
    $input = $form_state->getUserInput();
    $category_id = $input['category'];
    $collection_id = $input['collection'];
    unset($input['size']);
    unset($input['fuel']);
    unset($input['color']);
    $form_state->setUserInput($input);
    $response = $this->ajaxSetIdCallback($form, $form_state);


    $pids = $this->getAvailableVariations($category_id, $collection_id);

    $attributes = [
      'size',
      'fuel',
      'color',
    ];
    foreach ($attributes as $attribute) {
      $options = $form['group']['attributes'][$attribute]['#options'] +
        $this->getAttributeOptions($pids, 'attribute_' . $attribute);
      $renderedField = '';
      foreach ($options as $key => $value)
      {
        $renderedField .= "<option value='" . $key . "'>" . $value . "</option>";
      }
      $response->addCommand(new HtmlCommand('#edit-' . $attribute, $renderedField));
    }

    return $response;
  }

  /**
   * Return options for category select
   *
   * @return array
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getCategoryOptions()
  {
    $options = [];

    $query = \Drupal::entityQuery('taxonomy_term')
      ->condition('vid', 'mt_product_tags')
      ->condition('field_on_mobile', true);
    $tids = $query->execute();
    $categories = \Drupal::entityTypeManager()
      ->getStorage('taxonomy_term')
      ->loadMultiple($tids);
    foreach ($categories as $term) {
      $options[$term->id()] = $term->getName();
    }
    return $options;
  }

  /**
   * Return options for collection select
   *
   * @param $pids
   * @return array
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getCollectionOptions($pids)
  {
    $options = [];

    if (!empty($pids)) {
      $variations = ProductVariation::loadMultiple($pids);
      $tids = [];
      foreach ($variations as $variation) {
          $tids[] = $variation->field_range_cooker_collection->getValue()[0]["target_id"];
      }
      $tids = array_unique($tids);
      $collections = \Drupal::entityTypeManager()
        ->getStorage('taxonomy_term')
        ->loadMultiple($tids);
      foreach ($collections as $term) {
        $options[$term->id()] = $term->getName();
      }
    }

    return $options;
  }

  /**
   * Return options for attribute select
   *
   * @param $pids
   * @param $attribute_name
   * @return array
   */
  public function getAttributeOptions($pids, $attribute_name)
  {
    $options = [];
    $variations = ProductVariation::loadMultiple($pids);

    foreach ($variations as $variation) {
      if ($variation->hasField($attribute_name)) {
        $attribute = $variation->getAttributeValue($attribute_name);
        if ($attribute) {
          $options[$attribute->id()] = $attribute->getName();
        }
      }
    }

    return $options;
  }

  /**
   * @return mixed all products which is connected to stores
   */
  public function getAvailableVariations($category = null, $collection = null, $size = null, $fuel = null, $color = null)
  {
    $connection = Database::getConnection();
    $query = $connection->select('commerce_store__field_products', 'rp');
    $query->join('commerce_store_field_data', 's', 's.store_id = rp.entity_id');
    $query->join('commerce_product_variation_field_data', 'v', 'v.variation_id = rp.field_products_target_id');
    if ($category){
      $query->join('commerce_product_variation__field_category', 'category', 'v.variation_id = category.entity_id');
      $query->condition('category.field_category_target_id', $category);
    }
    if ($collection){
      //to get real table name of field
      $entity_manager = \Drupal::getContainer()->get('entity.manager');
      $storage_definitions = $entity_manager->getFieldStorageDefinitions('commerce_product_variation');
      $table_mapping = $entity_manager->getStorage('commerce_product_variation')->getTableMapping();
      $collection_table = $table_mapping->getDedicatedDataTableName($storage_definitions['field_range_cooker_collection']);

      $query->join($collection_table, 'collection', 'v.variation_id = collection.entity_id');
      $query->condition('collection.field_range_cooker_collection_target_id', $collection);
    }
    if ($size){
      $query->join('commerce_product_variation__attribute_size', 'size', 'v.variation_id = size.entity_id');
      $query->condition('size.attribute_size_target_id', $size);
    }
    if ($fuel){
      $query->join('commerce_product_variation__attribute_fuel', 'fuel', 'v.variation_id = fuel.entity_id');
      $query->condition('fuel.attribute_fuel_target_id', $fuel);
    }
    if ($color){
      $query->join('commerce_product_variation__attribute_color', 'color', 'v.variation_id = color.entity_id');
      $query->condition('color.attribute_color_target_id', $color);
    }
    $query->condition('v.status', 1);

    $query->fields('rp', ['field_products_target_id']);
    $data = $query->distinct()->execute();
    return $data->fetchCol();
  }
}
