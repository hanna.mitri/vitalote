<?php

namespace Drupal\rg_store\Form;

use Drupal\commerce_product\Entity\ProductVariation;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\InvokeCommand;
use Drupal\Core\Database\Database;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class AutocompleteProducts.
 */
class AutocompleteProducts extends FormBase
{


  /**
   * {@inheritdoc}
   */
  public function getFormId()
  {
    return 'autocomplete_products';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state)
  {
    $session = $this->getRequest()->getSession();
    if (!\Drupal::request()->query->get('pid')) {
      $session->set('store_locator_pid_autocomplete', null);
      $session->set('store_locator_chosen', null);
    }
    $chosen = $session->get('store_locator_pid_autocomplete') ? $session->get('store_locator_chosen') : '';

    $form['pid_autocomplete'] = [
      '#type' => 'textfield',
      '#value' => $chosen,
      '#placeholder' => t('Product'),
      '#autocomplete_route_name' => 'rg_store.products_on_store',
      '#ajax' => array(
        'callback' => '::rg_store_autocomplete_ajax_callback',
        'event' => 'autocompleteclose',
      ),
    ];
    if (empty(\Drupal::request()->query->all())) {
      $form['pid_autocomplete']['#attributes'] = ['class' => ['hidden']];
    }
    $form['#cache'] = ['max-age' => 0];

    return $form;
  }


  public function rg_store_autocomplete_ajax_callback(array &$form, FormStateInterface $form_state){
    $response = new AjaxResponse();

    $input = $form_state->getUserInput();
    preg_match_all("/\([0-9]{1,}\)/", $input['pid_autocomplete'], $pid);

    $pid = (int) str_replace(['(', ')'], ['', ''], $pid[0][0]);
    if ($pid) {
      $response->addCommand(new InvokeCommand(
          NULL,
          'changeInputValueOnAutocompleteAjaxCallback',
          [
            'product-id-wrapper',
            $pid,
            $input['pid_autocomplete']
          ])
      );
      $session = $this->getRequest()->getSession();
      $session->set('store_locator_pid_autocomplete', $pid);
      $session->set('store_locator_chosen', $input['pid_autocomplete']);
    }

    // Return ajax response.
    return $response;
  }
  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state)
  {
//    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state)
  {
  }
}
