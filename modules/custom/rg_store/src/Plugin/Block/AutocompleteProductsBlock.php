<?php

namespace Drupal\rg_store\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'DependentSelectBlock' block.
 *
 * @Block(
 *  id = "autocomplete_products_block",
 *  admin_label = @Translation("Autocomplete products block"),
 * )
 */
class AutocompleteProductsBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $form = \Drupal::formBuilder()->getForm('Drupal\rg_store\Form\AutocompleteProducts');
    return $form;
  }

}
