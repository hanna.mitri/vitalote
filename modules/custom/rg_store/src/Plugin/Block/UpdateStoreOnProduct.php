<?php

namespace Drupal\rg_store\Plugin\Block;

use Drupal\commerce_product\Entity\Product;
use Drupal\commerce_store\Entity\Store;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Session\AccountProxyInterface;

/**
 * Provides a block with a simple text.
 *
 * @Block(
 *   id = "update_store_on_product",
 *   admin_label = @Translation("Update store of product"),
 * )
 */
class UpdateStoreOnProduct extends BlockBase implements ContainerFactoryPluginInterface
{
  /**
   * @var $account \Drupal\Core\Session\AccountProxyInterface
   */
  protected $account;
  protected $product;
  protected $userStores = [];

  /**
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   * @param array $configuration
   * @param string $plugin_id
   * @param mixed $plugin_definition
   *
   * @return static
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition)
  {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('current_user')
    );
  }

  /**
   * @param array $configuration
   * @param string $plugin_id
   * @param mixed $plugin_definition
   * @param \Drupal\Core\Session\AccountProxyInterface $account
   * @param Product $product
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, AccountProxyInterface $account)
  {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->account = $account;
    $this->product = $this->getProductFromPage();
    $this->userStores = $this->getUserStores();
  }

  /**
   * {@inheritdoc}
   */
  public function build()
  {
    $build = [];
    $build['container'] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => [
          'btn btn-group btn-group-justified',
        ],
      ],
    ];
    $productStores = $this->product->getStoreIds();

    foreach ($this->userStores as $key => $storeId) {

      if ($storeId == 1){
        continue;
      }
      $store = Store::load($storeId);
      $storeName = $store->getName();

      if (in_array($storeId, $productStores)) {
        $url = Url::fromRoute(
          'rangemaster.remove_from_store',
          ['commerce_store' => $storeId, 'commerce_product' => $this->product->id()]
        );
        $build['container']['elem-' . $key] = [
          '#type' => 'link',
          '#url' => $url,
          '#title' => $this->t('Remove from ' . $storeName),
          '#attributes' => [
            'class' => [
              'btn btn-danger',
              'remove-from-store',
            ],
          ],
        ];
      } else {
        $url = Url::fromRoute(
          'rangemaster.add_to_store',
          ['commerce_store' => $storeId, 'commerce_product' => $this->product->id()]
        );
        $build['container']['elem-' . $key] = [
          '#type' => 'link',
          '#url' => $url,
          '#title' => $this->t('Add to ' . $storeName),
          '#attributes' => [
            'class' => [
              'btn btn-primary',
              'add-to-store',
            ],
          ],
        ];

      }
    }

    $build['#cache']['max-age'] = 0;

    return $build;
  }

  /**
   * {@inheritdoc}
   */
  protected function blockAccess(AccountInterface $account)
  {
    return AccessResult::allowed();
  }

  /**
   * @return Product|\Drupal\Core\Entity\EntityInterface|null
   */
  private function getProductFromPage()
  {
    $product = null;
    $current_path = \Drupal::service('path.current')->getPath();

    $current_path = explode('/', $current_path);
    if ($current_path[1] == 'product' && is_numeric($current_path[2])) {
      $product = Product::load($current_path[2]);
    }

    return $product;
  }

  /**
   * Return Stores of specific user
   */
  private function getUserStores()
  {
    $userStores = [];
    $uid = $this->account->id();
    $stores = Store::loadMultiple();
    foreach ($stores as $store) {
      /* @var $store Store */
      $ownerId = $store->getOwnerId();
      if ($ownerId == $uid) {
        $userStores[] = $store->id();
      }
    }
    return $userStores;
  }

}