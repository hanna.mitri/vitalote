<?php

namespace Drupal\rg_store\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'DependentSelectBlock' block.
 *
 * @Block(
 *  id = "dependent_select_block",
 *  admin_label = @Translation("Dependent select block"),
 * )
 */
class DependentSelectBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $form = \Drupal::formBuilder()->getForm('Drupal\rg_store\Form\DependentSelect');
    return $form;
  }

}
