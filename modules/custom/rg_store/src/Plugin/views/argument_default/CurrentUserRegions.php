<?php

namespace Drupal\rg_store\Plugin\views\argument_default;

use Drupal\views\Plugin\views\argument_default\ArgumentDefaultPluginBase;

/**
 * Default argument plugin for the current user.
 *
 * @ViewsArgumentDefault(
 *   id = "active_user_regions",
 *   title = @Translation("Regions ID from the current user")
 * )
 */
class CurrentUserRegions extends ArgumentDefaultPluginBase {

  /**
   * The current user.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $currentUser;

  /**
   * Constructs a new CurrentUser object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $user = \Drupal\user\Entity\User::load(\Drupal::currentUser()->id());
    $this->currentUser = $user;
  }

  /**
   * {@inheritdoc}
   */
  public function getArgument() {
    $ids = '';
    if ($this->currentUser) {
      $regions = $this->currentUser->field_target_region->getValue();
      foreach ($regions as $value) {
        $ids .= $value['target_id'] . '+';
      }
      return rtrim($ids, "+");
    }
    return null;
  }
}
