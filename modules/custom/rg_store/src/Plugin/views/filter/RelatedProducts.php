<?php
/**
 * @file
 * Definition of Drupal\rg_store\Plugin\views\filter\RelatedProducts.
 */
namespace Drupal\rg_store\Plugin\views\filter;

use Drupal\Core\Database\Database;
use Drupal\rg_store\StoreHelper;
use Drupal\views\Plugin\views\display\DisplayPluginBase;
use Drupal\views\Plugin\views\filter\InOperator;
use Drupal\views\ViewExecutable;
/**
 * Filters by given list of related content title options.
 *
 * @ingroup views_filter_handlers
 *
 * @ViewsFilter("rg_store_related_products")
 */
class RelatedProducts extends InOperator {
  /**
   * {@inheritdoc}
   */
  public function init(ViewExecutable $view, DisplayPluginBase $display, array &$options = NULL) {
    parent::init($view, $display, $options);
    $this->valueTitle = t('Allowed related content titles');
    $this->definition['options callback'] = array($this, 'generateOptions');
  }

  /**
   * Helper function that generates the options.
   * @return array
   */
  public function generateOptions() {
    $connection = Database::getConnection();
    $query = $connection->select('commerce_store__field_products', 'rp');
    $query->join('commerce_store_field_data', 's', 's.store_id = rp.entity_id');
    $query->join('commerce_product_variation_field_data', 'v', 'v.variation_id = rp.field_products_target_id');
    $query->fields('rp', ['field_products_target_id']);
    $query->fields('v', ['title']);
    $data = $query->distinct()->execute();

    $results = $data->fetchAllKeyed();

    return $results;
  }
}