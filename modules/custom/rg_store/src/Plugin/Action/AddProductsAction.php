<?php

namespace Drupal\rg_store\Plugin\Action;

use Drupal\Core\Entity\Entity\EntityFormDisplay;
use Drupal\Core\Form\FormStateInterface;
use Drupal\views_bulk_operations\Action\ViewsBulkOperationsActionBase;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Add products to store.
 *
 * @Action(
 *   id = "rg_store_add_products_action",
 *   label = @Translation("Add products"),
 *   type = ""
 * )
 */
class AddProductsAction extends ViewsBulkOperationsActionBase {

  use StringTranslationTrait;

  /**
   * @param array $form
   * @param FormStateInterface $form_state
   * @return array
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $entity_type_id = 'commerce_store';
    $bundle = 'brand_store';
    $entityType = \Drupal::entityTypeManager()->getDefinition($entity_type_id);
    $entity = \Drupal::entityTypeManager()->getStorage($entity_type_id)->create([
      $entityType->getKey('bundle') => $bundle,
    ]);
    $form_display = EntityFormDisplay::collectRenderDisplay($entity, 'vbo_add_products');
    $form_display->buildForm($entity, $form, $form_state);
    unset($form["uid"]);
    return $form;
  }

  /**
   * @param array $form
   * @param FormStateInterface $form_state
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state)
  {
    $form_clone = $form;
    $entity_type_id = 'commerce_store';
    $bundle = 'brand_store';
    $entityType = \Drupal::entityTypeManager()->getDefinition($entity_type_id);
    $entity = \Drupal::entityTypeManager()->getStorage($entity_type_id)->create([
      $entityType->getKey('bundle') => $bundle,
    ]);
    $form_display = EntityFormDisplay::collectRenderDisplay($entity, 'bulk_edit');
    $form_display->extractFormValues($entity, $form_clone, $form_state);

    $this->configuration['field_products'] = $entity->field_products->getValue();
  }

  /**
   * {@inheritdoc}
   */
  public function execute($entity = NULL)
  {
    if ($entity->hasField('field_products')) {
      $last_values = $entity->field_products->getValue();
      $products = array_merge($last_values, $this->configuration["field_products"]);
      $products = array_intersect_key($products, array_unique(array_map('serialize', $products)));
      $entity->set('field_products', $products);
      $entity->save();
    }
    return $this->t('Stores updated');
  }

  /**
   * {@inheritdoc}
   */
  public function access($object, AccountInterface $account = NULL, $return_as_object = FALSE) {
    if ($object->getEntityType() === 'commerce_store') {
      $access = $object->access('update', $account, TRUE)
        ->andIf($object->status->access('edit', $account, TRUE));
      return $return_as_object ? $access : $access->isAllowed();
    }

    return TRUE;
  }

}