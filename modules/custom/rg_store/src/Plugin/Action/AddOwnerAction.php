<?php

namespace Drupal\rg_store\Plugin\Action;

use Drupal\Core\Entity\Entity\EntityFormDisplay;
use Drupal\Core\Form\FormStateInterface;
use Drupal\views_bulk_operations\Action\ViewsBulkOperationsActionBase;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Add products to store.
 *
 * @Action(
 *   id = "rg_store_add_owner_action",
 *   label = @Translation("Add owner"),
 *   type = ""
 * )
 */
class AddOwnerAction extends ViewsBulkOperationsActionBase {

  use StringTranslationTrait;

  /**
   * @param array $form
   * @param FormStateInterface $form_state
   * @return array
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $entity_type_id = 'commerce_store';
    $bundle = 'brand_store';
    $entityType = \Drupal::entityTypeManager()->getDefinition($entity_type_id);
    $entity = \Drupal::entityTypeManager()->getStorage($entity_type_id)->create([
      $entityType->getKey('bundle') => $bundle,
    ]);
    $form_display = EntityFormDisplay::collectRenderDisplay($entity, 'vbo_add_owner');
    $form_display->buildForm($entity, $form, $form_state);
    unset($form["uid"]["widget"][0]["target_id"]["#default_value"]);
    $form["attention"] = [
      '#markup' => '<div class="danger">' .
        $this->t('Attention! Old values will be removed if exist') . '</div><br>',
      '#weight' => 100,
    ];
    return $form;
  }

  /**
   * @param array $form
   * @param FormStateInterface $form_state
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state)
  {
    $form_clone = $form;
    $entity_type_id = 'commerce_store';
    $bundle = 'brand_store';
    $entityType = \Drupal::entityTypeManager()->getDefinition($entity_type_id);
    $entity = \Drupal::entityTypeManager()->getStorage($entity_type_id)->create([
      $entityType->getKey('bundle') => $bundle,
    ]);
    $form_display = EntityFormDisplay::collectRenderDisplay($entity, 'bulk_edit');
    $form_display->extractFormValues($entity, $form_clone, $form_state);

    $this->configuration['uid'] = $entity->uid->getValue();
  }

  /**
   * {@inheritdoc}
   */
  public function execute($entity = NULL)
  {
    if ($entity->hasField('uid')) {
      $entity->set('uid', $this->configuration['uid']);
      $entity->save();
    }
    return $this->t('Stores updated');
  }

  /**
   * {@inheritdoc}
   */
  public function access($object, AccountInterface $account = NULL, $return_as_object = FALSE) {
    if ($object->getEntityType() === 'commerce_store') {
      $access = $object->access('update', $account, TRUE)
        ->andIf($object->status->access('edit', $account, TRUE));
      return $return_as_object ? $access : $access->isAllowed();
    }

    return TRUE;
  }

}