<?php

namespace Drupal\rg_store\Plugin\Action;

use Drupal\Core\Entity\Entity\EntityFormDisplay;
use Drupal\Core\Form\FormStateInterface;
use Drupal\views_bulk_operations\Action\ViewsBulkOperationsActionBase;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Delete products from stores.
 *
 * @Action(
 *   id = "rg_store_delete_products_action",
 *   label = @Translation("Delete products"),
 *   type = ""
 * )
 */
class DeleteProductsAction extends ViewsBulkOperationsActionBase {

  use StringTranslationTrait;

  /**
   * @param array $form
   * @param FormStateInterface $form_state
   * @return array
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $entity_type_id = 'commerce_store';
    $bundle = 'brand_store';
    $entityType = \Drupal::entityTypeManager()->getDefinition($entity_type_id);
    $entity = \Drupal::entityTypeManager()->getStorage($entity_type_id)->create([
      $entityType->getKey('bundle') => $bundle,
    ]);
    $form_display = EntityFormDisplay::collectRenderDisplay($entity, 'vbo_add_products');
    $form_display->buildForm($entity, $form, $form_state);
    unset($form["uid"]);
    return $form;
  }

  /**
   * @param array $form
   * @param FormStateInterface $form_state
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state)
  {
    $form_clone = $form;
    $entity_type_id = 'commerce_store';
    $bundle = 'brand_store';
    $entityType = \Drupal::entityTypeManager()->getDefinition($entity_type_id);
    $entity = \Drupal::entityTypeManager()->getStorage($entity_type_id)->create([
      $entityType->getKey('bundle') => $bundle,
    ]);
    $form_display = EntityFormDisplay::collectRenderDisplay($entity, 'bulk_edit');
    $form_display->extractFormValues($entity, $form_clone, $form_state);

    $this->configuration['field_products'] = $entity->field_products->getValue();
  }

  /**
   * {@inheritdoc}
   */
  public function execute($entity = NULL)
  {
    if ($entity->hasField('field_products')) {
      $last_values = $entity->field_products->getValue();
      $chosen_values = $this->configuration["field_products"];
      $diff = array_udiff($last_values, $chosen_values, array($this, "toLeave"));
      $entity->set('field_products', $diff);
      $entity->save();
    }
    return $this->t('Stores updated');
  }

  /**
   * Callback to find which product to leave
   * @param $a
   * @param $b
   * @return int
   */
  public function toLeave($a, $b) {
    if ($a['target_id'] === $b['target_id']) return 0;
    return ($a['target_id'] > $b['target_id'])? 1:-1;
  }

  /**
   * {@inheritdoc}
   */
  public function access($object, AccountInterface $account = NULL, $return_as_object = FALSE) {
    if ($object->getEntityType() === 'commerce_store') {
      $access = $object->access('update', $account, TRUE)
        ->andIf($object->status->access('edit', $account, TRUE));
      return $return_as_object ? $access : $access->isAllowed();
    }

    return TRUE;
  }

}