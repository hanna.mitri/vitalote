<?php

namespace Drupal\rg_import_cooker_60\Plugin\migrate\process;

use Drupal\commerce_product\Entity\ProductAttributeValue;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;
use Drupal\taxonomy\Entity\Term;

/**
 * Returns value of input array or JSON string using JSONPath.
 *
 * Example of usage:
 * @code
 * process:
 *   title:
 *     -
 *       plugin: build_cooker_60_title
 *       source: source_field
 * @endcode
 *
 * @MigrateProcessPlugin(
 *   id = "build_cooker_60_title"
 * )
 */
class BuildCooker60Title extends ProcessPluginBase
{
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property)
  {
    $title = [];
    $displaySize = null;
    $size = null;

    $attributes = $row->getSourceProperty('attr');
    foreach ($attributes as $attr) {
      if ($attr['key'] == 'ProductFamily') {
        $title[1] = $attr['value'];
        continue;
      }
      if ($attr['key'] == 'DisplaySize') {
        $displaySize = $attr['value'];
        continue;
      }
      if ($attr['key'] == 'size') {
        $size = $attr['value'];
        continue;
      }
      if ($attr['key'] == 'fuel') {
        $title[3] = $attr['value'];
        continue;
      }
      if ($attr['key'] == 'colour') {
        $title[4] = $attr['value'];
        continue;
      }
      if ($attr['key'] == 'PFINISH') {
        $title[5] = 'with ' . $attr['value'] . ' trim';
        continue;
      }
    }

    $title[1] = '<div class="first-line">' . $title[1];
    $title[2] = $displaySize ?: $size;
    $title[3] .= '</div>';
    $title[4] = '<div class="second-line">' . $title[4];
    $title[6] = '</div>';

    ksort($title);
    $title = implode(' ', $title);

    return $title;
  }
}