<?php

namespace Drupal\rg_product\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\rg_product\ProductHelper;

/**
 * Provides a 'Technical Specs for product variation' Block.
 *
 * @Block(
 *   id = "technical_specs",
 *   admin_label = @Translation("TechnicalSpecs"),
 *   category = @Translation("TechnicalSpecs"),
 * )
 */
class TechnicalSpecs extends BlockBase
{
  /**
   * {@inheritdoc}
   */
  public function build()
  {
    $build = [];

    $variation = ProductHelper::getDefaultVariation();
    if ($variation) {
      $type = $variation->bundle();
      $rows = ProductHelper::getData([$variation], 'technical_specification', $type);
      if ($rows) {
        $build['technical_specification'] = [
          '#theme' => 'product_attributes_table',
          '#rows_flipped' => $rows,
        ];
        $build['#cache']['max-age'] = 0;
      }
    }
    return $build;
  }

}

