<?php

namespace Drupal\rg_product\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\rg_product\ProductHelper;

/**
 * Provides a 'Key features for product variation' Block.
 *
 * @Block(
 *   id = "key_features",
 *   admin_label = @Translation("Key Features"),
 *   category = @Translation("Key Features"),
 * )
 */
class KeyFeatures extends BlockBase
{
  /**
   * {@inheritdoc}
   */
  public function build()
  {
    $build = [];
    $variation = ProductHelper::getDefaultVariation();
    if ($variation) {
      $type = $variation->bundle();
      $rows = ProductHelper::getData([$variation], 'key_features', $type);
      if ($rows) {
        $build['key_features'] = [
          '#theme' => 'product_attributes_table',
          '#rows_flipped' => $rows,
        ];
        $build['#cache']['max-age'] = 0;
      }
    }
    return $build;
  }

}

