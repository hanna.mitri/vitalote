<?php

namespace Drupal\rg_product\Plugin\facets\processor;

use Drupal\facets\FacetInterface;
use Drupal\facets\Processor\BuildProcessorInterface;
use Drupal\facets\Processor\ProcessorPluginBase;

/**
 * Provides a processor for string fields.
 *
 * @FacetsProcessor(
 *   id = "remove_underscores",
 *   label = @Translation("Remove Underscores"),
 *   description = @Translation("Remove Underscore from labels"),
 *   stages = {
 *     "build" = 35
 *   }
 * )
 */
class RemoveUnderscore extends ProcessorPluginBase implements BuildProcessorInterface {

  /**
   * {@inheritdoc}
   */
  public function build(FacetInterface $facet, array $results) {
    $config = $this->getConfiguration();

    /** @var \Drupal\facets\Result\Result $result */
    foreach ($results as $result) {
      $raw = $result->getRawValue();
        $result->setDisplayValue(str_replace('_', ' ', $raw));
    }
    return $results;
  }
}