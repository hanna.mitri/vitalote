<?php

namespace Drupal\rg_product\Plugin\views\argument_validator;

use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Entity\EntityManagerInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\taxonomy\Entity\Term;
use Drupal\taxonomy\Plugin\views\argument_validator\TermName;
use Drupal\views\Plugin\views\argument_validator\Entity;


/**
 * Validates whether an argument is a term name, and if so, converts into the corresponding term ID.
 *
 * @ViewsArgumentValidator(
 *   id = "taxonomy_term_name_into_id_multiple",
 *   title = @Translation("Taxonomy term name as ID (multiple)"),
 *   entity_type = "taxonomy_term"
 * )
 */
class TermNameAsIdMultiple extends Entity {

  /**
   * The taxonomy term storage.
   *
   * @var \Drupal\taxonomy\TermStorageInterface
   */
  protected $termStorage;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, EntityTypeBundleInfoInterface $entity_type_bundle_info = NULL) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $entity_type_manager, $entity_type_bundle_info);
    // Not handling exploding term names.
    $this->multipleCapable = TRUE;
    $this->termStorage = $entity_type_manager->getStorage('taxonomy_term');
  }
  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    $options['transform'] = ['default' => FALSE];

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);

    $form['transform'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Transform dashes in URL to spaces in term name filter values'),
      '#default_value' => $this->options['transform'],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function validateArgument($argument)
  {
    if ($this->multipleCapable && $this->options['multiple']) {
      // At this point only interested in individual IDs no matter what type,
      // just splitting by the allowed delimiters.
      $names = array_filter(preg_split('/[,+ ]/', $argument));
    } elseif ($argument) {
      $names = [$argument];
    }

    $ids = [];

    foreach ($names as $argument) {
      if ($this->options['transform']) {
        $argument = str_replace('-', ' ', $argument);
      }
      // If bundles is set then restrict the loaded terms to the given bundles.
      if (!empty($this->options['bundles'])) {
        $terms = $this->termStorage->loadByProperties(['name' => $argument, 'vid' => $this->options['bundles']]);
      } else {
        $terms = $this->termStorage->loadByProperties(['name' => $argument]);
      }
      if (!$terms) {
          $terms = $this->compareAllTerms($argument, $this->options['bundles']);
          if (!$terms) {
            return FALSE;
          }
      }
      // $terms are already bundle tested but we need to test access control.
      foreach ($terms as $term) {
        if ($this->validateEntity($term)) {
          // We only need one of the terms to be valid, so set the argument to
          // the term ID return TRUE when we find one.
          $ids[] = $term->id();
        }
      }
      if (!$ids) {
        return FALSE;
      }
    }
    $this->argument->argument = implode(' ', $ids);

    return TRUE;
  }

  /**
   * @param $argument
   * @param null $bundle
   * @return array
   * @throws InvalidPluginDefinitionException
   * @throws PluginNotFoundException
   */
  public function compareAllTerms($argument, $bundle = null) : array
  {
    $terms = [];
    if (isset($bundle)) {
      $taxonomies = $this->entityTypeManager->getStorage('taxonomy_term')->loadTree(array_shift($bundle));
      $taxonomies = array_column($taxonomies, 'name');
    } else {
      $taxonomies = $this->entityTypeManager->getStorage('taxonomy_term')->loadMultiple();
      $taxonomies = array_column($taxonomies, 'name');
      $taxonomies = array_column($taxonomies, 'value');
    }
    foreach ($taxonomies as $taxonomy) {
      $newTaxName = strtolower(str_replace('-', ' ', $taxonomy));
      if ($argument === $newTaxName) {
        $terms += $this->termStorage->loadByProperties(['name' => $taxonomy]);
      }
    }
    return $terms;
  }

}
