<?php

namespace Drupal\rg_product\EventSubscriber;

use Drupal\commerce_product\Event\ProductEvents;
use Drupal\commerce_product\Event\ProductVariationAjaxChangeEvent;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Ajax\InvokeCommand;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Drupal\rg_product\ProductHelper;

/**
 * Class CommerceAjaxSubscriber.
 */
class CommerceSubscriber implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  static function getSubscribedEvents() {
    return [
      ProductEvents::PRODUCT_VARIATION_AJAX_CHANGE => ['onResponse', 50],
    ];
  }

  /**
   * Respond to AJAX variation update.
   */
  public function onResponse(ProductVariationAjaxChangeEvent $event) {
    $product_variation = $event->getProductVariation();
    $response = $event->getResponse();
    $bundle = $product_variation->bundle();

    $keyFeatures = ProductHelper::getDisplayFields('key_features', $bundle) ?: [];
    $techSpecs = ProductHelper::getDisplayFields('technical_specification', $bundle) ?: [];
    $activeFields = array_merge($keyFeatures, $techSpecs);
    foreach ($activeFields as $field_name => $options) {
      if ($product_variation->hasField($field_name)) {

        $options["label"] = 'hidden';
        if ($options["type"] == 'boolean') {
          $options["settings"]["format"] = 'custom';
          $options["settings"]["format_custom_false"] = '<i class="fa fa-minus"></i>';
          $options["settings"]["format_custom_true"] = '<i class="fa fa-check"></i>';
        }
        $field_value = $product_variation->$field_name->view($options);
        $response->addCommand(new HtmlCommand(".field-value-$field_name", $field_value));
      }
    }
    //here js work to set correct url
    $user_guide_url = ProductHelper::getUserGuide($product_variation);
//    $user_guide_url = '<div class="url-for-href user-guide-url">' . $user_guide_url . '</div>';
    $response->addCommand(new HtmlCommand(".user-guide-url", $user_guide_url));

    $newUrl = $product_variation->toUrl()->toString();
    $response->addCommand(new InvokeCommand(NULL, 'changeUrl', [$newUrl]));

    //for comparison
    $product_variation_id  = $product_variation->get('variation_id')->value;
    $selector = '#comparison-button';
    $data = ProductHelper::changeCompareButton($product_variation_id);
    $response->addCommand(new HtmlCommand($selector, $data));

    //for 'Where to buy' button
    ProductHelper::changeBuyButton($response, $product_variation);
  }
}

