<?php

namespace Drupal\rg_product\Controller;

use Drupal\commerce_product\Entity\ProductVariation;
use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Ajax\AlertCommand;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Ajax\InvokeCommand;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\rg_product\ProductHelper;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class ProductComparisonController
 * @package Drupal\rg_product\Controller
 */
class ProductComparisonController extends ControllerBase
{

  private $cookie_value;

  public function __construct()
  {
    $this->cookie_value = 'variation_id';
  }

  /**
   * show comparison popup
   * @return AjaxResponse
   * @throws InvalidPluginDefinitionException
   * @throws PluginNotFoundException
   */
  public function showComparison()
  {
    $cookie_variation_id = $this->cookie_value;
    isset($_COOKIE[$cookie_variation_id]) ? $cookie = $_COOKIE[$cookie_variation_id] : $cookie = [];

    $ajax = new AjaxResponse();
    $this->comparisonTable($ajax, $cookie);
    $ajax->addCommand(new InvokeCommand('.block-comparisonpage',
      'addClass', ['active-popup']));
    $ajax->addCommand(new InvokeCommand('.comparison-content > table',
      'addClass', ['table']));
    $ajax->addCommand(new InvokeCommand('.comparison-content > table',
      'addClass', ['table-striped']));

    return $ajax;
  }

  /**
   * add product id to cookie and change bottom popup link (increase)
   * @param Request $request
   * @param $id
   * @return AjaxResponse
   */
  public function addToCompare(Request $request, $id)
  {
    $cookie_variation_id = $this->cookie_value;
    $id_arr = [];
    $ajax = new AjaxResponse();
    $cookies = $request->cookies;

    list($selector, $data) = $this->addCompareCheckbox($id);

    if ($cookies->has($cookie_variation_id)) {
      $cookie_arr = unserialize($cookies->get($cookie_variation_id));
      $id_arr = $cookie_arr;

      if (count($cookie_arr) < 4 && !in_array($id, $cookie_arr)) {
        if (!$this->checkBundle($id, $id_arr)) {
          $site_settings = \Drupal::service('site_settings.loader');
          $alert = $site_settings->loadByFieldset('product_comparison')['product_comparison'];
          $ajax->addCommand(new AlertCommand($alert));
        } else {
          $id_arr = $cookie_arr;
          $id_arr[] = $id;
          $ajax->addCommand(new HtmlCommand($selector, $data));
        }
      }

    } else {
      $id_arr[] = $id;
      $ajax->addCommand(new HtmlCommand($selector, $data));
    }

    $cookie = $this->setCookie($id_arr, $ajax);

    $cookieAmount = count(unserialize($cookie->getValue()));

    $this->changeBottomPopupLink($cookieAmount, $ajax);

    $ajax->addCommand(new InvokeCommand('.block-bottomcomparepopup',
      'removeClass', ['disable-popup']));
    $ajax->addCommand(new InvokeCommand('.block-bottomcomparepopup',
      'addClass', ['active-popup']));

    return $ajax;
  }

  /**
   * @param $id
   * @return array
   */
  public function addCompareCheckbox($id): array
  {
    $selector = '.catalog-compare-link-wrapper[data-id="' . $id . '"]';
    $data = "<a href='/compare/remove/" . $id . "' class='use-ajax catalog-compare-link'>
            <span class='catalog-compare-link-text catalog-compare-link-checked'>
                Compare
            </span>
        </a>";

    return [$selector, $data];
  }

  /**
   * Check if variables have the same bundle
   *
   * @param $new_id
   * @param $old_id
   * @return bool
   */
  private function checkBundle($new_id, $old_id)
  {
    if (!empty($old_id)) {
      $new_variation = ProductVariation::load($new_id);
      $old_variation = ProductVariation::load($old_id[0]);
      if ($new_variation && $old_variation) {
        return $new_variation->bundle() == $old_variation->bundle();
      }
    }

    return true;
  }

  /**
   * remove product id from cookie and change bottom popup link (decrease)
   * @param Request $request
   * @param $id
   * @return AjaxResponse
   * @throws InvalidPluginDefinitionException
   * @throws PluginNotFoundException
   */
  public function removeFromCompare(Request $request, $id)
  {
    $cookie_variation_id = $this->cookie_value;
    $ajax = new AjaxResponse();

    if (isset($_COOKIE[$cookie_variation_id]) && in_array($id, unserialize($_COOKIE[$cookie_variation_id]))) {

      $cookie_arr = unserialize($request->cookies->get($cookie_variation_id));
      $cookie_arr = array_diff($cookie_arr, [$id]);
      $cookie = $this->setCookie($cookie_arr, $ajax);

      list($selector, $data) = $this->removeCompareCheckbox($id);

      $cookie_value = $cookie->getValue();

      $this->comparisonTable($ajax, $cookie_value);

      $cookieAmount = count(unserialize($cookie_value));

      $this->changeBottomPopupLink($cookieAmount, $ajax);

      if ($cookieAmount < 1) {
        $ajax->addCommand(new InvokeCommand('.block-bottomcomparepopup',
          'addClass', ['disable-popup']));
        $ajax->addCommand(new InvokeCommand('.block-comparisonpage',
          'removeClass', ['active-popup']));
      }

      $ajax->addCommand(new HtmlCommand($selector, $data));
    }

    return $ajax;
  }

  /**
   * @param $id
   * @return array
   */
  public function removeCompareCheckbox($id): array
  {
    $selector = '.catalog-compare-link-wrapper[data-id="' . $id . '"]';
    $data = "<a href='/compare/add/" . $id . "' class='use-ajax catalog-compare-link'>
                <span class='catalog-compare-link-text catalog-compare-link-unchecked'>
                    Compare
                </span>
            </a>";

    return [$selector, $data];
  }

  /**
   * for close popup
   * @return AjaxResponse
   */
  public function closeComparison()
  {
    $ajax = new AjaxResponse();
    $ajax->addCommand(new InvokeCommand('.block-comparisonpage',
      'removeClass', ['active-popup']));
    return $ajax;
  }

  /**
   * for adding product id to cookie
   * @param $id_arr
   * @param $ajax
   * @return Cookie
   */
  public function setCookie(array $id_arr, $ajax)
  {
    $cookie_variation_id = $this->cookie_value;

    $cookie = new Cookie($cookie_variation_id, serialize($id_arr),
      0, '/', NULL, FALSE);
    $ajax->headers->setCookie($cookie);

    return $cookie;
  }

  /**
   * for changing bottom popup links
   * @param $cookieAmount
   * @param $ajax
   * @return mixed
   */
  public function changeBottomPopupLink($cookieAmount, $ajax)
  {
    $compareButton = "<button href='/comparison' 
                class='use-ajax btn btn-light link-to-comparison link-to-comparison-active'>
            Compare products (" . $cookieAmount . " of 4)</button>
                      <a href='/clearAllProductComparisons' class='bottom-compare-clear-all-comparison use-ajax'>
                        Clear All
                      </a>";
    $selectAnotherButton =
      "<button class='btn btn-light disabled link-to-comparison link-to-comparison-disabled'>
                Select another product</button>
                <a href='/clearAllProductComparisons' class='bottom-compare-clear-all-comparison use-ajax'>
                  Clear All
                </a>";

    if ($cookieAmount > 1) {
      $ajax->addCommand(new HtmlCommand('.bottom-compare-text',
        "$compareButton"));
    } else {
      $ajax->addCommand(new HtmlCommand('.bottom-compare-text',
        "$selectAnotherButton"));
    }

    return $ajax;
  }

  /**
   * Create comparison table (build this table)
   *
   * @param $ajax
   * @param $cookie
   * @return mixed
   * @throws InvalidPluginDefinitionException
   * @throws PluginNotFoundException
   */
  public function comparisonTable($ajax, $cookie)
  {
    isset($cookie) ? $cookie_variation_id = unserialize($cookie) : $cookie_variation_id = [];

    $variations = ProductVariation::loadMultiple($cookie_variation_id);

    if ($variations) {
      $variation = [];
      $data[0] = [];
      $i = 1;
      foreach ($variations as $value) {
        $variation[] = $value;
        $data[$i]['variation_id'] = $value->get('variation_id')->value;
        $data[$i]['url'] = $value->toUrl();
        $data[$i]['title'] = $value->getTitle();
        $image = [
          '#theme' => 'imagecache_external',
          '#uri' => $value->get("field_dam_thumb")[0]->value,
          '#style_name' => 'mini_product',
          '#alt' => $value->getTitle(),
        ];
        $data[$i]['image'] = render($image);
        $i++;
      }

      $row_flipped_key_features = ProductHelper::getData($variation, 'key_features', $value->bundle());
      $row_flipped_tec_spec = ProductHelper::getData($variation, 'technical_specification', $value->bundle());

      $build['key_features'] = [
        '#theme' => 'product_attributes_table',
        '#rows_flipped' => $row_flipped_key_features,
        '#data' => $data,
      ];

      $build['technical_specification'] = [
        '#theme' => 'product_attributes_table',
        '#rows_flipped' => $row_flipped_tec_spec,
      ];
    }
    $build['#cache']['max-age'] = 0;

    $ajax->addCommand(new HtmlCommand('.comparison-content', $build));

    return $ajax;
  }

  /**
   * Close bottom popup (clear all appropriate cookies)
   *
   * @return AjaxResponse
   */
  public function clearAllProductComparisons()
  {
    $cookie_variation_id = $this->cookie_value;
    isset($_COOKIE[$cookie_variation_id]) ? $cookie = $_COOKIE[$cookie_variation_id] : $cookie = [];
    $cookie_arr = unserialize($cookie);

    $ajax = new AjaxResponse();
    foreach ($cookie_arr as $value) {
      list($selector, $data) = $this->removeCompareCheckbox($value);
      $ajax->addCommand(new HtmlCommand($selector, $data));
    }

    $this->setCookie([], $ajax);

    $ajax->addCommand(new InvokeCommand('.block-bottomcomparepopup',
      'addClass', ['disable-popup']));
    $ajax->addCommand(new InvokeCommand('.block-comparisonpage',
      'removeClass', ['active-popup']));

    return $ajax;
  }
}
