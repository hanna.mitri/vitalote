<?php
/**
 * Created by PhpStorm.
 * User: ginger
 * Date: 10.03.21
 * Time: 14:33
 */

namespace Drupal\rg_product\Controller;

use Drupal\commerce_product\Entity\ProductVariation;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\AppendCommand;
use Drupal\Core\Ajax\RemoveCommand;
use Drupal\Core\Controller\ControllerBase;
use GuzzleHttp\Exception\RequestException;

class WhereToBuyController extends ControllerBase
{

  /**
   * Performs call to API and generates popup template
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   * @param $id
   * @return null
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function whereToBuy(\Symfony\Component\HttpFoundation\Request $request, $id) {

    $ajax = new AjaxResponse();
    $variation = ProductVariation::load($id);
    $sku = $variation->getSku();

    $response = $this->callToApi($sku);

    $where_buy_popup = [
      '#theme' => 'where_buy_popup',
      '#where_buy_items' => $response['d']
    ];

    $ajax->addCommand(new AppendCommand('.right-product .text-center', $where_buy_popup));

    return $ajax;
  }

  /**
   * @param $sku
   * @return mixed|null
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function callToApi($sku) {

    $request_url =
      'http://services.internetbuyer.co.uk/REST.svc/GetByProductPartNumber?ProductPartNumber=%27' .
      $sku .
      '%27&CID=211&$format=json';

    try {
      $result = \Drupal\Component\Serialization\Json::decode(\Drupal::httpClient()->request('GET', $request_url)->getBody());
    } catch (RequestException $e) {
      return null;
    }

    return $result;
  }


  /**
   * @return AjaxResponse
   */
  public function whereToBuyClosePopup() {

    $ajax = new AjaxResponse();
    $ajax->addCommand(new RemoveCommand('.product-where-buy-popup-wrapper'));
    return $ajax;
  }

}