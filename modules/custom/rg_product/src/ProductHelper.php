<?php

namespace Drupal\rg_product;

use Drupal\commerce_product\Entity\ProductInterface;
use Drupal\commerce_product\Entity\ProductVariation;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Ajax\RemoveCommand;
use Drupal\Core\Database\Database;
use Drupal\node\Entity\Node;
use Drupal\paragraphs\Entity\Paragraph;

class ProductHelper
{

  /**
   * @return mixed all products by filters
   */
  public static function getAvailableVariations(
    $category = null,
    $collection = null,
    $size = null,
    $fuel = null,
    $color = null,
    $no_of_bowls = null
  )
  {
    $connection = Database::getConnection();
    $query = $connection->select('commerce_product_variation_field_data', 'v');
    if ($category) {
      $query->join('commerce_product_variation__field_category', 'category', 'v.variation_id = category.entity_id');
      $query->condition('category.field_category_target_id', $category);
    }
    if ($collection) {
      //to get real table name of field
      $entity_manager = \Drupal::getContainer()->get('entity.manager');
      $storage_definitions = $entity_manager->getFieldStorageDefinitions('commerce_product_variation');
      $table_mapping = $entity_manager->getStorage('commerce_product_variation')->getTableMapping();
      $collection_table = $table_mapping->getDedicatedDataTableName($storage_definitions['field_range_cooker_collection']);

      $query->join($collection_table, 'collection', 'v.variation_id = collection.entity_id');
      if (is_array($collection)) {
        $query->condition('collection.field_range_cooker_collection_target_id', $collection, 'IN');
      } else {
        $query->condition('collection.field_range_cooker_collection_target_id', $collection);
      }
    }
    if ($size) {
      $query->join('commerce_product_variation__attribute_size', 'size', 'v.variation_id = size.entity_id');
      $query->condition('size.attribute_size_target_id', $size);
    }
    if ($fuel) {
      $query->join('commerce_product_variation__attribute_fuel', 'fuel', 'v.variation_id = fuel.entity_id');
      $query->condition('fuel.attribute_fuel_target_id', $fuel);
    }
    if ($color) {
      $query->join('commerce_product_variation__attribute_color', 'color', 'v.variation_id = color.entity_id');
      $query->condition('color.attribute_color_target_id', $color);
    }
    if ($no_of_bowls) {
      $query->join('commerce_product_variation__attribute_no_of_bowls', 'no_of_bowls', 'v.variation_id = no_of_bowls.entity_id');
      $query->condition('no_of_bowls.attribute_no_of_bowls_target_id', $no_of_bowls);
    }
    $query->condition('v.status', 1);

    $query->fields('v', ['variation_id']);
    $data = $query->distinct()->execute();
    return $data->fetchCol();
  }

  /**
   * Return data for key features table
   *
   * @param $variations
   * @param $display_name
   * @param $type
   * @return array
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public static function getData($variations, $display_name, $type) {
    $rowsFlipped = [];
    if (isset($variations[0]) && $type) {
      /** @var \Drupal\Core\Entity\Entity\EntityViewDisplay $display */
      $display = \Drupal::entityTypeManager()
        ->getStorage('entity_view_display')
        ->load('commerce_product_variation' . '.' . $type . '.' . $display_name);
      $activeFields = $display->getComponents();
      if (!$activeFields) { return []; }
      uasort($activeFields, "self::weightSort");
      $fields = $display->get('fieldDefinitions');
      foreach ($activeFields as $name => $options) {
        if (isset($fields[$name])) {
          $field = $fields[$name];
          $options["label"] = 'hidden';
          $rowsFlipped[$name]['header']['label'] = ['#markup' => $field->getLabel()];
          if ($description = $field->getDescription()) {
            $rowsFlipped[$name]['header']['description'] = ['#markup' => $description];
          }
          if ($options["type"] && $options["type"] == 'boolean') {
            $options["settings"]["format"] = 'custom';
            $options["settings"]["format_custom_false"] = '<i class="fa fa-minus"></i>';
            $options["settings"]["format_custom_true"] = '<i class="fa fa-check"></i>';
          }
          foreach ($variations as $variation) {
            if (self::ifDisplayField($variation, $options["type"], $name)) {
              $rowsFlipped[$name]['values'][] = $variation->$name->view($options);
            }
            else {
              $rowsFlipped[$name]['values'][] = '-';
            }
          }
        }
      }

      foreach ($rowsFlipped as $rowName => $rowValues) {
        if (count(array_keys($rowValues['values'], '-')) === count($rowValues['values'])) {
          unset($rowsFlipped[$rowName]);
        }
      }
    }
    return $rowsFlipped;
  }

  /**
   * To hide fields with no relevant values "no" 0.
   * @param $variation
   * @param $type
   * @param $name
   * @return bool
   */
  public static function ifDisplayField($variation, $type, $name) {
    if (!$variation->$name->isEmpty()) {
      $displayField = FALSE;

      if ($type == 'string' || $type == 'boolean' || $type == 'number_decimal'  || $type == 'number_unformatted') {
        $value = $variation->$name->getValue();
        $value = $value[0]["value"];
        $displayField = strtolower($value) == 'no' || $value == '0';
      }
      return !$displayField;
    }
    else {
      return FALSE;
    }
  }

  /**
   * @param $displayName
   * @return mixed
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public static function getDisplayFields($displayName, $type)
  {
    $display = \Drupal::entityTypeManager()
      ->getStorage('entity_view_display')
      ->load('commerce_product_variation' . '.' . $type . '.' . $displayName);
    return $display->getComponents();
  }

  /**
   * Get default variation from product page
   * @return |null
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public static function getDefaultVariation()
  {
    $current_route = \Drupal::routeMatch();
    foreach ($current_route->getParameters() as $param) {
      if ($param instanceof ProductInterface) {
        $product = $param;

        $variation_storage = \Drupal::entityTypeManager()->getStorage('commerce_product_variation');
        $variation = $variation_storage->loadFromContext($product);
        return $variation;
      }
    }
    return null;
  }

  public static function changeCompareButton($variation_id)
  {
    $cookie_variation_id = 'variation_id';
    if (isset($_COOKIE[$cookie_variation_id]) && in_array($variation_id, unserialize($_COOKIE[$cookie_variation_id]))) {
      $data = "<div class='catalog-compare-link-wrapper' data-id=" . $variation_id . ">
        <a href='/compare/remove/" . $variation_id . "' class='use-ajax catalog-compare-link'>
                    <span class='catalog-compare-link-text catalog-compare-link-checked'>
                        Compare
                    </span>
                </a>
                </div>";
    } else {
      $data = "<div class='catalog-compare-link-wrapper' data-id=" . $variation_id . ">
        <a href='/compare/add/" . $variation_id . "' class='use-ajax catalog-compare-link'>
                    <span class='catalog-compare-link-text catalog-compare-link-unchecked'>
                        Compare
                    </span>
                </a>
                </div>";
    }
    return $data;
  }


  /**
   * just sort fields by weight
   * @param $a
   * @param $b
   * @return int
   */
  public static function weightSort($a, $b) {
    if ($a["weight"] == $b['weight']) {
      return 0;
    }
    return ($a['weight'] < $b['weight']) ? -1 : 1;
  }


  /**
   * @param $variation
   * @return string|null
   */
  public static function getUserGuide($variation) {
    $variation_id = $variation->get('variation_id')->value;

    $connection = Database::getConnection();
    $query = $connection->select('node__field_products', 'gp');
    $query->fields('gp', ['entity_id']);
    $query->condition('gp.field_products_target_id', $variation_id);
    $query->condition('gp.bundle', 'user_guide');
    $data = $query->distinct()->execute();

    $results = $data->fetchCol();
    if ($results) {
      $node = Node::load($results[0]);
      if ($node && !$node->get('field_files')->isEmpty()) {
        $paragraphs = $node->field_files->getValue();
        $paragraph_id = array_pop($paragraphs);
        $paragraph = Paragraph::load($paragraph_id['target_id']);
        if (!$paragraph->get('field_pdf')->isEmpty()) {
          $uri = $paragraph->field_pdf->entity->getFileUri();
          $url = file_create_url($uri);
          return $url;
        }
      }
    }
    else {
      return null;
    }
  }

  /**
   * find gallery for product
   *
   * @param $variation
   * @return array
   */
  public static function getProductGallery($variation) {

    $variation_id = $variation->get('variation_id')->value;
    $nodes = null;
    $connection = Database::getConnection();
    $query = $connection->select('node__field_products', 'gp');
    $query->fields('gp', ['entity_id']);
    $query->condition('gp.field_products_target_id', $variation_id);
    $query->condition('gp.bundle', 'product_gallery');
    $data = $query->distinct()->execute();

    $results = $data->fetchCol();
    if ($results) {
      $nodes = Node::loadMultiple($results);
    }

    return [
      '#theme' => 'gallery',
      '#nodes' => $nodes
    ];
  }

  /**
   * @param AjaxResponse $response
   * @param ProductVariation $variation
   */
  public static function changeBuyButton(AjaxResponse $response, ProductVariation $variation) {

    $ecom_label = $variation->get('ecomlabel')->value;
    if ($ecom_label) {

      $variation_id = $variation->id();
      $buy_button = [
        '#markup' =>
          '<a href="/product-where-buy/' . $variation_id . '" class="product-where-buy-button use-ajax">' . $ecom_label . '</a>'
      ];

      $response->addCommand(new HtmlCommand('.right-product .text-center', $buy_button));
    } else {

      $response->addCommand(new RemoveCommand('.right-product .text-center .product-where-buy-button'));
    }
  }

}