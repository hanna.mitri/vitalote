<?php

use Drupal\commerce_product\Entity\Product;
use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Component\Render\MarkupInterface;
use Drupal\rg_product\ProductHelper;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Markup;
use Drupal\views\ViewExecutable;


/**
 * Implements hook_preprocess_HOOK() for catalog view's fields.
 * @param $vars
 */
function rg_product_preprocess_views_view_fields(&$vars)
{
  $cookie_variation_id = 'variation_id';

  $view = $vars['view'];
  if (isset($view) && $view->id() == 'catalog' && $view->current_display == 'page_1') {

    $viewField = array_shift($view->result);
    $fieldValue = $viewField->_object->getValue();
    $variation_id = $fieldValue->variation_id->value;

    if (isset($_COOKIE[$cookie_variation_id])) {
      $vars['#cache']['max-age'] = 0;
      $cookie = unserialize($_COOKIE[$cookie_variation_id]);

      if (in_array($variation_id, $cookie)) {
        $vars["fields"]["nothing"]->content = Markup::create("
        <div class='catalog-compare-link-wrapper' data-id='" . $variation_id . "'>
            <a href='/compare/remove/" . $variation_id . "' class='use-ajax catalog-compare-link' >
                <span class='catalog-compare-link-text catalog-compare-link-checked'>
                    Compare
                </span>
            </a>
        </div>
        ");
      }
    }
  }
}

/**
 * Implements hook_preprocess_HOOK() for catalog view's bottom popup block.
 * @param $variables
 */
function rg_product_preprocess_block__bottomcomparepopup(&$variables)
{
  $cookie_variation_id = 'variation_id';

  $variables['#cache']['max-age'] = 0;
  isset($_COOKIE[$cookie_variation_id]) ? $cookie = unserialize($_COOKIE[$cookie_variation_id]) : $cookie = [];
  if (($cookie)) {

    $cookie_amount = count($cookie);
    $variables['attributes']['class'][] = 'active-popup';

    if ($cookie_amount > 1) {
      $variables['content']['body'][0]['#text'] =
        bottomComparePopupLink(
          'href="/comparison"',
          'class="use-ajax btn btn-light link-to-comparison link-to-comparison-active"',
          'Compare products (' . $cookie_amount . ' of 4)');
    } else {
      $variables['content']['body'][0]['#text'] =
        bottomComparePopupLink(
          '',
          'class="btn btn-light disabled link-to-comparison link-to-comparison-disabled"',
          'Select another product');

    }
  }
}

/**
 * for creating link to comparison popup
 * @param string $link
 * @param $class
 * @param $text
 * @return MarkupInterface|string
 */
function bottomComparePopupLink($link = '', $class, $text)
{
  $bottomPopup = Markup::create('
                <wrapper id="bottom_compare_popup">
                    <div class="col bottom-compare-text">
                        <button ' . $link . ' ' . $class . '>
                                ' . $text . '
                        </button>
                        <a href="/clearAllProductComparisons" class="bottom-compare-clear-all-comparison use-ajax">
                          Clear All
                        </a>
                    </div>
                </wrapper>');

  return $bottomPopup;
}

/**
 * Add image to attributes filter (size, design_styles...) in add to cart form
 *
 * @param $form
 * @param FormStateInterface $form_state
 * @param $form_id
 */
function rg_product_form_alter(&$form, FormStateInterface $form_state, $form_id)
{
  //add_to_cart_form
  if (stristr($form_id, "add_to_cart_form")) {
    $form['actions']['submit']['#disabled'] = true;
    $form['actions']['submit']['#value'] = t('Buy now');

    if (isset($form["purchased_entity"]["widget"][0]["attributes"]["attribute_fuel"])) {
      asort($form["purchased_entity"]["widget"][0]["attributes"]["attribute_fuel"]["#options"]);
    }
    if (isset($form["purchased_entity"]["widget"][0]["attributes"]["attribute_color"])) {
      asort($form["purchased_entity"]["widget"][0]["attributes"]["attribute_color"]["#options"]);
    }
    if (isset($form["purchased_entity"]["widget"][0]["attributes"]["attribute_trim"])) {
      asort($form["purchased_entity"]["widget"][0]["attributes"]["attribute_trim"]["#options"]);
    }

    // List of the attributes to add the images
    $attributes = [
      'size',
      'design_style',
      'number_of_programs'
    ];

    foreach ($attributes as $attribute) {
      if (isset($form["purchased_entity"]["widget"][0]["attributes"]["attribute_$attribute"])) {
        asort($form["purchased_entity"]["widget"][0]["attributes"]["attribute_$attribute"]["#options"]);
        $storage = $form_state->getStorage();
        $attribute_option = &$form["purchased_entity"]["widget"][0]["attributes"]["attribute_$attribute"]["#options"];

        foreach ($attribute_option as $key => $option) {
          $query = \Drupal::entityQuery('commerce_product_variation')
            ->condition('product_id', $storage["product"]->id())
            ->condition("attribute_$attribute", $key)
            ->condition('status', true)
            ->range(0, 1);
          if (isset($form["purchased_entity"]["widget"][0]["attributes"]["attribute_color"]))
          {
            $color = $form["purchased_entity"]["widget"][0]["attributes"]["attribute_color"]["#default_value"];
            $query->condition('attribute_color', $color);
          }
          $vid = $query->execute();
          if ($vid) {
            $variation = \Drupal::entityTypeManager()->getStorage('commerce_product_variation')->load(array_pop($vid));
            $image = $variation->field_dam_image->getValue();
            if ($image && $image = $image[0]["value"]) {
              $variationImageUrl = array(
                '#theme' => 'imagecache_external',
                '#uri' => $image,
                '#style_name' => 'small_icon',
                '#alt' => 'Druplicon',
              );
              $attribute_option[$key] = render($variationImageUrl). "<p class='$attribute-text field--name-name'>" . $option . '</p>';
            }}
        }
      }
    }
  }

  // edit variation form
  if (stristr($form_id, "commerce_product_variation")
    && stristr($form_id, "_edit_form")) {
    $form['actions']['#weight'] = 500;
  }

}


/**
 * Implements hook_theme().
 * @return array
 */
function rg_product_theme()
{
  return [
    'product_attributes_table' => [
      'variables' => [
        'rows_flipped' => null,
        'data' => null,
      ],
    ],

    'where_buy_popup' => [
      'variables' => [
        'where_buy_items' => null,
      ],
    ],
  ];
}

/**
 * Implements hook_views_pre_view().
 * @param ViewExecutable $view
 * @param $display_id
 * @param array $args
 */
function rg_product_views_pre_view(ViewExecutable $view, $display_id, array &$args)
{
  // This is custom dynamic filter for banner on product page
  if ($view->id() == 'collection_banner' && $display_id == 'block_2') {
    $current_path = Drupal::service('path.current')->getPath();

    $current_path = explode('/', $current_path);
    if ($current_path[1] == 'product' && is_numeric($current_path[2])) {
      $product = Product::load($current_path[2]);
      if ($product instanceof Product) {
        $collectionId = $product->get('field_range_cooker_collection')->getValue();
      }
      if (!empty($collectionId[0]["target_id"])) {
        $args[0] = (integer)$collectionId[0]["target_id"];
      }
    }
  }
}

/**
 * Implements hook_views_pre_render().
 * @param ViewExecutable $view
 */
function rg_product_views_pre_render(ViewExecutable $view)
{
  if ($view->id() == 'catalog' && $view->current_display == 'page_1') {
    $title = $view->getTitle();
    $arguments = explode(', ', $title);
    $new_title = array_unique($arguments);
    $title = implode(', ', $new_title);
    $view->setTitle($title);
  }
}

/**
 * Implements hook_preprocess_commerce_product().
 * @param $variables
 * @throws InvalidPluginDefinitionException
 * @throws PluginNotFoundException
 */
function rg_product_preprocess_commerce_product(&$variables)
{
  $block_manager = Drupal::service('plugin.manager.block');
  $variables['#attached']['library'][] = 'corporateplus/product-page';
  $config = Drupal::config('rg_additional_form_fields.settings');

  $variation = ProductHelper::getDefaultVariation();
  if ($variation) {
    $bundle = $variation->bundle();
    $variables['accordion_text'] = Markup::create($config->get("product_page.$bundle"));
    $variables['gallery'] = ProductHelper::getProductGallery($variation);
    $variables['user_guide_url'] = ProductHelper::getUserGuide($variation);
    $variables['variation_ecom_label'] = $variation->get('ecomlabel')->value;
    //get product variation default id
    $variables['variation_id'] = $variation->get('variation_id')->value;
  }

  //key features
  $plugin_block = $block_manager->createInstance('key_features', []);
  $key_features = $plugin_block->build();
  if ($key_features) {
    $variables['key_features_block'] = $key_features;
    $variables['key_features_title'] = Markup::create($config->get('product_page.accordion_titles.key_features'));
  }
  //technical specs
  $plugin_block = $block_manager->createInstance('technical_specs', []);
  $render = $plugin_block->build();
  if ($render) {
    $variables['technical_specs_block'] = $render;
    $variables['technical_specs_title'] = Markup::create($config->get('product_page.accordion_titles.technical_specs'));
  }

  $variables['energy_rating'] = Markup::create($config->get('product_page.accordion_titles.energy_rating'));
  if ($bundle === 'sink' || $bundle === 'tap') {
    $variables['data_sheet'] = Markup::create($config->get('product_page.accordion_titles.data_sheet'));
  } else {
    $variables['product_fiche'] = Markup::create($config->get('product_page.accordion_titles.product_fiche'));
  }
  $variables['user_guide'] = Markup::create($config->get('product_page.accordion_titles.user_guide'));
  //replace bottom block title
  $bottom_text = $config->get('product_page.bottom_block');
  $title = $variables["elements"]["#commerce_product"]->get('title')->value;
  $bottom_text = str_replace('[current-page:title]', $title, $bottom_text);
  $variables['bottom_text'] = Markup::create($bottom_text);

  //get product id
  $product_id = $variables["elements"]["#commerce_product"]->get('product_id')->value;
  $variables["product"]["id"] = $product_id;
  $variables['subject'] = $variables["elements"]["#commerce_product"]->get('title')->value;

  //change compare link
  $cookie_variation_id = 'variation_id';
  if (isset($_COOKIE[$cookie_variation_id]) && in_array($variables['variation_id'], unserialize($_COOKIE[$cookie_variation_id]))) {
    $variables["product"]["compare_href"] = '/compare/remove/';
    $variables["product"]["compare_class"] = 'catalog-compare-link-checked';
  }
}
