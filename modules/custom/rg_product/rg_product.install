<?php

use Drupal\commerce_product\Entity\ProductVariationType;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;

/**
 * Change field type to 'String' for list of fields
 */
function rg_product_update_8001(&$sandbox)
{
  $entity_type = 'commerce_product_variation';
  $field_names = [];

  $fields = [
    'plugfitted',
    'proinsreq',
    'saffeachiloc',
    'saffeaovedet',
    'multiringwokburner',
    'brassburners',
    'hothobindicators',
    'powerboostinduction',
    'rapidresponse',
    'energysavingpanel',
    'funtypnooffun',
    'furrigshesup',
    'furshesup',
    'control',
    'shelves',
    'shelfrunners',
    'hobburners',
    'pansupports',
    'drawertype',
    'runnerroller',
    'mat',
    'furniturehandy',
    'fishefuel1',
    'fishefuel2',
    'fishefuel3',
    'fishefuel4',
    'fishefuel5',
    'fishefuel6',
    'fishefuel7',
    'sinrevdrapos',
    'sininccomwaskit',
    'sineascle',
    'sinmicshefin',
    'sinsubgroedgfin',
    'sinprefitcliandsea',
    'sinincsoudeapad',
    'sinhiggra1810staste',
    'sin10mcorrad',
    'sin25mcorrad',
    'sin65mcorrad',
    'sinhigresagascr',
    'sinhigresagahea',
    'sinhigresagarus',
    'sinhigresagasta',
    'sinnatantsur',
    'sinuvsta',
    'sincomsiz',
    'sinundins',
    'tapwraapp',
    'tapantsplspo',
    'tappuloutspr',
    'tapcerdiscar',
    'tapquaturhan',
    'tapfleinlpip',
    'tapcopinlpip',
    'taphonaer',
    'tapflostr',
  ];

  if (!isset($sandbox['total'])) {
    $sandbox['total'] = count($fields);
    $sandbox['current'] = 0;
    $sandbox['messages'] = [];
  }

  $items_per_batch = 1;

  $fields_range['start'] = $sandbox['current'];
  $fields_range['finish'] = $sandbox['current'] + $items_per_batch;
  if ($fields_range['finish'] > $sandbox['total']) {
    $fields_range['finish'] = $sandbox['total'];
  }

  for($i = $fields_range['start']; $i < $fields_range['finish']; $i++) {

    $new_fields_list = [];
    $field_name = $fields[$sandbox['current']];
    $field_names[] = $field_name;
    $field_storage = FieldStorageConfig::loadByName($entity_type, $field_name);

    $sandbox['current']++;

    if (is_null($field_storage)) {
      continue;
    }
    $type = $field_storage->getType();

    if ($type === 'string') {
      continue;
    }

    $weights = _rg_product_get_field_weight($entity_type, $field_name);

    // Use existing field config for new field.
    foreach ($field_storage->getBundles() as $bundle => $label) {
      $field = FieldConfig::loadByName($entity_type, $bundle, $field_name);
      if ($field) {
        $new_field = $field->toArray();
        $new_field['field_type'] = 'string';
        $new_field['settings'] = [
          'max_length' => 255,
        ];
        $new_fields_list[] = $new_field;
      }
    }

    // Deleting field storage which will also delete bundles(fields).
    $new_field_storage = $field_storage->toArray();
    $new_field_storage['type'] = 'string';
    $new_field_storage['settings'] = [
      'max_length' => 255,
    ];

    _rg_product_delete_field($field_storage);

    // Create new field storage.
    $new_field_storage = FieldStorageConfig::create($new_field_storage);
    $new_field_storage->save();

    // Create new fields.
    foreach ($new_fields_list as $nfield) {
      $new_field_config = FieldConfig::create($nfield);
      $new_field_config->save();
    }

    _rg_product_change_field_settings($weights, $field_name);
  }

  $field_name_string = implode(', ', $field_names);

  $messenger = \Drupal::service('messenger');
  $messenger->addStatus(t("'$field_name_string' field processed") );

  if ($sandbox['total'] == 0) {
    $sandbox['#finished'] = 1;
  } else {
    $sandbox['#finished'] = ($sandbox['current'] / $sandbox['total']);
  }

  if (function_exists('drush_print')) {
    drush_print('Progress: ' . (round($sandbox['#finished'] * 100)) . '% (' .
      $sandbox['current'] . ' of ' . $sandbox['total'] . ' fields processed)');
  }

  return t('The fields were changed successfully');
}

/**
 * @param $field_storage
 */
function _rg_product_delete_field($field_storage) {

  field_purge_batch(500);
  $field_storage->delete();

  // Purge field data now to allow new field and field_storage with same name
  // to be created.
  field_purge_batch(500);
}

/**
 * @param $entity_type
 * @param $field_name
 * @return array
 */
function _rg_product_get_field_weight($entity_type, $field_name) {

  $weights = [];
  $bundles = ProductVariationType::loadMultiple();

  foreach (array_keys($bundles) as $bundle) {
    // Get selected view modes for bundle
    $view_modes = \Drupal::service('entity_display.repository')
      ->getViewModeOptionsByBundle($entity_type, $bundle);
    // Save the weights in view and form modes
    foreach (array_keys($view_modes) as $view_mode) {
      $view_display = \Drupal::entityTypeManager()
        ->getStorage('entity_view_display')
        ->load($entity_type . '.' . $bundle . '.' . $view_mode)
        ->getComponent($field_name);
      if ($view_display) {
        $weights['entity_view_display'][$entity_type . '.' . $bundle . '.' . $view_mode] = $view_display['weight'];
      }
    }
    $form_display = \Drupal::entityTypeManager()
      ->getStorage('entity_form_display')
      ->load($entity_type . '.' . $bundle . '.default')
      ->getComponent($field_name);
    if ($form_display) {
      $weights['entity_form_display'][$entity_type . '.' . $bundle . '.default'] = $form_display['weight'];
    }
  }

  return $weights;
}

/**
 * @param $weights
 * @param $field_name
 */
function _rg_product_change_field_settings($weights, $field_name) {

  // Reset the positions in the view and form display modes
  foreach ($weights as $display_mode => $view_modes) {

    if ($display_mode === 'entity_form_display') {
      $widget = 'string_textfield';
    } elseif ($display_mode === 'entity_view_display') {
      $widget = 'string';
    } else {
      $widget = 'default';
    }

    foreach ($view_modes as $view_mode => $weight) {
      if ($weight) {
        \Drupal::entityTypeManager()
          ->getStorage($display_mode)
          ->load($view_mode)
          ->setComponent($field_name, [
            'weight' => $weight,
            'type' => $widget
          ])
          ->save();
      }
    }
  }
}