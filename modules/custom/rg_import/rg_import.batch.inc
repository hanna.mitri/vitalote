<?php

use Drupal\migrate\MigrateMessage;
use Drupal\migrate\Plugin\MigrationInterface;
use Drupal\migrate_tools\MigrateExecutable;

/**
 * Batch execution.
 *
 * @param $number
 *   Number of times to execute.
 * @param array $context
 *   An array of contextual key/value information for rebuild batch process.
 */
function _awesome_batch($migrations, &$context)
{
  if ($migrations) {

    // Initiate multistep processing.
    if (empty($context['sandbox'])) {
      $context['sandbox']['progress'] = 0;
      $context['sandbox']['max'] = count($migrations);
    }

    // Process the next 1 if there are at least 1 left. Otherwise,
    // we process the remaining number.
    $batch_size = 1;
    $max = $context['sandbox']['progress'] + $batch_size;
    if ($max > $context['sandbox']['max']) {
      $max = $context['sandbox']['max'];
    }

    // Start where we left off last time.
    $start = $context['sandbox']['progress'];
    for ($i = $start; $i < $max; $i++) {
      // Run migration.
      $migration_id = $migrations[$i];

      if (empty($migrations)) {
        $this->logger->error(dt('No migrations found.'));
      }
      $migration = \Drupal::service('plugin.manager.migration')->createInstance($migration_id);
      if ($migration->getStatus() != MigrationInterface::STATUS_IDLE) {
        $migration->setStatus(MigrationInterface::STATUS_IDLE);
      }
      // Migrations for products needs updates.
      if (strpos($migration_id, 'sku') || strpos($migration_id, 'page')) {
        $migration->setTrackLastImported(TRUE);
        $migration->getIdMap()->prepareUpdate();
      }
      $executable = new MigrateExecutable($migration, new MigrateMessage());
      $result = $executable->import();

      if ($result) {
        $current_time = \Drupal::time()->getRequestTime();
        $migrate_last_imported_store = \Drupal::keyValue('migrate_last_imported');
        $migrate_last_imported_store->set($migration_id, $current_time * 1000);
      }
      $created = (string) $executable->getCreatedCount();
      $updated = (string) $executable->getUpdatedCount();
      $ignored = (string) $executable->getIgnoredCount();


      // Send report.
      $message = t(
        '@migration_id : created - @created, updated - @updated, ignored - @ignored',
        [
          '@migration_id' => $migration_id,
          '@created' => $created,
          '@updated' => $updated,
          '@ignored' => $ignored,
        ]);
      \Drupal::messenger()->addMessage($message);

      // Update our progress!
      $context["message"] = t('Completed @current of @total.',
        [
          '@current' => $context['sandbox']['progress'],
          '@total' => $context['sandbox']['max'],
        ]
      );
      $context['sandbox']['progress']++;
    }

    // Multistep processing : report progress.
    if ($context['sandbox']['progress'] != $context['sandbox']['max']) {
      $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
    }
  }
}