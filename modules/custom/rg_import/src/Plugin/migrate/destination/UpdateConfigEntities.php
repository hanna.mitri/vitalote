<?php

namespace Drupal\rg_import\Plugin\migrate\destination;

use Drupal\migrate\Plugin\migrate\destination\EntityFieldInstance;
use Drupal\migrate\Row;

/**
 * @MigrateDestination(
 *   id = "entity:field_config"
 * )
 */
class UpdateConfigEntities extends EntityFieldInstance {

  /**
   * Creates or loads an entity.
   *
   * @param \Drupal\migrate\Row $row
   *   The row object.
   * @param array $old_destination_id_values
   *   The old destination IDs.
   *
   * @return \Drupal\Core\Entity\EntityInterface
   *   The entity we are importing into.
   */
  protected function getEntity(Row $row, array $old_destination_id_values) {
    $entity_id = null;
    if (reset($old_destination_id_values)) {
      $entity_id = implode('.', $old_destination_id_values);
    }
    $entity_id = $entity_id ?: $this->getEntityId($row);
    if (!empty($entity_id) && ($entity = $this->storage->load($entity_id))) {
      // Allow updateEntity() to change the entity.
      $entity = $this->updateEntity($entity, $row) ?: $entity;
    }
    else {
      // Attempt to ensure we always have a bundle.
      if ($bundle = $this->getBundle($row)) {
        $row->setDestinationProperty($this->getKey('bundle'), $bundle);
      }

      // Stubs might need some required fields filled in.
      if ($row->isStub()) {
        $this->processStubRow($row);
      }
      $entity = $this->storage->create($row->getDestination());
      $entity->enforceIsNew();
    }
    return $entity;
  }

}