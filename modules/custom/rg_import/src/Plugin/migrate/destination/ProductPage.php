<?php
/**
 * Created by PhpStorm.
 * User: ginger
 * Date: 04.02.19
 * Time: 12:37
 */

namespace Drupal\rg_import\Plugin\migrate\destination;

use Drupal\commerce_product\Entity\Product;
use Drupal\commerce_product\Entity\ProductVariation;
use Drupal\Core\Entity\EntityInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\migrate\Plugin\migrate\destination\EntityContentBase;
use Drupal\migrate\Plugin\MigrationInterface;
use Drupal\migrate\Row;
use Drupal\migrate\Plugin\MigrateIdMapInterface;
use Drupal\Core\TypedData\TypedDataInterface;

/**
 * Provides node destination, updating only the non-empty fields.
 *
 * @MigrateDestination(
 *   id = "product_page",
 * )
 */
class ProductPage extends EntityContentBase
{

  /**
   * Entity type.
   *
   * @var string $entityType
   */
  public static $entityType = 'commerce_product';

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition, MigrationInterface $migration = NULL)
  {
    return parent::create($container, $configuration, 'entity:' . static::$entityType, $plugin_definition, $migration);
  }

  /**
   * Creates or loads an entity.
   *
   * @param \Drupal\migrate\Row $row
   *   The row object.
   * @param array $old_destination_id_values
   *   The old destination IDs.
   *
   * @return \Drupal\Core\Entity\EntityInterface
   *   The entity we are importing into.
   * @throws \Drupal\migrate\MigrateException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function getEntity(Row $row, array $old_destination_id_values) {
    $entity_id = $this->findEntityByGroup($row); //todo

    if (!empty($entity_id) && ($entity = $this->storage->load($entity_id))) {
      // Allow updateEntity() to change the entity.
      $entity = $this->updateEntity($entity, $row) ?: $entity;
    }
    else {
      // Attempt to ensure we always have a bundle.
      if ($bundle = $this->getBundle($row)) {
        $row->setDestinationProperty($this->getKey('bundle'), $bundle);
      }

      // Stubs might need some required fields filled in.
      if ($row->isStub()) {
        $this->processStubRow($row);
      }
      $entity = $this->storage->create($row->getDestination());
      $entity->enforceIsNew();
    }
    return $entity;
  }

  /**
   * Search product with such project number
   *
   * @param Row $row
   * @return int|string|null
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function findEntityByGroup(Row $row) {
    $entityId = null;
    $field_range_cooker_collection = $row->getDestinationProperty('field_range_cooker_collection');
    $vid = $row->getDestinationProperty('variations');
    $this->deleteOldRelations($row, $vid, $field_range_cooker_collection);
    $query = \Drupal::entityQuery('commerce_product')
      ->condition('type', $this->configuration["default_bundle"])
      ->condition('field_range_cooker_collection', $field_range_cooker_collection);
    $query->accessCheck(FALSE);
    $pids = $query->execute();
    if (!empty($pids)) {
      ksort($pids);
      $entityId = array_pop($pids);
      foreach ($this->storage->loadMultiple($pids) as $entity) {
        $entity->delete();
      }
      $variation = ProductVariation::load($vid);
      $variation->get('product_id')->setValue($entityId);
      $variation->save();
    }
    return $entityId;
  }

  /**
   * @param Row $row
   * @param null $vid
   * @param $variationCollection
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function deleteOldRelations(Row $row, $vid = null, $variationCollection)
  {
    //get all products with such variation
    $query = \Drupal::entityQuery('commerce_product')
      ->condition('variations', $vid)
      ->condition('field_range_cooker_collection', $variationCollection, '!=');
    $pids = $query->execute();

    foreach ($pids as $value) {
      $product = Product::load($value);
      $variations = $product->variations->getValue();
      foreach ($variations as $key => $variation) {
        if ($variation["target_id"] == $vid){
          unset($variations[$key]);
        }
      }
      $product->get('variations')->setValue($variations);
      $product->save();
    }

  }

  /**
   * {@inheritdoc}
   */
  protected function updateEntity(EntityInterface $entity, Row $row)
  {
    // By default, an update will be preserved.
    $rollback_action = MigrateIdMapInterface::ROLLBACK_PRESERVE;

    // Make sure we have the right translation.
    if ($this->isTranslationDestination()) {
      $property = $this->storage->getEntityType()->getKey('langcode');
      if ($row->hasDestinationProperty($property)) {
        $language = $row->getDestinationProperty($property);
        if (!$entity->hasTranslation($language)) {
          $entity->addTranslation($language);

          // We're adding a translation, so delete it on rollback.
          $rollback_action = MigrateIdMapInterface::ROLLBACK_DELETE;
        }
        $entity = $entity->getTranslation($language);
      }
    }

    // If the migration has specified a list of properties to be overwritten,
    // clone the row with an empty set of destination values, and re-add only
    // the specified properties.
    if (isset($this->configuration['overwrite_properties'])) {
      $clone = $row->cloneWithoutDestination();
      foreach ($this->configuration['overwrite_properties'] as $property) {
        $clone->setDestinationProperty($property, $row->getDestinationProperty($property));
      }
      $row = $clone;
    }

    foreach ($row->getDestination() as $field_name => $values) {
      $field = $entity->$field_name;
      if ($field instanceof TypedDataInterface) {
        //add variations to product
        if ($field_name == 'variations' && !empty($field->getValue())) {
          $old_values = [];
          foreach ($field->getValue() as $old_value) {
            $old_values[] = $old_value["target_id"];
          }
          $updated_values = array_merge($old_values, [$values]);
          $updated_values = array_unique($updated_values);
          $field->setValue($updated_values);
        }
        else {
          $field->setValue($values);
        }
      }
    }

    $this->setRollbackAction($row->getIdMap(), $rollback_action);

    // We might have a different (translated) entity, so return it.
    return $entity;
  }

}