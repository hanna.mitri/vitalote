<?php

namespace Drupal\rg_import\Plugin\migrate\destination;

use Drupal\migrate\Plugin\migrate\destination\DestinationBase;
use Drupal\migrate\Plugin\MigrationInterface;
use Drupal\migrate\Row;

/**
 * Provides delete_variation destination plugin.
 *
 * @MigrateDestination(
 *   id = "delete_variation",
 * )
 */
class DeleteVariation extends DestinationBase {

  /**
   * @var string
   */
  protected $entityType = 'commerce_product_variation';

  /**
   * The entity storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $storage;

  /**
   * DeleteVariation constructor.
   * @param array $configuration
   * @param $plugin_id
   * @param $plugin_definition
   * @param MigrationInterface $migration
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, MigrationInterface $migration)
  {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $migration);
    $this->storage = \Drupal::entityTypeManager()->getStorage($this->entityType);
  }

  /**
   * {@inheritdoc}
   */
  public function getIds() {
    $ids = [];

    return $ids;
  }

  /**
   * {@inheritdoc}
   */
  public function fields(MigrationInterface $migration = NULL) {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function import(Row $row, array $old_destination_id_values = []) {
    $sku = $row->getDestinationProperty('sku');

    $product_variation = \Drupal::entityQuery($this->entityType)->condition("sku", $sku)->range(0, 1)->execute();
    if ($product_variation) {
      $product_variation = $this->storage->load(current($product_variation));
      $product_variation->delete();
    }
    \Drupal::database()->delete('product_sku')
      ->condition('sku', $sku)
      ->execute();
  }


}
