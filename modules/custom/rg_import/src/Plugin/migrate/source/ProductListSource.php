<?php

namespace Drupal\rg_import\Plugin\migrate\source;

use Drupal\migrate\Plugin\MigrationInterface;
use Drupal\migrate_plus\Plugin\migrate\source\Url;

/**
 * Source plugin for retrieving data via URLs.
 *
 * @MigrateSource(
 *   id = "product_list_source"
 * )
 */
class ProductListSource extends Url
{
  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, MigrationInterface $migration) {
    $migration->setTrackLastImported(TRUE);

    if (!is_array($configuration['urls'])) {
      if ($configuration["updates_only"] && $this->ifProductsExist($migration)) {
        $last_imported = $this->getTimeOfLastImport($migration);
        $last_imported = (int) $last_imported - 86400;
      }
      else {
        $last_imported = '1000000000';
      }
      $configuration['urls'] = [$configuration['urls'] . '/' . $last_imported];
    }
    parent::__construct($configuration, $plugin_id, $plugin_definition, $migration);

    $this->sourceUrls = $configuration['urls'];
  }

  private function getTimeOfLastImport($migration){
    $migrate_last_imported_store = \Drupal::keyValue('migrate_last_imported');
    $last_imported = $migrate_last_imported_store->get($migration->id(), FALSE);
    $last_imported = round($last_imported / 1000);
    return $last_imported;
  }

  /**
   * to check if do import for all products or for updated only
   * @param $migration
   * @return bool
   */
  private function ifProductsExist($migration) {
    $bundle = $migration->pluginDefinition["process"]["type"]["default_value"];
    $query = \Drupal::entityQuery('commerce_product_variation');
    $query->condition('type', $bundle);
    $entity_ids = $query->execute();
    return !empty($entity_ids);
  }

}