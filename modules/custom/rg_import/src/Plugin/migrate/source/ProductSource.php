<?php

namespace Drupal\rg_import\Plugin\migrate\source;

use Drupal\migrate_plus\Plugin\migrate\source\SourcePluginExtension;
use Drupal\migrate\Plugin\MigrationInterface;

/**
 * Source plugin for retrieving data via URLs.
 *
 * @MigrateSource(
 *   id = "product_source"
 * )
 */
class ProductSource extends SourcePluginExtension
{

  /**
   * The source URLs to retrieve.
   *
   * @var array
   */
  protected $sourceUrls = [];

  /**
   * The data parser plugin.
   *
   * @var \Drupal\migrate_plus\DataParserPluginInterface
   */
  protected $dataParserPlugin;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, MigrationInterface $migration) {
    if (!is_array($configuration['urls'])) {
      $destination = $migration->getDestinationConfiguration();
      $configuration['urls'] = $this->getUrlList(
        $configuration["domain"],
        $configuration["apiname"],
        $configuration["version"],
        $configuration["inside_page"],
        $destination["default_bundle"]
      );
    }
    parent::__construct($configuration, $plugin_id, $plugin_definition, $migration);

    $this->sourceUrls = $configuration['urls'];
  }

  /**
   * Return a string representing the source URLs.
   *
   * @return string
   *   Comma-separated list of URLs being imported.
   */
  public function __toString() {
    // This could cause a problem when using a lot of urls, may need to hash.
    $urls = implode(', ', $this->sourceUrls);
    return $urls;
  }

  /**
   * Returns the initialized data parser plugin.
   *
   * @return \Drupal\migrate_plus\DataParserPluginInterface
   *   The data parser plugin.
   */
  public function getDataParserPlugin() {
    if (!isset($this->dataParserPlugin)) {
      $this->dataParserPlugin = \Drupal::service('plugin.manager.migrate_plus.data_parser')->createInstance($this->configuration['data_parser_plugin'], $this->configuration);
    }
    return $this->dataParserPlugin;
  }

  /**
   * Creates and returns a filtered Iterator over the documents.
   *
   * @return \Iterator
   *   An iterator over the documents providing source rows that match the
   *   configured item_selector.
   */
  protected function initializeIterator() {
    return $this->getDataParserPlugin();
  }


  /**
   * @param $domain
   * @param $apiname
   * @param $version
   * @param $inside_page
   * @param $type
   * @return array
   */
  public function getUrlList($domain, $apiname, $version, $inside_page, $type)
  {
    $languages = \Drupal::languageManager()->getLanguages();

    $query_result = \Drupal::database()->select('product_sku', 'p')
      ->fields('p', array('sku'))
      ->condition('p.type', $type, '=')
      ->condition('p.status', STATUS_NEED_IMPORT, '=')
      ->orderBy('id', 'DESC')
      ->execute()
      ->fetchAll();
    if (!empty($query_result)) {
      $urls = [];

      foreach ($query_result as $item) {

        foreach ($languages as $ln) {
          $ln = $ln->getId();
          $url = 'http://' . $domain;
          $url .= '/' . $apiname;
          $url .= '/' . $ln;
          $url .= '/' . $version;
          $url .= '/' . $inside_page;
          $url .= '?sku=' . $item->sku;
          $urls[] = $url;
        }
      }
    }
    else {
      $urls = ['http://akeneoce.hicmedia.com/customapi/en/V1/GetRangeMasterProducts'];
    }
    return $urls;
  }

}