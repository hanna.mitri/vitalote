<?php

namespace Drupal\rg_import\Plugin\migrate\process;

use Drupal\commerce_product\Entity\ProductAttributeValue;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;
use Drupal\taxonomy\Entity\Term;

/**
 * Returns value of input array or JSON string using JSONPath.
 *
 * Example of usage:
 * @code
 * process:
 *   title:
 *     -
 *       plugin: build_title
 *       source: source_field
 * @endcode
 *
 * @MigrateProcessPlugin(
 *   id = "build_title"
 * )
 */
class BuildTitle extends ProcessPluginBase
{
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property)
  {
    $result = '';
    if (!empty($value)) {
      $collection = Term::load($value);
      $result .= $collection->getName();
    }
    else {
      $result .= $row->getDestinationProperty('field_project_number');
      $row->setDestinationProperty('status', false);
    }
    return $result;
  }
}