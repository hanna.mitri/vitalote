<?php

namespace Drupal\rg_import\Plugin\migrate\process;

use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;

/**
 * Returns value from other field if value is empty.
 *
 * Example of usage:
 * @code
 * process:
 *   title:
 *     -
 *       plugin: default_value_from_other_field
 *       default_value: source_field
 * @endcode
 *
 * @MigrateProcessPlugin(
 *   id = "default_value_from_other_field"
 * )
 */
class DefaultValueFromOtherField extends ProcessPluginBase
{
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property)
  {
    $result = $value;
    $source = $row->getSource();
    if (empty($result)) {
      $result = $source[$this->configuration["default_value"]];
    }
    return $result;
  }
}