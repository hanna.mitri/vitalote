<?php

namespace Drupal\rg_import\Plugin\migrate\process;

use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;

/**
 * Returns value of input array or JSON string using JSONPath.
 *
 * Example of usage:
 * @code
 * process:
 *   title:
 *     -
 *       plugin: get_attribute_value
 *       source: source_field
 *       attribute: fuel
 * @endcode
 *
 * @MigrateProcessPlugin(
 *   id = "get_attribute_value"
 * )
 */
class GetAttributeValue extends ProcessPluginBase
{
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property)
  {
    $result = null;
    $source = $row->getSource();
    if ($source["attr"]) {
      foreach ($source['attr'] as $attribute) {

        if ($attribute['key'] == $this->configuration["attribute"]) {
          $result = $attribute['value'];
          break;
        }
      }

    }
    return $result;
  }
}