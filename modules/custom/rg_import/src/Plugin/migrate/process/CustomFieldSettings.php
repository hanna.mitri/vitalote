<?php
/**
 * Created by PhpStorm.
 * User: ginger
 * Date: 25.12.18
 * Time: 16:12
 */

namespace Drupal\rg_import\Plugin\migrate\process;

use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;

/**
 * Perform custom value transformations.
 *
 * @MigrateProcessPlugin(
 *   id = "custom_field_settings"
 * )
 *
 * To do custom value transformations use the following:
 *
 * @code
 * settings:
 *   plugin: custom_field_settings
 *   source: type
 * @endcode
 *
 */
class CustomFieldSettings extends ProcessPluginBase
{
  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    $settings = array();
    switch ($value) {
      case 'pim_catalog_date' :
        $settings['datetime_type'] = 'datetime';
        break;
      case 'pim_catalog_simpleselect' :
        $settings['max_length'] = 255;
        break;
      case 'pim_catalog_text' :
        $settings['max_length'] = 255;
        break;
      case 'pim_catalog_file' :
        $settings['display_field'] = true;
        $settings['display_default'] = false;
        $settings['uri_scheme'] = 'public';
        $settings['target_type'] = 'file';
        break;
      case 'pim_catalog_image' :
        $settings['uri_scheme'] = 'public';
        $settings['default_image']['uuid'] = '';
        $settings['default_image']['alt'] = '';
        $settings['default_image']['title'] = '';
        $settings['default_image']['width'] = null;
        $settings['default_image']['height'] = null;
        $settings['target_type'] = 'file';
        $settings['display_field'] = false;
        $settings['display_default'] = false;
        break;
      case 'pim_catalog_number' :
        $settings['precision'] = 10;
        $settings['scale'] = 0;
        break;
    }
    return $settings;
  }
}