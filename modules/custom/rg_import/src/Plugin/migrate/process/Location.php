<?php

namespace Drupal\rg_import\Plugin\migrate\process;

use GuzzleHttp\Exception\RequestException;
use Drupal\Component\Serialization\Json;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;

/**
 * Perform custom value transformations.
 *
 * @MigrateProcessPlugin(
 *   id = "geocodding_location"
 * )
 *
 * To do custom value transformations use the following:
 *
 * @code
 * field_geofield:
 *   plugin: geocodding_location
 *   source: text
 * @endcode
 *
 */
class Location extends ProcessPluginBase
{

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    if (is_array($value)) {
      $address = implode(' ', $value);
    }
    elseif ($value) {
      $address = $value;
    }
    elseif (empty($value)) {
      return null;
    }
    $request_url = 'https://maps.googleapis.com/maps/api/geocode/json?address=' . $address;
    $google_map_key = \Drupal::config('geolocation.settings')->get('google_map_api_key');


    if (!empty($google_map_key)) {
      $request_url .= '&key=' . $google_map_key;
    }

    try {
      $result = Json::decode(\Drupal::httpClient()->request('GET', $request_url)->getBody());
    } catch (RequestException $e) {
      watchdog_exception('geolocation', $e);
      return null;
    }

    if (
      $result['status'] != 'OK'
      || empty($result['results'][0]['geometry'])
    ) {
      return null;
    }

    return [
      'lat' => $result['results'][0]['geometry']['location']['lat'],
      'lng' => $result['results'][0]['geometry']['location']['lng'],
    ];
  }
}