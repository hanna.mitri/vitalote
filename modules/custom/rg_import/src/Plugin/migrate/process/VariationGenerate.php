<?php

namespace Drupal\rg_import\Plugin\migrate\process;

use Drupal\commerce_price\Price;
use Drupal\commerce_product\Entity\ProductAttributeValue;
use Drupal\commerce_product\Entity\ProductVariation;
use Drupal\Component\Transliteration\TransliterationInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Site\Settings;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;
use Drupal\taxonomy\Entity\Term;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Language\LanguageInterface;


/**
 * This plugin generates entities within the process plugin.
 *
 * @MigrateProcessPlugin(
 *   id = "variation_generate"
 * )
 *
 * @see EntityLookup
 *
 * All the configuration from the lookup plugin applies here. In its most
 * simple form, this plugin needs no configuration. If there are fields on the
 * generated entity that are required or need some value, their values can be
 * provided via values and/or default_values configuration options.
 *
 * Example usage with values and default_values configuration:
 * @code
 * destination:
 *   plugin: 'entity:node'
 * process:
 *   type:
 *     plugin: default_value
 *     default_value: page
 *   foo: bar
 *   field_tags:
 *     plugin: variation_generate
 *     source: tags
 *     default_values:
 *       description: Default description
 *     values:
 *       field_long_description: some_source_field
 *       field_foo: '@foo'
 * @endcode
 */
class VariationGenerate extends ProcessPluginBase implements ContainerFactoryPluginInterface
{
    private $default_image_url;

  /**
   * The row from the source to process.
   *
   * @var \Drupal\migrate\Row
   */
  protected $row;

  /**
   * The MigrateExecutable instance.
   *
   * @var \Drupal\migrate\MigrateExecutable
   */
  protected $migrateExecutable;

  /**
   * The get process plugin instance.
   *
   * @var \Drupal\migrate\Plugin\migrate\process\Get
   */
  protected $getProcessPlugin;

  /**
   * The transliteration service.
   *
   * @var \Drupal\Component\Transliteration\TransliterationInterface
   */
  protected $transliteration;

  /**
   * @var array of available filters
   */
  protected $existed_attributes = [
    'color' => 'colour',
    'design_style' => 'tapdessty',
    'drainer_position' => 'sindrapos',
    'fuel' => 'fuel',
    'main_material' => 'sbmaimat',
    'no_of_bowls' => 'sinnoofbow',
    'size' => 'size',
    'trim' => 'pfinish',
    'number_of_programs' => 'numofpro',
  ];

  /**
   * Constructs a VariationGenerate plugin.
   *
   * @param array $configuration
   *   The plugin configuration.
   * @param string $plugin_id
   *   The plugin ID.
   * @param mixed $plugin_definition
   *   The plugin definition.
   * @param \Drupal\Component\Transliteration\TransliterationInterface $transliteration
   *   The transliteration service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, TransliterationInterface $transliteration)
  {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->transliteration = $transliteration;
    $host = Settings::get('host') ?: '';
    $this->default_image_url = $host . '/sites/default/files/default.jpg';
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition)
  {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('transliteration')
    );
  }

  /**
   * EntityGenerate constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $pluginId
   *   The plugin_id for the plugin instance.
   * @param mixed $pluginDefinition
   *   The plugin implementation definition.
   * @param \Drupal\migrate\Plugin\MigrationInterface $migration
   *   The migration.
   * @param \Drupal\Core\Entity\EntityManagerInterface $entityManager
   *   The $entityManager instance.
   * @param \Drupal\Core\Entity\EntityReferenceSelection\SelectionPluginManagerInterface $selectionPluginManager
   *   The $selectionPluginManager instance.
   * @param \Drupal\migrate\Plugin\MigratePluginManager $migratePluginManager
   *   The MigratePluginManager instance.
   */

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrateExecutable, Row $row, $destinationProperty)
  {
    $this->row = $row;
    $this->migrateExecutable = $migrateExecutable;

    $source = $this->row->getSource();
    if ($source["sku"]) {
      $source['collection'] = $this->row->getDestinationProperty('collection');
      if ($vids = $this->ifVariationExist($source["sku"])) {
        $result = $this->updateEntity($source, $vids);
      } else {
        $result = $this->generateEntity($source);
      }
      \Drupal::database()->update('product_sku')
        ->fields([
          'imported' => WAS_PROCESSED,
          'status' => STATUS_IMPORTED,
        ])
        ->condition('sku', $source["sku"], '=')
        ->execute();
    }

    return $result;
  }

  /**
   * Set variation field values for a given value.
   *
   * @param string $value
   *   Value to use in creation of the entity.
   *
   * @param $variation
   * @return int|string
   *   The variation id of the generated entity.
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Drupal\Core\TypedData\Exception\ReadOnlyException
   */
  protected function setVariationFields($value, ProductVariation $variation)
  {
    $variation->set('status', $value["enabled"]);
    $variation->set('field_sheet_url', $value["sheet_url"]);
    $variation->set('field_energy_label_url', $value["energy_label_url"]);
    $variation->set('field_variation_video', '');
    if (!empty($value["dam_thumb"])) {
      $variation->set('field_dam_thumb', $value["dam_thumb"]);
    }
    else {
        $variation->set('field_dam_thumb', $this->default_image_url);
    }
    if (!empty($value["dam_image"])) {
      $variation->set('field_dam_image', $value["dam_image"]);
    }
    else {
        $variation->set('field_dam_image', $this->default_image_url);
    }
    $brand = $this->row->getDestinationProperty('temp_brand');
    if (!empty($brand) && $variation->hasField('field_brand')) {
      $variation->set('field_brand', $brand);
    }
    $country = $this->row->getDestinationProperty('temp_country');
    if (!empty($country) && $variation->hasField('field_country')) {
      $variation->set('field_country', $country);
    }
    $categories = $this->row->getDestinationProperty('field_category');
    if (!empty($categories)) {
      $variation->set('field_category', $categories);
    }
    if (!empty($value["collection"])) {
      $variation->set('field_range_cooker_collection', $value["collection"]);
      $collection = Term::load($value["collection"]);
      $collection_name = $collection->getName() ?: null;
    }
    else {
      $variation->set('status', false);
    }


    //to set values for fields
    foreach ($value["attr"] as $attribute) {
      //skip field process if no value

        $fieldName = $this->transformToMachineNumber($attribute['key']);
        if ($variation->hasField($fieldName)) {

          //datetime
          $field_type = $variation->get($fieldName)->getFieldDefinition()->getType();
          if ($field_type == 'datetime') {
            $attribute['value'] = substr($attribute['value'], 0, -6);
          }
          //textarea
          elseif ($field_type == 'text_long') {
            $variation->$fieldName->setValue([
                'value' => $attribute['value'],
                'format' => 'raw_html',
            ]);
            continue;
          }
          //price field
          if ($fieldName == 'price' && !is_float($attribute['value']) && is_numeric($attribute['value'])) {
            $currency = $this->row->getDestinationProperty('temp_currency') ?: 'GBP';
            $attribute['value'] = new Price($attribute['value'], $currency);
          }
          if ($fieldName == 'promotionprice' && !is_float($attribute['value']) && is_numeric($attribute['value'])) {
            $currency = $this->row->getDestinationProperty('temp_currency') ?: 'GBP';
            $attribute['value'] = new Price($attribute['value'], $currency);
            $variation->set('list_price', $attribute['value']);
            continue;
          }

          //variation attributes
          $machine_name = array_search($fieldName, $this->existed_attributes);
          if ($machine_name && $variation->hasField("attribute_$machine_name") && $attribute['value']) {
            $id = $this->getAttributeId($machine_name, $attribute['value']);
            $variation->set("attribute_$machine_name", $id);
            continue;
          }

          $variation->set($fieldName, $attribute['value']);
        }
    }


    //set title
    $temp_title = $this->row->getDestinationProperty('temp_title');
    if ($temp_title) {
      $title = $temp_title;
    } else {
      $title = $collection_name;
    }
    $variation->field_pseudo_title->setValue([
      'value' => $title,
      'format' => 'raw_html',
    ]);
    $variation->set('title', trim(strip_tags($title)));

    $variation->save();
    return $variation->id();
  }

  /**
   * Update existing Variation
   *
   * @param $value
   * @param $vid
   * @return int|string|null
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Drupal\Core\TypedData\Exception\ReadOnlyException
   */
  protected function updateEntity($value, $vid)
  {
    $variation = ProductVariation::load($vid);
    $vid = $this->setVariationFields($value, $variation);
    return $vid;
  }

  /**
   * @param $value
   * @return int|string
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Drupal\Core\TypedData\Exception\ReadOnlyException
   */
  protected function generateEntity($value)
  {
    $variation = ProductVariation::create([
      'type' => $this->configuration["type"],
      'sku' => $value["sku"],
    ]);
    $vid = $this->setVariationFields($value, $variation);
    return $vid;
  }

  /**
   * Check if exist variation with such sku
   *
   * @param null $sku
   * @return mixed|null
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function ifVariationExist($sku = null)
  {
    $vid = null;
    $query = \Drupal::entityQuery('commerce_product_variation')
      ->condition('sku', $sku, '=');
    $vids = $query->execute();

    if (is_array($vids)) {
      $vid = array_pop($vids);
      foreach ($vids as $id) {
        $variation = ProductVariation::load($id);
        if (!$variation->getProductId()) {
          $variation->set('product_id', 1);
        }
        $variation->delete();
      }
    }
    return $vid;
  }

  /**
   * @param $name
   * @param $value
   * @return int|string|null
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function getAttributeId($name, $value)
  {
    // Look up while filtering by Attribute
    $result = \Drupal::entityTypeManager()
      ->getStorage('commerce_product_attribute_value')
      ->loadByProperties(['attribute' => $name, 'name' => $value]);
    reset($result);
    $productAttributeId = key($result);

    //Create new attribute if not exist
    if (!is_int($productAttributeId)) {
      $attribute = ProductAttributeValue::create([
        'attribute' => $name,
        'name' => $value,
      ]);
      $attribute->save();
      $productAttributeId = $attribute->id();
    }

    return $productAttributeId;
  }


  /**
   * {@inheritdoc}
   */
  protected function transformToMachineNumber($value)
  {
    $new_value = $this->transliteration->transliterate($value, LanguageInterface::LANGCODE_DEFAULT, '_');
    $new_value = strtolower($new_value);
    $new_value = preg_replace('/[^a-z0-9_]+/', '_', $new_value);
    return preg_replace('/_+/', '_', $new_value);
  }

}
