<?php

namespace Drupal\rg_import\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class KickOffBatchForm.
 */
class ImportBatchForm extends FormBase {

  /**
   * List of migrations to be imported.
   */
  const MIGRATIONS = [
    'rgp_category',
    'rgp_collection',
    'rgp_fuel_type',
    'rgp_trim',
    'rgp_color',
    'rgp_size',
    'rgp_noofbowls',
    'rgp_drainer_position',
    'rgp_design_style',
    'rgp_delete_sku',
    'rgp_cooker_field_storage',
    'rgp_cooker_field_instance',
    'rgp_cooker_field_form_mode',
    'rgp_cooker_sku',
    'rgp_cooker_page',
    //builtin
    'rgp_builtin_field_storage',
    'rgp_builtin_field_instance',
    'rgp_builtin_field_form_mode',
    'rgp_builtin_sku',
    'rgp_builtin_page',
    //disposal
    'rgp_disposal_field_storage',
    'rgp_disposal_field_instance',
    'rgp_disposal_default_view',
    'rgp_disposal_field_form_mode',
    'rgp_disposal_sku',
    'rgp_disposal_page',
    //dishwashers
    'rgp_dishwashers_field_storage',
    'rgp_dishwashers_field_instance',
    'rgp_dishwashers_default_view',
    'rgp_dishwashers_field_form_mode',
    'rgp_dishwashers_sku',
    'rgp_dishwashers_page',
    //fridge
    'rgp_fridge_field_storage',
    'rgp_fridge_field_instance',
    'rgp_fridge_default_view',
    'rgp_fridge_field_form_mode',
    'rgp_fridge_sku',
    'rgp_fridge_page',
    //hood
    'rgp_hood_field_storage',
    'rgp_hood_field_instance',
    'rgp_hood_default_view',
    'rgp_hood_field_form_mode',
    'rgp_hood_sku',
    'rgp_hood_page',
    //sink
    'rgp_sink_field_storage',
    'rgp_sink_field_instance',
    'rgp_sink_default_view',
    'rgp_sink_field_form_mode',
    'rgp_sink_sku',
    'rgp_sink_page',
    //splashback
    'rgp_splashback_field_storage',
    'rgp_splashback_field_instance',
    'rgp_splashback_default_view',
    'rgp_splashback_field_form_mode',
    'rgp_splashback_sku',
    'rgp_splashback_page',
    //tap
    'rgp_tap_field_storage',
    'rgp_tap_field_instance',
    'rgp_tap_default_view',
    'rgp_tap_field_form_mode',
    'rgp_tap_sku',
    'rgp_tap_page',
    //cooker 60
    'rgp_cooker_60_field_storage',
    'rgp_cooker_60_field_instance',
    'rgp_cooker_60_default_view',
    'rgp_cooker_60_field_form_mode',
    'rgp_cooker_60_sku',
    'rgp_cooker_60_page',
  ];

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'kick_off_batch_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['actions'] = ['#type' => 'actions'];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => t('Execute'),
    ];

    $form['migrations_to_execute'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Migrations'),
      '#options' => array_combine(self::MIGRATIONS, self::MIGRATIONS),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $chosen_migrations = [];
    $migrations = $form_state->getValue('migrations_to_execute');
    foreach ($migrations as $migration) {
      if ($migration !== 0) {
        $chosen_migrations[] = $migration;
      }
    }
    $batch = [
      'init_message' => t('Executing import...'),
      'operations' => [
        [
          '_awesome_batch',
          [$chosen_migrations],
        ],
      ],
      'file' => drupal_get_path('module', 'rg_import') . '/rg_import.batch.inc',
    ];
    batch_set($batch);
  }
}