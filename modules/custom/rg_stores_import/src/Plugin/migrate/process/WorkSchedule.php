<?php
/**
 * Created by PhpStorm.
 * User: ginger
 * Date: 25.12.18
 * Time: 16:12
 */

namespace Drupal\rg_stores_import\Plugin\migrate\process;

use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;

/**
 * Perform custom value transformations.
 *
 * @MigrateProcessPlugin(
 *   id = "work_schedule"
 * )
 *
 * To do custom value transformations use the following:
 *
 * @code
 * field_geofield:
 *   plugin: work_schedule
 *   source:
 *    - monday
 *    - tuesday
 *    - wednesday
 *    - thursday
 *    - friday
 *    - saturday
 *    - sunday
 * @endcode
 *
 */
class WorkSchedule extends ProcessPluginBase
{
  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    if (count($value) > 1) {
      $rows = [
        ['Mon:', $value[0]],
        ['Tue:', $value[1]],
        ['Wed:', $value[2]],
        ['Thu:', $value[3]],
        ['Fri:', $value[4]],
        ['Sat:', $value[5]],
        ['Sun:', $value[6]],
      ];

      $table = array(
        '#theme' => 'table',
        '#rows' => $rows,
      );
      $html = \Drupal::service('renderer')->renderPlain($table);
      return $html;
    }
    return null;
  }
}