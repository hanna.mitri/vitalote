jQuery(document).ready(function ($) {

  //for Similar blog and Other blog blocks
  var similarBlogContent = $('.block-views-blockmt-latest-block-7 .view-display-id-block_7 > .view-content');
  var similarBlogBlock = $('.block-views-blockmt-latest-block-7 ');
  var otherBlogContent = $('.block--type-mt-tabs-block .block-views-blockmt-latest-block-1 .view-display-id-block_1 > .view-content');
  var otherBlogBlock = $('.block--type-mt-tabs-block');

  if (similarBlogContent.length <= 0) {
    similarBlogBlock.hide();
  }

  if (otherBlogContent.length <= 0) {
    otherBlogBlock.hide();
  }

  //for filters on mobile
  //for moving filters
  if (jQuery(window).width() < 768) {
    var bloggers = $('.block-views-blockmt-articles-grid-block-2');
    var categories = $('.block-blog-tags-links-block');
    var resetButton = $('.reset-button-block');
    var breadcrumbsBlock = $('#block-corporateplus-breadcrumbs');
    var pageTitle = $('.block-page-title-block > .content > h1.page-title');

    $(categories).wrap('<div class="filter-by"><div class="filter-content"></div></div>');

    var filterByBlock = $('.filter-by');
    var filterContent = $('.filter-content');
    var filterHeaderText;

    if (pageTitle.text() !== 'Blog') {
      filterHeaderText = '<span class="filter-header-text">' + $.trim(pageTitle.text()) + '</span>';
    } else {
      filterHeaderText = '<span class="filter-header-text">All</span>';
    }
    filterByBlock.prepend('<div class="filter-header">' + filterHeaderText + '<i class="fa fa-angle-up"></i></div>');

    var filterHeader = $('.filter-header');
    var filterArrow = $('.filter-header > i');

    filterContent.prepend(bloggers);
    breadcrumbsBlock.after(filterByBlock);
    if (resetButton.length > 0) {
      filterByBlock.before(resetButton);
    }

    //for click event
    var bloggersHeader = $('.filter-by .filter-content .block-views-blockmt-articles-grid-block-2 > h2.title');
    var bloggersContent = $('.filter-by .filter-content .block-views-blockmt-articles-grid-block-2 > .content');
    var categoriesHeader = $('.filter-by .filter-content .block-blog-tags-links-block > h2.title');
    var categoriesContent = $('.filter-by .filter-content .block-blog-tags-links-block > .content');

    bloggersHeader.append('<i class="fa fa-angle-down">');
    categoriesHeader.append('<i class="fa fa-angle-down">');

    var bloggersHeaderArrow = $('.filter-by .filter-content .block-views-blockmt-articles-grid-block-2 > h2.title > i');
    var categoriesHeaderArrow = $('.filter-by .filter-content .block-blog-tags-links-block > h2.title > i');

    filterClick(filterHeader, filterContent, filterArrow);
    filterClick(bloggersHeader, bloggersContent, bloggersHeaderArrow, true);
    filterClick(categoriesHeader, categoriesContent, categoriesHeaderArrow, true);

  }
});