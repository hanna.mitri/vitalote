jQuery(document).ready(function ($) {


  setTimeout(function () {
    changeBlock();
  }, 8000);


  $('#block-corporateplus-content .field--name-field-slide').click(function () {
    setTimeout(function () {
      changeBlock();
    }, 8000);
  });

  function changeBlock() {
    var links = $('#block-corporateplus-content .field--name-field-slide .active-revslide .related-links');
    if (links.length) {
      $(".related-links-wrapper .empty-text").hide();
      $(".related-links-wrapper .content").html(links);
    } else {
      $(".related-links-wrapper .empty-text").show();
      $(".related-links-wrapper .content").html('');
    }
  }
});
