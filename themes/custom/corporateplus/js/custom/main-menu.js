
jQuery(document).ready(function($) {

  var mainItems = ['products-link', 'buying-link'];
  var maxColsInHeader = 5;
  mainItems.forEach(function (element) {

    for (var i = 1; i <= maxColsInHeader; i++) {
      // Header main menu cols
      var col = $('.header-main-menu .' + element + '+ ul.sf-multicolumn .menu-col-' + i).parent().parent();
      $('.' + element + ' + ul.sf-multicolumn > li > ol').append('<li class="inline-col div-col-' + i + '"></li>');
      $.each(col, function () {
        $('.' + element + ' + ul.sf-multicolumn > li > ol  li.div-col-' + i).append($(this).html());
      });
      col.remove();
    }
  });

  $('#block-mainnavigation-2 .inline-col .sf-multicolumn-column').css('width', '0');

    for (var i = 1; i <= 3; i++) {
        // Footer main menu cols
        var footerCol;
        footerCol = $('.footer-main-menu .footer-col-'+i).parent();
        $('.footer-main-menu .menu-level--0').append('<div class="inline-col div-footer-col-'+i+'"></div>');
        $.each(footerCol, function() {
            $('.footer-main-menu .menu-level--0 .div-footer-col-'+i).append($( this ));
        });
    }

    for (var i = 1; i <= 3; i++) {
        // Footer main menu cols
        var footerProductCol = $('.footer-main-menu .footer-col-2-'+i).parent();
        $('.footer-main-menu .products-link + ul.menu-level--1').append('<div class="inline-product-col div-footer-col-2-'+i+'"></div>');
        $.each(footerProductCol, function() {
            $('.footer-main-menu .products-link + ul.menu-level--1 .div-footer-col-2-'+i).append($( this ));
        });
    }


    var activeParentElement = $('#block-mainnavigation-2 .sf-main li');
    activeParentElement.each(function () {
      var hasActive = $(this).find('.is-active');

      if (hasActive.length > 0) {
        $(this).find('a.sf-depth-1').addClass('is-active');
      }
    });


  // To hide menu items
  // $('#block-menutop-4 .hide-section-from-top').parent().parent().addClass('hide-section-from-top');
  $('#block-menutop-4 [class-hide=hide-section-from-top]').closest('li').addClass('hide-section-from-top');


  // Footer menu items (hide li items that has 'class-hide="hide-section"' inside a tag)
  $('#subfooter ul.menu-level--0 > div > li').has(' > a[class-hide="hide-section"]').hide();
  $('#subfooter ul.menu-level--0 > li').has(' > a[class-hide="hide-section"]').hide();

});
