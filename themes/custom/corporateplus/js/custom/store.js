jQuery(document).ready(function ($) {
  var content = $('.with-icon');
  content.each(function () {
    if ($(this).text().trim().length == 0) {
      $(this).removeClass('with-icon');
    }
  });

  // List of stores for Store managers
  var operationEditLink = $('.view-id-commerce_stores [headers=view-operations-table-column] li > a');
  var dropdown = $('.view-id-commerce_stores [headers=view-operations-table-column] .dropbutton-multiple');
  var dropdownToogleBlock = $('.view-id-commerce_stores [headers=view-operations-table-column] .dropbutton-toggle')

  operationEditLink.addClass('button');
  dropdown.removeClass('dropbutton-multiple');
  dropdownToogleBlock.remove();

});
