(function ($, Drupal) {
  Drupal.behaviors.myModuleBehavior = {
    attach: function (context, settings) {
      //energy rating & product fiche
      var href = $('.url-for-href');
      href.each(function () {
        if ( $(this).text() == '' ) {
          $(this).parent().parent().parent().hide();
        }
        else {
          $(this).parent().parent().parent().show();
        }
        $(this).parent().find('a').attr("href", $(this).text().trim());
      });

      //price rules
      var hidePrice = $('.field--name-hideprice').text();
      var mainPrice = $('.main-price');
      mainPrice.text('');
      var secondaryPrice = $('.secondary-price');
      secondaryPrice.text('');
      if (!hidePrice || hidePrice == 'No') {
        mainPrice.text($('.path-product .field--name-price').text());
      } else if (hidePrice == 'Promotion') {
        mainPrice.text($('.path-product .field--name-list-price').text());
        secondaryPrice.text($('.path-product .field--name-price').text());
      }


      // Button
      // var url = $('.field--name-ecomurl').text();
      // var label = $('.field--name-ecomlabel').text();
      // if (url) {
      //   label = label ? label.trim() : Drupal.t('Buy Now');
      //   var button = '<a href="'+url+'" class="button" rel="nofollow">'+label+'</a>';
      //   $('.right-product .text-center').html(button);
      // } else {
      //   $('.right-product .text-center').html('');
      // }

      var imageField = $(context).hasClass('field--name-field-dam-image') ? $(context) : $(context).find('.field--name-field-dam-image');

      //for big screens
      if ($(window).width() > 768) {

        //zoom for main image
        imageField.find('img').after('<div id="zoomed-image"></div>');
        imageField.zoom({
          magnify: 1.5,
          target: '#zoomed-image',
          duration: 0
        });

        //popup for main image
        imageField.find('a').once('myModuleBehavior').magnificPopup({
          type: "image",
          removalDelay: 30,
          mainClass: "mfp-fade",
          gallery: {
            enabled: false, // set to true to enable gallery
          }
        });

      }


      /* for mobile screens */
      else {

        //remove link from main image
        imageField.find("img").unwrap();


        //move description above image
        $('.left-product').prepend($('.right-product .field--name-description'));

        // plus to see product details
        var closedFilters = '<span class="text btn-show">Show filters    </span><i class="fa fa-filter"></i>';
        var openFilters = '<span class="text btn-hide">Hide filters    </span><i class="fa fa-filter"></i>';
        var filters = $(context).find('.right-product > .field--name-variations');
        filters.css({'height': '0', 'overflow': 'hidden'});

        // filters.hide();
        $('.mobile-bt-wrapper').once('myModuleBehavior').before('<div class="see-filters to-show btn btn-default">' + closedFilters + '</div>');
        $('.see-filters').once('myModuleBehavior').on('click', function () {
          // filters.toggle();
          if ($(this).hasClass("to-show")) {
            filters.css('height', 'auto');
            $(this).removeClass("to-show");
            $(this).html(openFilters);
          }
          else {
            filters.css('height', '0');
            $(this).addClass("to-show");
            $(this).html(closedFilters);
          }
        });

        // trim description
        var showChar = 75;  // How many characters are shown by default
        var ellipsestext = "...";
        var moretext = "READ MORE »";
        var lesstext = "« HIDE";
        var description = $(context).hasClass('field--name-description') ? $(context) : $(context).find('.field--name-description');
        description.each(function () {
          var content = $(this).text();
          if (content.length > showChar) {
            var c = content.substr(0, showChar);
            var h = content.substr(showChar, content.length - showChar);
            var html = c + '<span class="moreellipses">' + ellipsestext + '&nbsp;</span><span class="morecontent">' +
              '<span>' + h + '</span>&nbsp;&nbsp;<a href=""  class="morelink read-more" style="color: black">' + moretext + '</a></span>';
            $(this).html(html);
          }
        });
        $(".morelink").once('myModuleBehavior').click(function () {
          if ($(this).hasClass("less")) {
            $(this).removeClass("less");
            $(this).html(moretext);
          } else {
            $(this).addClass("less");
            $(this).html(lesstext);
          }
          $(this).parent().prev().toggle();
          $(this).prev().toggle();
          return false;
        });

      }
    }
  };
})(jQuery, Drupal);


// change url on ajax
(function($) {
  $.fn.changeUrl = function(url) {
    window.history.replaceState('some data', 'Next variation', url);
  };
})(jQuery);
