Drupal.behaviors.productFilters = {
  attach: function (context, settings) {
    var searchBlock = jQuery('.block-facets-summary-blockcurrent-search');
    var search = jQuery('.block-facets-summary-blockcurrent-search > h2.title');
    var searchContent = jQuery('.block-facets-summary-blockcurrent-search .content');
    var blockFilterBy = jQuery('.block-filterby');
    var title = jQuery('.block-facet--checkbox .facets-widget-checkbox > h3, .key-features h3');
    var productMenuLinkActive = jQuery('.block-superfishproduct-category-menu .product-category-menu-link.is-active');
    var productActive = jQuery('.page-title');
    var keyFeatures = jQuery('.key-features');
    var keyFeaturesItems = jQuery('.key-features .facet-item');


    if (productMenuLinkActive.length) {
      productMenuLinkActive.hide();
    }
    searchContent.once('productFiltersOnce').prepend('<span class="product-active">' + productActive.text() + '</span>');
    jQuery('.block-facet-blockrange-cooker-collection h3').text(productActive.text().toLowerCase() + ' Collection');

    if (keyFeaturesItems.length <= 0) {
      keyFeatures.addClass('empty');
    }
    if (searchContent.children().length <= 0) {
      searchBlock.addClass('empty');
    }
    if (title.length <= 0) {
      blockFilterBy.addClass('empty');
    }


    title.once('productFilters').click(function () {
      jQuery(this).next().slideToggle();
      jQuery(this).toggleClass('closed');
    });

    search.once('productFilters').click(function () {
      if (jQuery(this).parent().find('.content').is(":visible")) {
        jQuery(this).parent().find('.content').slideUp();
        jQuery(this).addClass('closed');
      } else {
        jQuery(this).parent().find('.content').slideDown();
        if (searchContent.children().length) {
          jQuery(this).removeClass('closed');
        } else {
          search.addClass('closed');
        }
      }
    });

    //for info description popup in comparison popup
    jQuery('.field--description.popup').once().click(function () {
      var popup = jQuery(this).find('span');
      popup.toggleClass("show");
    });

    //for left-right scroll on mobile compare popup
    if (jQuery(window).width() < 768) {
      jQuery('#comparison-page').once('scroll')
        .append('<div id="right-scroll-button" ></div>' +
          '<div id="left-scroll-button"></div>');

      var right = jQuery('#right-scroll-button');
      var left = jQuery('#left-scroll-button');

      right.once('scroll').click(function () {
        event.preventDefault();
        jQuery('#block-comparisonpage').animate({
          scrollLeft: "+=200px"
        }, "slow");
      });
      left.once('scroll').click(function () {
        event.preventDefault();
        jQuery('#block-comparisonpage').animate({
          scrollLeft: "-=200px"
        }, "slow");
      });
    }
    //for filters on mobile and tablets
    if (jQuery(window).width() < 992) {
      jQuery(document).ready(function ($) {
        //to do 100% width for sidebar block
        var sidebarFirst = jQuery('.region-sidebar-first');
        var blockContainerLeft = jQuery('.container').css('margin-left');
        var blockContainerRight = jQuery('.container').css('margin-right');
        var pagination = jQuery('.detailed-pager--top:not(.second-pager)');

        sidebarFirst.css({
          'margin-left': -(15 + parseInt(blockContainerLeft)) + 'px',
          'margin-right': -(15 + parseInt(blockContainerRight)) + 'px'
        });
        pagination.css({
          'margin-left': -(15 + parseInt(blockContainerLeft)) + 'px',
          'margin-right': -(15 + parseInt(blockContainerRight)) + 'px'
        });


        //to move filters
        var pageTitle = jQuery('#block-corporateplus-page-title');
        var currentSearchBlock = jQuery('#block-currentsearch');
        var currentSearchList = jQuery('#block-currentsearch > .content > ul');
        var clearAll = jQuery('#block-currentsearch .facet-summary-item--clear');
        var filterByBlock = jQuery('#block-filterby');
        var filterByBlockHeader = jQuery('#block-filterby h2.title');
        var filterByBlockArrow = jQuery('<i class="fa fa-angle-down"></i>');
        var filterByContent = jQuery('<div class="filter-content"></div>');
        var filters = jQuery('.block-facet--checkbox.block-facets:not(.block-keyfeatures-facet-content), .key-features');
        var categoryMenuBlock = jQuery('.block-superfishproduct-category-menu');
        var categoryMenuHeader = jQuery('.block-superfishproduct-category-menu > .content > .sf-accordion-toggle > a');
        var paginationItems = jQuery('.detailed-pager--top:not(.second-pager) > .row > div.col-sm-2');
        var paginationPagination = jQuery('.detailed-pager--top:not(.second-pager) > .row > div.col-sm-10');

        pageTitle.after(sidebarFirst);
        filterByBlock.after(filterByContent);
        filterByBlockHeader.once('productFilters').append(filterByBlockArrow);
        filterByBlockHeader.append(clearAll);
        filterByContent.prepend(filters);
        filterByContent.after(categoryMenuBlock);
        if (currentSearchList.length <= 0) {
          currentSearchBlock.hide();
        }
        sidebarFirst.after(pagination);
        paginationItems.removeClass('col-sm-2');
        paginationItems.addClass('col-sm-4 col-xs-4');
        paginationPagination.removeClass('col-sm-10');
        paginationPagination.addClass('col-sm-8 col-xs-8');

        let paginationItems2 = jQuery('.second-pager .col-sm-2')
        let paginationPagination2 = jQuery('.second-pager .col-sm-10')
        paginationItems2.removeClass('col-sm-2');
        paginationItems2.addClass('col-sm-4 col-xs-4');
        paginationPagination2.removeClass('col-sm-10');
        paginationPagination2.addClass('col-sm-8 col-xs-8');


        //for click events
        var filterByArrow = jQuery('#block-filterby h2.title > i');

        filterClick(filterByBlockHeader, filterByContent, filterByArrow);

        categoryMenuHeader.once('productFilters').click(function () {
          jQuery(this).parent().toggleClass('closed');
        });
      });
    }
  }
};
