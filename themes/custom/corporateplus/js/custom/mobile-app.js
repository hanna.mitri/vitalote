jQuery(document).ready(function($) {


  //button for native app call
  $('.call-mobile-app').show();
  $('.call-mobile-app').on('click', function () {
      callNativeApp();
  });

  function callNativeApp () {
    var sku = $('.sku').text().trim();
    //android
    if(typeof Android !== "undefined" && Android !== null) {
      Android.showToast(sku);
    }
    //ios
    try {
      webkit.messageHandlers.callbackHandler.postMessage(sku);
    } catch(err) {
      console.log('The native ios context does not exist yet');
    }
  }

  // trim description
  var showChar = 250;  // How many characters are shown by default
  var ellipsestext = "...";
  var moretext = "Show more >";
  var lesstext = "< Show less";

  $('.field--name-description').each(function() {
    var content = $(this).html();

    if(content.length > showChar) {

      var c = content.substr(0, showChar);
      var h = content.substr(showChar, content.length - showChar);
      var html = c + '<span class="moreellipses">' + ellipsestext+ '&nbsp;</span><span class="morecontent"><span>' + h + '</span>&nbsp;&nbsp;<a href="" class="morelink">' + moretext + '</a></span>';

      $(this).html(html);
    }
  });

  $(".morelink").click(function(){
    if($(this).hasClass("less")) {
      $(this).removeClass("less");
      $(this).html(moretext);
    } else {
      $(this).addClass("less");
      $(this).html(lesstext);
    }
    $(this).parent().prev().toggle();
    $(this).prev().toggle();
    return false;
  });



});
