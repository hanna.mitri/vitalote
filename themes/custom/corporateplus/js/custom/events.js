(function ($, Drupal, drupalSettings) {
  Drupal.behaviors.eventsCalendar = {
    attach: function (context, settings) {

      //events list
      var row = $('.view-display-id-page_2 .views-row', context);

      row.each(function () {
        var color = $(this).find('.views-field-field-color').text().trim();
        var elements = $(this).find('.views-field-field-date, .views-field-field-date-1, .views-field-field-event-location');
        var readMore = $(this).find('.views-field-view-node a');

        if (color) {
          elements.css('color', color);
          readMore.css('background-color', color);
        }
      });

    }
  };
})(jQuery, Drupal, drupalSettings);