(function ($, Drupal, drupalSettings) {
  Drupal.behaviors.storeLocator = {
    attach: function (context, settings) {
      //remove link text
      $('.open-arrow').text('');

      var isMobile = window.matchMedia("only screen and (max-width: 600px)").matches;

      if (!isMobile) {
        //scrolling
        var window_height;
        var doc_height;
        var posTop_s1;
        var posBottom_s1;

        $(document).ready(function () {
          getValues();
        });

        $(window).scroll(function () {
          var scroll = $(window).scrollTop();


          if (scroll < posTop_s1) {
            $('.locator > .view-content').removeClass('fixy');
            $('.locator > .view-content').removeClass('bottom');
          }

          if (scroll > posTop_s1) {
            $('.locator > .view-content').removeClass('fixy');
            $('.locator > .view-content').removeClass('bottom');
            $('.locator > .view-content').addClass('fixy');
          }
          if (scroll > posBottom_s1) {
            $('.locator > .view-content').removeClass('fixy');
            $('.locator > .view-content').removeClass('bottom');
            $('.locator > .view-content').addClass('bottom');
            $('.bottom').css({'max-height': window_height + 'px'});
          }
        });

        function getValues() {
          window_height = $(window).height();
          doc_height = $(document).height();

          //get heights first
          var height_s1 = $('.locator').height();

          //get top position second
          posTop_s1 = $('.locator > .view-content .geolocation-common-map-container').height();
          //get bottom position
          posBottom_s1 = height_s1;
        }

        var rtime;
        var timeout = false;
        var delta = 50;
        $(window).resize(function () {
          rtime = new Date();
          if (timeout === false) {
            timeout = true;
            setTimeout(resizeend, delta);
          }
        });

        function resizeend() {
          if (new Date() - rtime < delta) {
            setTimeout(resizeend, delta);
          } else {
            timeout = false;
            getValues();
          }
        }
      } else {
        //move map
        var tickets = $('.locator > .attachment-before');
        $('.locator').append(tickets);
      }

      var colors = $('.locator .views-field-field-color .field-content');
      colors.each(function () {
        var color = $(this).text().trim();
        $(this).parent().parent().css('background-color', color);
        $('.locator .views-field-field-storewebsite .field-content:hover').css('background-color', color);
      });

      //move form
      var dependentSelect = $('#block-dependentselectblock');
      var autocomplete = $('#block-autocompleteproductsblock')
      var submit = $('#views-exposed-form-store-locator-page-1 .form-actions.first-input');
      submit.hide();
      if (submit.length) {
        dependentSelect.hide();
        autocomplete.hide();
        $("input").on('blur keyup change click', function () {
          submit.show();
        });
        $('.locator .companies-list .view-content').css('min-height', 'auto');
      }
      var submit2 = $('#views-exposed-form-store-locator-page-1 .form-actions.second-input ');
      submit2.before(autocomplete);
      submit2.before(dependentSelect);

      //set width for dependent select inputs
      var width = $('#views-exposed-form-store-locator-page-1').width();
      if (jQuery(window).width() > 768) {
        var selects = $('#dependent-select-wrapper .form-type-select');
        selects.each(function () {
          $(this).css('width', (width / 5));
        });
      }

      //slide un/down Product search field
      var searchSummary = $('.product_search_summary');
      var searchContent = $('#dependent-select-wrapper');
      var searchArrow = $('.product_search_summary > i')
      filterClick(searchSummary, searchContent, searchArrow);

      //ajax callback function
      (function ($) {
        $.fn.changeInputValueAjaxCallback = function (selector, value, chosen) {
          cleanInput(selector);

          var obj = JSON.parse(value);
          if ($.isArray(obj)) {
            for (var i = 0; i < obj.length; i++) {
              $('#' + selector + ' option[value=' + obj[i] + ']').attr('selected', 'selected');
            }
          }
          setChosen(chosen);
        };
      })(jQuery);

      //ajax callback function
      (function ($) {
        $.fn.changeInputValueOnAutocompleteAjaxCallback = function (selector, value, chosen) {
          cleanInput(selector);
          $('#' + selector + ' option[value=' + value + ']').attr('selected', 'selected');
          setChosen(chosen);
        };
      })(jQuery);


      $('.js-reset').click(function () {
        cleanInput('product-id-wrapper');
        cleanInput('edit-category');
        cleanInput('dropdown_third_replace'); //todo
        $("#edit-category")[0].selectedIndex = 0;
        $("#edit-collection")[0].selectedIndex = 0;


        $('#dropdown_third_replace select').each(function () {
          this.selectedIndex = 0;
        });
        document.getElementById("product-id-wrapper").value = '';
        $('.chosen').remove();
      });

      function setChosen(chosen) {
        $('.chosen').remove();
        var reset = ' <a class="js-reset">&#10005;</a>';
        $('#views-exposed-form-store-locator-page-1 .form-actions ').after('<div class="chosen">' + chosen + reset + '</div>');
      }

      function cleanInput(selector) {
        $('#' + selector + ' option').each(function () {
          $(this).removeAttr('selected');
        });
      }
    }
  };
})(jQuery, Drupal, drupalSettings);


//on page load
//this dublication of code is needed bcs js reset button doesn't work when page is just loaded in behaviour
jQuery(document).ready(function ($) {

  var onLoadChosen = $('.chosen-value').text();

  if (onLoadChosen && !$('.chosen').text()) {
    setChosen(onLoadChosen);
  }

  //add chosen element to page
  function setChosen(chosen) {
    $('.chosen').remove();
    var reset = ' <a class="js-reset">&#10005;</a>';
    $('#views-exposed-form-store-locator-page-1 .form-actions ').after('<div class="chosen">' + chosen + reset + '</div>');
  }

  $('.js-reset').click(function () {
    cleanInput('product-id-wrapper');
    cleanInput('dropdown_third_replace');
    $('#dropdown_third_replace select').each(function () {
      this.selectedIndex = 0;
    });
    document.getElementById("product-id-wrapper").value = '';
    $('.chosen').remove();
  });

  function cleanInput(selector) {
    $('#' + selector + ' option').each(function () {
      $(this).removeAttr('selected');
    });
  }
});