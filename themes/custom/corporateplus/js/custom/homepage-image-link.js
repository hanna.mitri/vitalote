jQuery(document).ready(function ($) {

    var field_item = $('.block--type-homapage-image-link-type .field--name-field-image-link > .field__item');

    if (field_item.length > 1) {
        setTimeout(function () {
            var image_height = $('.block--type-homapage-image-link-type .field--name-field-image-link > .field__item ' +
              '.field--name-field-mt-if-image > img').height();

            if (image_height) {
              $('.block--type-homapage-image-link-type .field--name-field-image-link > .field__item .field--name-field-mt-if-image')
                .css('height', image_height);
            }
        }, 500);
    }

});
