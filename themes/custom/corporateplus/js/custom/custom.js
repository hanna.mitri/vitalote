//function for filters slide up and slide down effects
function filterClick(clickButton, content, arrow, classActive) {
  clickButton.click(function () {
    setTimeout(function () {
      content.slideToggle(300);
      arrow.toggleClass('transform-rotate');
      if (classActive === true) {
        jQuery('*').removeClass('active');
        clickButton.addClass('active');
      }
    }, 500);


  });
}

// Add div wrapper for all CKEditor tables
jQuery(document).ready(function ($){
  var tableWrapper = '<div class="ckeditor-table-wrapper"></div>';
  $(".field.text-formatted > table").each(function() {
    $(this).wrap(tableWrapper);
  });

  // external links
  $(document.links).filter(function() {
    return this.hostname != window.location.hostname;
  }).attr('target', '_blank').attr('rel', 'noreferrer noopener nofollow');
});

// highlight
jQuery(document).ready(function ($) {
  $(".paragraph--type--mt-highlight").each(function () {
    var color = $(this).find(".field--name-field-link-color .field__item").text();
    $(this).find(".field--name-field-mt-highlight-link a").css('background-color', color);
  });
});
