(function ($, Drupal, drupalSettings) {
  Drupal.behaviors.contactForm = {
  attach: function (context, settings) {

    var popupBlock = $('.footer-popup');

    //open form
    var colorLabel = $('[data-drupal-selector="edit-purchased-entity-0-attributes-attribute-color"] legend');
    var buttomText = '<span class="request-link">(Request a color sample)</span>';
    colorLabel.once('contactForm').append(buttomText);
    $('.request-link').once('contactForm').click(function () {
      popupBlock.addClass('open');
    });


    //close button
    var closeButton = '<span class="close-popup"><i class="fa fa-times"></i></span>';
    popupBlock.find('.content').addClass('container');
    popupBlock.prepend(closeButton);
    $('.close-popup').once('contactForm').click(function () {
      popupBlock.removeClass('open');
    });


    var form  = popupBlock.find('form');
    if (popupBlock.length !== 0 && form.length == 0) {
      location.reload();
    }
  }
};
})(jQuery, Drupal, drupalSettings);