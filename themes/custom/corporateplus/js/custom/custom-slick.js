(function ($, Drupal, drupalSettings) {
  Drupal.behaviors.productPage = {
    attach: function (context, settings) {

      //slider for product attributes select
      var attributes = $(context).find('.product--rendered-attribute .form-radios');
      attributes.each(function () {

        var items = $(this).find('.form-item');
        var initialSlide = 0;
        if (items.length >= 8) {
          items.each(function (n) { // set initial slide
            if ($( this ).has('input:checked').length) {
              initialSlide = n;
              return false;
            }
          });

          //attributes slider
          $(this).slick({
            swipeToSlide: true,
            dots: false,
            speed: 200,
            slidesToShow: 8,
            slidesToScroll: 4,
            arrows: true,
            initialSlide: initialSlide,
            prevArrow: '<i class="fa fa-chevron-left navigate-arrow s-prev attribute-navigate-arrow"></i>',
            nextArrow: '<i class="fa fa-chevron-right navigate-arrow s-next attribute-navigate-arrow"></i>',
            responsive: [{
              breakpoint: 1024,
              settings: {
                slidesToShow: 8,
                slidesToScroll: 4,
              }
            }, {
              breakpoint: 768,
              settings: {
                slidesToShow: 6,
                slidesToScroll: 2,
              }
            }, {
              breakpoint: 420,
              settings: {
                slidesToShow: 5,
                slidesToScroll: 1,
              }
            }]
          });
        }
      });

      //bottom product slider
      $(context).find('.slider-single').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        fade: false,
        adaptiveHeight: false,
        infinite: true,
        prevArrow: '<i class="fa fa-chevron-left navigate-arrow s-prev"></i>',
        nextArrow: '<i class="fa fa-chevron-right navigate-arrow s-next"></i>',
        useTransform: true,
        speed: 400,
        cssEase: 'cubic-bezier(0.77, 0, 0.18, 1)',
        responsive: [{
          breakpoint: 992,
          settings: {
            adaptiveHeight: true,
            dots: false,
            arrows: true,
          }
        }]
      });

      if (jQuery(window).width() > 767) {
        $(context).find('.slider-nav')
            .on('init', function (event, slick) {
              $(context).find('.slider-nav .slick-slide.slick-current').addClass('is-active');
            })
            .slick({
              slidesToShow: 7,
              slidesToScroll: 7,
              dots: false,
              focusOnSelect: false,
              infinite: true,
              arrows: true,
              prevArrow: '<i class="fa fa-chevron-left navigate-arrow s-prev"></i>',
              nextArrow: '<i class="fa fa-chevron-right navigate-arrow s-next"></i>',
              responsive: [{
                breakpoint: 1024,
                settings: {
                  slidesToShow: 5,
                  slidesToScroll: 5,
                }
              }, {
                breakpoint: 768,
                settings: {
                  arrows: false,
                  slidesToShow: 4,
                  slidesToScroll: 4,
                }
              }, {
                breakpoint: 420,
                settings: {
                  slidesToShow: 3,
                  slidesToScroll: 3,
                }
              }]
            });

      $(context).find('.slider-single').on('afterChange', function (event, slick, currentSlide) {
        $(context).find('.slider-nav').slick('slickGoTo', currentSlide);
        var currrentNavSlideElem = '.slider-nav .slick-slide[data-slick-index="' + currentSlide + '"]';
        $(context).find('.slider-nav .slick-slide.is-active').removeClass('is-active');
        $(currrentNavSlideElem).addClass('is-active');
      });

        $(context).find('.slider-nav').on('click', '.slick-slide', function (event) {
          event.preventDefault();
          var goToSingleSlide = $(this).data('slick-index');

          $(context).find('.slider-single').slick('slickGoTo', goToSingleSlide);
        });
      }


      //top product slider
      $(context).find('.slider-single-1').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        fade: false,
        adaptiveHeight: true,
        infinite: false,
        useTransform: true,
        speed: 400,
        cssEase: 'cubic-bezier(0.77, 0, 0.18, 1)',
      });

      $(context).find('.slider-nav-1')
        .on('init', function (event, slick) {
          $(context).find('.slider-nav-1 .slick-slide.slick-current').addClass('is-active');
        })
        .slick({
          slidesToShow: 2,
          slidesToScroll: 2,
          dots: false,
          arrows: false,
          focusOnSelect: false,
          infinite: true,
          responsive: [{
            breakpoint: 1024,
            settings: {
              slidesToShow: 2,
              slidesToScroll: 2,
            }
          }, {
            breakpoint: 768,
            settings: {
              slidesToShow: 2,
              slidesToScroll: 2,
            }
          }, {
            breakpoint: 420,
            settings: {
              slidesToShow: 2,
              slidesToScroll: 2,
            }
          }]
        });

      $(context).find('.slider-single-1').on('afterChange', function (event, slick, currentSlide) {
        $(context).find('.slider-nav-1').slick('slickGoTo', currentSlide);
        var currrentNavSlideElem = '.slider-nav-1 .slick-slide[data-slick-index="' + currentSlide + '"]';
        $(context).find('.slider-nav-1 .slick-slide.is-active').removeClass('is-active');
        $(currrentNavSlideElem).addClass('is-active');
      });

      $(context).find('.slider-nav-1').on('click', '.slick-slide', function (event) {
        event.preventDefault();
        var goToSingleSlide = $(this).data('slick-index');

        $(context).find('.slider-single-1').slick('slickGoTo', goToSingleSlide);
      });

    }
  };
})(jQuery, Drupal, drupalSettings);
