(function ($, Drupal, drupalSettings) {
  Drupal.behaviors.userGuides = {
    attach: function (context, settings) {

      var moretext = "<a href=\"#\" class=\"plus\"></a>";
      var lesstext = "<a href=\"#\" class=\"minus\"></a>";

      $('.user-guides .views-field-other-versions-views-field').each(function() {

        if ($(this).text().trim().length != 0) {
          $(this).addClass('hidden');
          $(this).parent().find('.views-field-title').append('<div class=\'morelink\'>'+moretext+'</div>');
        }
      });

      $(".morelink").click(function(){
        var versions = $(this).parent().parent().find('.views-field-other-versions-views-field');
        if (versions.hasClass("hidden")) {
          versions.removeClass("hidden");
          $(this).html(lesstext);
        } else {
          versions.addClass("hidden");
          $(this).html(moretext);
        }
        // $(this).prev().toggle();
        return false;
      });

      $('.user-guides .views-row .field--name-field-pdf a')
        .attr({'target' : '_blank', 'rel' : 'nofollow noopener'});

    }
  };
})(jQuery, Drupal, drupalSettings);
