jQuery(document).ready(function ($) {

  //for "Big bg image and text' block
  jQuery('.block--type-big-bg-image-and-text').each(function () {
    var positionClass = jQuery(this).find(".field--name-field-position").text();
    jQuery(this).find('.big-bg-highlighted').addClass("position-"+positionClass);
    //color
    var bgColor = jQuery(this).find(".field--name-field-background-color").text();
    jQuery(this).find('.big-bg-highlighted').css("background-color", bgColor);
  });

  //for right sticky block
  if (jQuery('#block-rightstickyblock').length) {
    if ((window.screen.width) > 991) {
      jQuery(function (sticky) {
        var sticky_block = sticky('#block-rightstickyblock');
        sticky(window).scroll(function () {

          sticky_block['fade' + (sticky(this).scrollTop() > 450 ? 'In' : 'Out')](500);
        });
      });
    }
  }

  //for unique feature block
  var featureItem = $('.block--type-mt-if-block:not(#block-rightstickyblock) > .content > .field--name-field-mt-if > .field__item');

  var width = 100/featureItem.length - 1;
  featureItem.css('width', width + '%');

});
