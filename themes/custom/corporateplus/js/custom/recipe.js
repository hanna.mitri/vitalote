jQuery(document).ready(function ($) {

  //for recipes isotope (mobile)
  if($(window).width() < 768){
    var button = $('.view-mt-isotope .view-display-id-attachment_1 .view-header > .icon-wrapper');
    var arrow = $('.view-mt-isotope .view-display-id-attachment_1 .view-header .icon-wrapper > i');
    var categoriesBlock = $('.view-mt-isotope .view-display-id-attachment_1 .view-content');
    var categoriesItem = $('.view-mt-isotope .view-display-id-attachment_1 .view-content .nav-pills > li > a');
    var categoryActive = $('.view-mt-isotope .view-display-id-attachment_1 .view-content .nav-pills > .active > a');

    button.click(function () {
      categoriesBlock.slideToggle(300);
      arrow.toggleClass('transform-rotate');
    });

    button.prepend('<span class="category-item-text">' + categoryActive.text() + '</span>');

    categoriesItem.click(function () {
      var categoryText = $(this).text();
      var categoryClonedText = $('span.category-item-text');

      if (categoryClonedText) {
        categoryClonedText.remove();
      }
      categoriesBlock.slideToggle(300);

      button.prepend('<span class="category-item-text">' + categoryText + '</span>');
    });
  }

  //for recipe content
  var otherRecipesContent = $('.block--type-mt-tabs-block .block-views-blockmt-latest-block-5');
  var otherRecipesBlock = $('.block--type-mt-tabs-block');

  if (otherRecipesContent.length <= 0) {
    otherRecipesBlock.hide();
  }

  //for printing recipes page
  var printPage = $('#printPage');

  printPage.click(function () {
    window.print();
  });

});