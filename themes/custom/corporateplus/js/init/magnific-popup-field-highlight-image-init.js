(function ($, Drupal, drupalSettings) {
  Drupal.behaviors.mtMagnificPopupHighlightImage = {
    attach: function (context, settings) {

      $(context).find('.field--name-field-mt-highlight-image a.image-popup').once('mtMagnificPopupHighlightImageInit').magnificPopup({
        type:"image",
        removalDelay: 300,
        mainClass: "mfp-fade",
        gallery: {
          enabled: true, // set to true to enable gallery
        },
        image: {
          titleSrc: function(item) {
            return item.el.closest('.overlay-container').children()[1].title || '';
          }
        }
      });

      $(context).find('.field--name-field-video a').once('mtMagnificPopupHighlightImageInit').magnificPopup({
        type:"iframe",
        removalDelay: 300,
        mainClass: "mfp-fade",
        gallery: {
          enabled: false, // set to true to enable gallery
        },
      });

      // to add 'play' video icon to video content (for Highlight paragraph ?)
      $(context).find('.field--name-field-video a').once('mtMagnificPopupHighlightVideoInit')
        .prepend(
          '      <span class="overlay-icon overlay-icon--button overlay-icon-video">\n' +
          '        <i class="ml-5 fa fa-play"></i>\n' +
          '      </span>\n'
        );

    }
  };
})(jQuery, Drupal, drupalSettings);
