(function ($, Drupal, drupalSettings) {
  Drupal.behaviors.storesAdmin = {
    attach: function (context, settings) {

      var variationlist = $('.stores-view .views-field-field-products ul');

      variationlist.each(function () {

        var variations = $(this).find('li:nth-child(n+5)');
        if (variations.length > 0) {
          variations.hide();
          $(this).once().append('<span class="toggle-more">+ Show more</span>');
        }
      });

      $(".toggle-more ").on('click', function () {
        var text = $(this).text();
        var variations = $(this).parent().find('li:nth-child(n+5)');

        if (text === "+ Show more") {
          text = "- Show less";
          variations.show();
        } else {
          text = "+ Show more";
          variations.hide();
        }
        $(this).text(text);
      });

    }
  };
})(jQuery, Drupal, drupalSettings);

